create table cc_driver_msg
(
    id                  bigint auto_increment
        primary key,
    gmt_create          datetime default CURRENT_TIMESTAMP null comment '创建时间',
    gmt_modified        datetime default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP comment '修改时间',
    deleted             int      default 0                 null comment '逻辑删除标',
    dm_code             varchar(32)                        not null comment '司机发送的信息code',
    phone               varchar(16)                        null comment '联系电话',
    free_seat_num       int                                null comment '空闲座位数量',
    expect_price_fen    int                                null comment '预期价格（分）',
    start_date          bigint                             null comment '出发日期',
    start_province_code varchar(8)                         null comment '起始省code',
    start_city_code     varchar(8)                         null comment '起始市code',
    start_area_code     varchar(8)                         null comment '起始区code',
    end_province_code   varchar(8)                         null comment '目的省code',
    end_city_code       varchar(8)                         null comment '目的市code',
    end_area_code       varchar(8)                         null comment '目睹区code',
    creator_code        varchar(16)                        null comment '创建者code',
    feature             longtext                           null comment '扩展字段 json方式存储',
    constraint dm_code
        unique (dm_code),
    constraint id
        unique (id)
)
    comment '司机发送车找人信息表';

create table cc_head_line
(
    id                  bigint auto_increment
        primary key,
    gmt_create          datetime default CURRENT_TIMESTAMP null comment '创建时间',
    gmt_modified        datetime default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP comment '修改时间',
    deleted             int      default 0                 null comment '逻辑删除标',
    hl_code             varchar(32)                        not null comment '头条信息code',
    biz_type            varchar(8)                         null comment '业务类型',
    head_line_time      bigint                             null comment '头条时间',
    content             varchar(1024)                      null comment '头条内容',
    like_count          int                                null comment '点赞数量',
    start_province_code varchar(8)                         null comment '起始省code',
    start_city_code     varchar(8)                         null comment '起始市code',
    start_area_code     varchar(8)                         null comment '起始区code',
    end_province_code   varchar(8)                         null comment '目的省code',
    end_city_code       varchar(8)                         null comment '目的市code',
    end_area_code       varchar(8)                         null comment '目睹区code',
    creator_code        varchar(16)                        null comment '创建者code',
    feature             longtext                           null comment '扩展字段 json方式存储',
    constraint hl_code
        unique (hl_code),
    constraint id
        unique (id)
)
    comment '头条表信息表';

create table cc_like
(
    id           bigint auto_increment
        primary key,
    gmt_create   datetime default CURRENT_TIMESTAMP null comment '创建时间',
    gmt_modified datetime default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP comment '修改时间',
    deleted      int      default 0                 null comment '逻辑删除标',
    like_code    varchar(32)                        not null comment '点赞code',
    biz_type     varchar(8)                         null comment '业务类型',
    biz_code     varchar(32)                        null comment '业务code',
    creator_code varchar(16)                        null comment '创建者code',
    feature      longtext                           null comment '扩展字段 json方式存储',
    constraint id
        unique (id),
    constraint like_code
        unique (like_code)
)
    comment '点赞信息表';

create table cc_user
(
    id           bigint auto_increment
        primary key,
    gmt_create   datetime default CURRENT_TIMESTAMP null comment '创建时间',
    gmt_modified datetime default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP comment '修改时间',
    deleted      int      default 0                 null comment '逻辑删除标',
    user_code    varchar(32)                        not null comment '用户code',
    open_id      varchar(32)                        not null comment '微信openId',
    phone        varchar(16)                        null comment '手机号',
    avatar_url   varchar(128)                       null comment '头像URL',
    gender       int                                null comment '性别1 男 0 女',
    age          int                                null comment '年龄',
    use_language varchar(8)                         null comment '使用语言',
    is_locked    int      default 0                 null comment '是否锁定',
    feature      longtext                           null comment '扩展字段 json方式存储',
    constraint id
        unique (id),
    constraint open_id
        unique (open_id),
    constraint user_code
        unique (user_code)
)
    comment '用户信息表';

create table cc_user_msg
(
    id                  bigint auto_increment
        primary key,
    gmt_create          datetime default CURRENT_TIMESTAMP null comment '创建时间',
    gmt_modified        datetime default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP comment '修改时间',
    deleted             int      default 0                 null comment '逻辑删除标',
    um_code             varchar(32)                        not null comment '用户发送的信息code',
    phone               varchar(16)                        null comment '联系电话',
    head_count          int                                null comment '坐车人数',
    expect_price_fen    int                                null comment '预期价格（分）',
    start_date          bigint                             null comment '出发日期',
    start_province_code varchar(8)                         null comment '起始省code',
    start_city_code     varchar(8)                         null comment '起始市code',
    start_area_code     varchar(8)                         null comment '起始区code',
    end_province_code   varchar(8)                         null comment '目的省code',
    end_city_code       varchar(8)                         null comment '目的市code',
    end_area_code       varchar(8)                         null comment '目的区code',
    creator_code        varchar(16)                        null comment '创建者code',
    feature             longtext                           null comment '扩展字段 json方式存储',
    constraint id
        unique (id),
    constraint um_code
        unique (um_code)
)
    comment '用户发送人找车信息表';

