package com.notname.callcar.service;

import com.notname.callcar.entity.CcCommentEntity;
import com.notname.callcar.entity.CcHeadLineEntity;
import com.notname.callcar.entity.CcLikeEntity;
import com.notname.callcar.entity.CcUserMsgEntity;
import com.notname.callcar.module.req.CcCommentSaveReq;
import com.notname.callcar.module.req.CcHeadLineQueryReq;
import com.notname.callcar.module.req.CcHeadLineSaveReq;
import com.notname.callcar.module.req.CcMsgPageQueryReq;
import com.notname.callcar.module.res.CcHeadLineVO;
import com.notname.callcar.module.res.CcUserMsgVO;
import com.notname.callcar.module.res.PageResult;

public interface ICcHeadLineService {

    int save(CcHeadLineSaveReq req);

    int modify(CcHeadLineSaveReq req);

    PageResult<CcHeadLineVO> pageQuery(CcHeadLineQueryReq req);

    PageResult<CcHeadLineVO> pageQueryMyHeadline(CcHeadLineQueryReq req);

    int like(CcLikeEntity req);

    String comment(CcCommentSaveReq req);

    int remove(String req);



}
