package com.notname.callcar.service;

import com.notname.callcar.entity.CcActivityEntity;
import com.notname.callcar.entity.CcCollectionEntity;
import com.notname.callcar.entity.CcLikeEntity;
import com.notname.callcar.module.req.CcCollectionPageQueryReq;
import com.notname.callcar.module.req.CcCollectionSaveReq;
import com.notname.callcar.module.req.CcCommentPageQueryReq;
import com.notname.callcar.module.res.CcCollectionVO;
import com.notname.callcar.module.res.CcCommentVO;
import com.notname.callcar.module.res.PageResult;

public interface ICcCollectionService {

    PageResult<CcCollectionVO> pageQuery(CcCollectionPageQueryReq req);

    int save(CcCollectionSaveReq req);

    int remove(String req);


}
