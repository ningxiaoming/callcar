package com.notname.callcar.service;

import com.notname.callcar.entity.CcActivityEntity;
import com.notname.callcar.entity.CcUserMsgEntity;
import com.notname.callcar.module.req.CcActivityPageQueryReq;
import com.notname.callcar.module.req.CcMsgPageQueryReq;
import com.notname.callcar.module.res.CcActivityVO;
import com.notname.callcar.module.res.CcUserMsgVO;
import com.notname.callcar.module.res.GlobalConfigVO;
import com.notname.callcar.module.res.PageResult;

import java.util.List;

public interface ICcActivityService {

    int save(CcActivityEntity req);


    int modify(CcActivityEntity req);

    int upOrDown(Long id);

    int remove(Long id);

    PageResult<CcActivityVO> pageQuery(CcActivityPageQueryReq req);

    GlobalConfigVO queryConfig();


}
