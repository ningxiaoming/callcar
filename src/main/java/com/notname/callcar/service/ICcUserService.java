package com.notname.callcar.service;

import cn.binarywang.wx.miniapp.bean.WxMaJscode2SessionResult;
import com.notname.callcar.entity.CcUserEntity;
import com.notname.callcar.module.req.WxLoginReq;
import com.notname.callcar.module.res.CcUserVO;
import me.chanjar.weixin.common.error.WxErrorException;

public interface ICcUserService {

    int save(CcUserEntity req);
    CcUserEntity login(WxLoginReq req);
    int completion(CcUserEntity req);

    /**
     * 解密手机号
     * @param phoneStr
     * @return
     */
    String decryptPhone(String phoneStr);

    int modify(CcUserEntity req);

    CcUserVO queryByCode();

    CcUserVO queryByUserCode(String userCode);



}
