package com.notname.callcar.service;

import com.notname.callcar.entity.CcDriverMsgEntity;
import com.notname.callcar.entity.CcLikeEntity;
import com.notname.callcar.module.req.CcCommentPageQueryReq;
import com.notname.callcar.module.req.CcMsgPageQueryReq;
import com.notname.callcar.module.res.CcCommentVO;
import com.notname.callcar.module.res.CcDriverMsgVO;
import com.notname.callcar.module.res.PageResult;

public interface ICcCommentService {

    PageResult<CcCommentVO> pageQuery(CcCommentPageQueryReq req);

    int like(CcLikeEntity req);

    int remove(String req);



}
