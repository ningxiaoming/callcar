package com.notname.callcar.service.impl;

import com.notname.callcar.aop.ControllerLog;
import com.notname.callcar.common.enums.CcBizTypeEnum;
import com.notname.callcar.common.enums.DeletedEnum;
import com.notname.callcar.common.exception.CcBizException;
import com.notname.callcar.common.exception.CcErrorCodeEnum;
import com.notname.callcar.config.VOConverter;
import com.notname.callcar.dao.CcUserMsgDao;
import com.notname.callcar.entity.*;
import com.notname.callcar.module.req.CcMsgPageQueryReq;
import com.notname.callcar.module.req.CcRemoveReq;
import com.notname.callcar.module.res.CcUserMsgVO;
import com.notname.callcar.module.res.CcUserVO;
import com.notname.callcar.module.res.PageResult;
import com.notname.callcar.service.ICcUserMsgService;
import com.notname.callcar.utils.DateUtils;
import com.notname.callcar.utils.PhoneEncryptUtil;
import com.notname.callcar.utils.SnowFlakeUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.ObjectUtils;

import java.util.*;
import java.util.stream.Collectors;


@Service
@Slf4j
public class CcUserMsgServiceImpl extends BaseService implements ICcUserMsgService {

    @Autowired
    private CcUserMsgDao ccUserMsgDao;

    @Autowired
    private SnowFlakeUtil snowFlakeUtil;


    @Override
    public int save(CcUserMsgEntity req) {
        String currentUserCode = ControllerLog.currentUserCodeThreadLocal.get();
        Assert.notNull(req, "req is null");
        Assert.hasLength(req.getPhone(), "Phone is null");
        Assert.hasLength(req.getStartProvinceCode(), "StartProvinceCode is null");
        Assert.hasLength(req.getStartCityCode(), "StartCityCode is null");
        Assert.hasLength(req.getStartAreaCode(), "StartAreaCode is null");
        Assert.hasLength(req.getEndProvinceCode(), "EndProvinceCode is null");
        Assert.hasLength(req.getEndCityCode(), "EndCityCode is null");
        Assert.hasLength(req.getEndAreaCode(), "EndAreaCode is null");
        Assert.hasLength(currentUserCode, "currentUserCode is null");
        Assert.notNull(req.getStartDate(), "StartDate is null");
        Assert.notNull(req.getHeadCount(), "HeadCount is null");
        Assert.isTrue(req.getHeadCount() > 0, "HeadCount must > 0");
        if (req.getPhone().contains("****")){
            CcUserEntity userEntityByCode = findUserEntityByCode(currentUserCode);
            req.setPhone(userEntityByCode.getPhone());
        }
        Assert.isTrue(PhoneEncryptUtil.isValidPhoneNumber(req.getPhone()),"非法的电话号");
        req.setUmCode(snowFlakeUtil.snowflakeStringId());
        req.setCreatorCode(currentUserCode);
        Date date = new Date();
        req.setGmtCreate(date);
        req.setGmtModified(date);
        req.setDeleted(0);
        return ccUserMsgDao.insert(req);
    }


    @Override
    public int modify(CcUserMsgEntity req) {
        CcUserMsgEntity ccUserMsgEntity = ccUserMsgDao.selectByPrimaryKey(req.getId());
        Assert.notNull(ccUserMsgEntity, "msg is null");
        if (!req.getCreatorCode().equals(ccUserMsgEntity.getCreatorCode())){
            throw new CcBizException(CcErrorCodeEnum.NO_PERMISSION);
        }
        CcUserMsgEntityExample example = new CcUserMsgEntityExample();
        example.createCriteria().andIdEqualTo(req.getId());
        req.setGmtModified(new Date());
        if (!ObjectUtils.isEmpty(req.getPhone())){
            req.setPhone(PhoneEncryptUtil.encryptBase64(req.getPhone()));
            Assert.isTrue(PhoneEncryptUtil.isValidPhoneNumber(req.getPhone()),"非法的电话号");
        }
        return ccUserMsgDao.updateByExampleSelective(req, example);
    }

    @Override
    public CcUserMsgVO queryUserMsgByCode(String umCode) {
        Assert.hasLength(umCode, "biz is null");
        CcUserMsgEntityExample example = new CcUserMsgEntityExample();
        example.createCriteria().andUmCodeEqualTo(umCode).andDeletedEqualTo(DeletedEnum.NOT_DELETE.getCode());
        List<CcUserMsgEntity> ccUserMsgEntities = ccUserMsgDao.selectByExample(example);
        Assert.notEmpty(ccUserMsgEntities, "UserMsg is null");
        Map<String, CcCityEntity> cityEntityMap = getCityEntityMap(ccUserMsgEntities);
        CcUserMsgVO ccUserMsgVO = VOConverter.INSTANCE.conver(ccUserMsgEntities.get(0));
        buildCityName(cityEntityMap,ccUserMsgVO);
        if (!ObjectUtils.isEmpty(ccUserMsgVO.getPhone())){
            ccUserMsgVO.setPhone(PhoneEncryptUtil.decryptStr(ccUserMsgVO.getPhone()));
        }
        return ccUserMsgVO;
    }

    @Override
    public PageResult<CcUserMsgVO> pageQuery(CcMsgPageQueryReq req) {
        CcUserMsgEntityExample example = new CcUserMsgEntityExample();
        List<CcUserMsgEntity> ccUserMsgEntities = firstGetCcUserMsgEntities(req, example);
        example.clear();
        if (ObjectUtils.isEmpty(ccUserMsgEntities)){
            ccUserMsgEntities = secondGetCcUserMsgEntities(req,example);
            example.clear();
        }
        if (ObjectUtils.isEmpty(ccUserMsgEntities)){
            ccUserMsgEntities = thirdGetCcUserMsgEntities(req,example);
            example.clear();
        }
        PageResult<CcUserMsgVO> pageResult = new PageResult<>();
        pageResult.setPageNum(req.getPageNum());
        pageResult.setPageSize(req.getPageSize());
        if (ObjectUtils.isEmpty(pageResult)) {
            return pageResult;
        }
        List<String> userCodes = ccUserMsgEntities.stream().map(CcUserMsgEntity::getCreatorCode).collect(Collectors.toList());
        Map<String, CcUserVO> ccUserVOMap = findUserEntity(userCodes);
        Map<String, CcCityEntity> cityEntityMap = getCityEntityMap(ccUserMsgEntities);
        List<CcUserMsgVO> collect = ccUserMsgEntities.stream()
                .map(VOConverter.INSTANCE::conver)
                .peek(item -> {
                    item.setCcUserVO(ccUserVOMap.get(item.getCreatorCode()));
                    buildCityName(cityEntityMap, item);
                })
                .collect(Collectors.toList());
        pageResult.setList(collect);
        return pageResult;
    }

    // 第一次精确查询
    private List<CcUserMsgEntity> firstGetCcUserMsgEntities(CcMsgPageQueryReq req, CcUserMsgEntityExample example) {
        CcUserMsgEntityExample.Criteria criteria = example.createCriteria();
        criteria.andDeletedEqualTo(DeletedEnum.NOT_DELETE.getCode());
        if (!ObjectUtils.isEmpty(req.getStartProvinceCode())) {
            criteria.andStartProvinceCodeEqualTo(req.getStartProvinceCode());
        }
        if (!ObjectUtils.isEmpty(req.getStartCityCode())) {
            criteria.andStartCityCodeEqualTo(req.getStartCityCode());
        }
        if (!ObjectUtils.isEmpty(req.getStartAreaCode())) {
            criteria.andStartAreaCodeEqualTo(req.getStartAreaCode());
        }
        if (!ObjectUtils.isEmpty(req.getEndProvinceCode())) {
            criteria.andEndProvinceCodeEqualTo(req.getEndProvinceCode());
        }
        if (!ObjectUtils.isEmpty(req.getEndCityCode())) {
            criteria.andEndCityCodeEqualTo(req.getEndCityCode());
        }
        if (!ObjectUtils.isEmpty(req.getEndAreaCode())) {
            criteria.andEndAreaCodeEqualTo(req.getEndAreaCode());
        }
        if (ObjectUtils.isEmpty(req.getStartDate())) {
            criteria.andStartDateGreaterThanOrEqualTo(DateUtils.initDateByDay().getTime());
        } else {
            criteria.andStartDateGreaterThanOrEqualTo(req.getStartDate());
        }
        example.setOrderByClause("`gmt_modified` DESC");
        example.setLimit(req.getPageSize());
        Long offset = (long) (req.getPageNum() - 1) * req.getPageSize();
        example.setOffset(offset);
        return ccUserMsgDao.selectByExample(example);
    }

    // 第二次查询到市
    private List<CcUserMsgEntity> secondGetCcUserMsgEntities(CcMsgPageQueryReq req, CcUserMsgEntityExample example) {
        CcUserMsgEntityExample.Criteria criteria = example.createCriteria();
        criteria.andDeletedEqualTo(DeletedEnum.NOT_DELETE.getCode());
        if (!ObjectUtils.isEmpty(req.getStartProvinceCode())) {
            criteria.andStartProvinceCodeEqualTo(req.getStartProvinceCode());
        }
        if (!ObjectUtils.isEmpty(req.getStartCityCode())) {
            criteria.andStartCityCodeEqualTo(req.getStartCityCode());
        }
        if (!ObjectUtils.isEmpty(req.getEndProvinceCode())) {
            criteria.andEndProvinceCodeEqualTo(req.getEndProvinceCode());
        }
        if (!ObjectUtils.isEmpty(req.getEndCityCode())) {
            criteria.andEndCityCodeEqualTo(req.getEndCityCode());
        }
        if (ObjectUtils.isEmpty(req.getStartDate())) {
            criteria.andStartDateGreaterThanOrEqualTo(DateUtils.initDateByDay().getTime());
        } else {
            criteria.andStartDateGreaterThanOrEqualTo(req.getStartDate());
        }
        example.setOrderByClause("`gmt_modified` DESC");
        example.setLimit(req.getPageSize());
        Long offset = (long) (req.getPageNum() - 1) * req.getPageSize();
        example.setOffset(offset);
        return ccUserMsgDao.selectByExample(example);
    }

    // 第三次查询到省
    private List<CcUserMsgEntity> thirdGetCcUserMsgEntities(CcMsgPageQueryReq req, CcUserMsgEntityExample example) {
        CcUserMsgEntityExample.Criteria criteria = example.createCriteria();
        criteria.andDeletedEqualTo(DeletedEnum.NOT_DELETE.getCode());
        if (!ObjectUtils.isEmpty(req.getStartProvinceCode())) {
            criteria.andStartProvinceCodeEqualTo(req.getStartProvinceCode());
        }
        if (!ObjectUtils.isEmpty(req.getEndProvinceCode())) {
            criteria.andEndProvinceCodeEqualTo(req.getEndProvinceCode());
        }
        if (ObjectUtils.isEmpty(req.getStartDate())) {
            criteria.andStartDateGreaterThanOrEqualTo(DateUtils.initDateByDay().getTime());
        } else {
            criteria.andStartDateGreaterThanOrEqualTo(req.getStartDate());
        }
        example.setOrderByClause("`gmt_modified` DESC");
        example.setLimit(req.getPageSize());
        Long offset = (long) (req.getPageNum() - 1) * req.getPageSize();
        example.setOffset(offset);
        return ccUserMsgDao.selectByExample(example);
    }

    @Override
    public PageResult<CcUserMsgVO> pageQueryUserMsg(CcMsgPageQueryReq req) {
        Assert.notNull(req, "userCode is null");
        String currentUserCode = ControllerLog.currentUserCodeThreadLocal.get();
        Assert.hasLength(currentUserCode, "CurrentUserCode is null");
        CcUserMsgEntityExample example = new CcUserMsgEntityExample();
        CcUserMsgEntityExample.Criteria criteria = example.createCriteria();
        criteria.andDeletedEqualTo(DeletedEnum.NOT_DELETE.getCode()).andCreatorCodeEqualTo(currentUserCode);
        example.setOrderByClause("`gmt_modified` DESC");
        example.setLimit(req.getPageSize());
        Long offset = (long) (req.getPageNum() - 1) * req.getPageSize();
        example.setOffset(offset);
        long count = ccUserMsgDao.countByExample(example);
        List<CcUserMsgEntity> ccUserMsgEntities = ccUserMsgDao.selectByExample(example);
        PageResult<CcUserMsgVO> pageResult = new PageResult<>();
        pageResult.setTotal(count);
        pageResult.setPageNum(req.getPageNum());
        pageResult.setPageSize(req.getPageSize());
        if (ObjectUtils.isEmpty(ccUserMsgEntities)) {
            return pageResult;
        }
        List<String> userCodes = ccUserMsgEntities.stream().map(CcUserMsgEntity::getCreatorCode).collect(Collectors.toList());
        Map<String, CcUserVO> ccUserVOMap = findUserEntity(userCodes);
        Map<String, CcCityEntity> cityEntityMap = getCityEntityMap(ccUserMsgEntities);
        List<CcUserMsgVO> collect = ccUserMsgEntities.stream()
                .map(VOConverter.INSTANCE::conver)
                .peek(item -> {
                    item.setCcUserVO(ccUserVOMap.get(item.getCreatorCode()));
                    item.setPhone(PhoneEncryptUtil.decryptStr(item.getPhone()));
                    buildCityName(cityEntityMap, item);
                })
                .collect(Collectors.toList());
        pageResult.setList(collect);
        return pageResult;
    }

    @Override
    public int remove(String umCode) {
        String currentUserCode = ControllerLog.currentUserCodeThreadLocal.get();
        Assert.hasLength(currentUserCode, "currentUserCode is null");
        Assert.hasLength(umCode, "umCode is null");
        CcUserMsgEntityExample example = new CcUserMsgEntityExample();
        example.createCriteria().andUmCodeEqualTo(umCode).andDeletedEqualTo(DeletedEnum.NOT_DELETE.getCode());
        List<CcUserMsgEntity> ccUserMsgEntities = ccUserMsgDao.selectByExample(example);
        Assert.notEmpty(ccUserMsgEntities, "UserMsg is null");
        CcUserMsgEntity ccCommentEntity = ccUserMsgEntities.get(0);
        if (!currentUserCode.equals(ccCommentEntity.getCreatorCode())) {
            throw new CcBizException(CcErrorCodeEnum.NO_PERMISSION);
        }
        ccCommentEntity.setDeleted(DeletedEnum.DELETE.getCode());
        return ccUserMsgDao.updateByPrimaryKeySelective(ccCommentEntity);
    }



    private void buildCityName(Map<String, CcCityEntity> cityEntityMap, CcUserMsgVO item) {
        item.setStartProvinceName(cityEntityMap.get(item.getStartProvinceCode()) == null ? "" : cityEntityMap.get(item.getStartProvinceCode()).getCityName());
        item.setStartCityName(cityEntityMap.get(item.getStartCityCode()) == null ? "" : cityEntityMap.get(item.getStartCityCode()).getCityName());
        item.setStartAreaName(cityEntityMap.get(item.getStartAreaCode()) == null ? "" : cityEntityMap.get(item.getStartAreaCode()).getCityName());
        item.setEndProvinceName(cityEntityMap.get(item.getEndProvinceCode()) == null ? "" : cityEntityMap.get(item.getEndProvinceCode()).getCityName());
        item.setEndCityName(cityEntityMap.get(item.getEndCityCode()) == null ? "" : cityEntityMap.get(item.getEndCityCode()).getCityName());
        item.setEndAreaName(cityEntityMap.get(item.getEndAreaCode()) == null ? "" : cityEntityMap.get(item.getEndAreaCode()).getCityName());
    }

    private Map<String, CcCityEntity> getCityEntityMap(List<CcUserMsgEntity> ccUserMsgEntities) {
        Set<String> startCityCodes1 = ccUserMsgEntities.stream().map(CcUserMsgEntity::getStartProvinceCode).collect(Collectors.toSet());
        Set<String> startCityCodes2 = ccUserMsgEntities.stream().map(CcUserMsgEntity::getStartCityCode).collect(Collectors.toSet());
        Set<String> startCityCodes3 = ccUserMsgEntities.stream().map(CcUserMsgEntity::getStartAreaCode).collect(Collectors.toSet());
        Set<String> endCityCodes1 = ccUserMsgEntities.stream().map(CcUserMsgEntity::getEndProvinceCode).collect(Collectors.toSet());
        Set<String> endCityCodes2 = ccUserMsgEntities.stream().map(CcUserMsgEntity::getEndCityCode).collect(Collectors.toSet());
        Set<String> endCityCodes3 = ccUserMsgEntities.stream().map(CcUserMsgEntity::getEndAreaCode).collect(Collectors.toSet());
        Set<String> set = new HashSet<>();
        set.addAll(startCityCodes1);
        set.addAll(startCityCodes2);
        set.addAll(startCityCodes3);
        set.addAll(endCityCodes1);
        set.addAll(endCityCodes2);
        set.addAll(endCityCodes3);
        return findCityEntity(new ArrayList<>(set));
    }

}
