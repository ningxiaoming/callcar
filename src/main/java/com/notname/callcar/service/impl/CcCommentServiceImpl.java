package com.notname.callcar.service.impl;

import com.notname.callcar.aop.ControllerLog;
import com.notname.callcar.common.enums.CcBizTypeEnum;
import com.notname.callcar.common.enums.DeletedEnum;
import com.notname.callcar.common.exception.CcBizException;
import com.notname.callcar.common.exception.CcErrorCodeEnum;
import com.notname.callcar.config.VOConverter;
import com.notname.callcar.dao.CcCommentDao;
import com.notname.callcar.dao.CcLikeDao;
import com.notname.callcar.entity.*;
import com.notname.callcar.module.req.CcCommentPageQueryReq;
import com.notname.callcar.module.res.*;
import com.notname.callcar.service.ICcCommentService;
import com.notname.callcar.utils.SnowFlakeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.ObjectUtils;

import java.util.*;
import java.util.stream.Collectors;


@Service
public class CcCommentServiceImpl extends BaseService implements ICcCommentService {


    @Autowired
    private CcCommentDao ccCommentDao;

    @Autowired
    private CcLikeDao ccLikeDao;

    @Autowired
    private SnowFlakeUtil snowFlakeUtil;



    @Override
    public PageResult<CcCommentVO> pageQuery(CcCommentPageQueryReq req) {
        Assert.notNull(req, "req is null");
        Assert.hasLength(req.getBizCode(), "BizCode is null");
        CcCommentEntityExample example = new CcCommentEntityExample();
        example.createCriteria().andBizCodeEqualTo(req.getBizCode())
                .andDeletedEqualTo(DeletedEnum.NOT_DELETE.getCode());
        example.setOrderByClause("`like_count` DESC");
        example.setLimit(req.getPageSize());
        Long offset = (long) (req.getPageNum() - 1) * req.getPageSize();
        example.setOffset(offset);
        long count = ccCommentDao.countByExample(example);
        List<CcCommentEntity> ccCommentEntities = ccCommentDao.selectByExample(example);
        PageResult<CcCommentVO> pageResult = new PageResult<>();
        pageResult.setTotal(count);
        pageResult.setPageNum(req.getPageNum());
        pageResult.setPageSize(req.getPageSize());
        if (ObjectUtils.isEmpty(ccCommentEntities)) {
            return pageResult;
        }
        List<String> commentCodes = ccCommentEntities.stream().map(CcCommentEntity::getCommentCode).collect(Collectors.toList());
        Map<String, CcLikeVO> likeEntityMap = findLikeEntity(commentCodes);
        Set<String> userCodes = ccCommentEntities.stream().map(CcCommentEntity::getCreatorCode).collect(Collectors.toSet());
        Map<String, CcUserVO> ccUserVOMap = findUserEntity(new ArrayList<>(userCodes));
        List<CcCommentVO> collect = ccCommentEntities.stream().map(VOConverter.INSTANCE::conver)
                .peek(item -> {
                    item.setLiked(!ObjectUtils.isEmpty(likeEntityMap.get(item.getCommentCode())));
                    CcUserVO ccUserVO = ccUserVOMap.get(item.getCreatorCode());
                    if (!ObjectUtils.isEmpty(ccUserVO)) {
                        item.setNickName(ccUserVO.getNickName());
                        item.setAvatarUrl(ccUserVO.getAvatarUrl());
                    }
                })
                .collect(Collectors.toList());
        pageResult.setList(collect);
        return pageResult;
    }


    @Override
    public int like(CcLikeEntity req) {
        Assert.notNull(req, "req is null");
        String currentUserCode = ControllerLog.currentUserCodeThreadLocal.get();
        Assert.hasLength(currentUserCode, "currentUserCode is null");
        Assert.hasLength(req.getBizCode(), "BizCode is null");
        CcCommentEntityExample commentExample = new CcCommentEntityExample();
        commentExample.createCriteria().andCommentCodeEqualTo(req.getBizCode());
        List<CcCommentEntity> ccCommentEntities = ccCommentDao.selectByExample(commentExample);
        Assert.notNull(ccCommentEntities, "Comment is null");
        CcCommentEntity ccCommentEntity = ccCommentEntities.get(0);
        CcCommentEntity upCcCommentEntity = new CcCommentEntity();
        Assert.notNull(ccCommentEntity, "Comment is null");
        upCcCommentEntity.setId(ccCommentEntity.getId());
        if (ObjectUtils.isEmpty(ccCommentEntity.getLikeCount())) {
            ccCommentEntity.setLikeCount(0);
        }
        CcLikeEntityExample example = new CcLikeEntityExample();
        example.createCriteria().andBizCodeEqualTo(req.getBizCode());
        example.createCriteria().andCreatorCodeEqualTo(currentUserCode);
        List<CcLikeEntity> ccLikeEntities = ccLikeDao.selectByExample(example);
        if (ObjectUtils.isEmpty(ccLikeEntities)) {
            req.setLikeCode(snowFlakeUtil.snowflakeStringId());
            req.setBizTypeCode(CcBizTypeEnum.COMMENT.getCode());
            Date date = new Date();
            req.setGmtCreate(date);
            req.setGmtModified(date);
            req.setDeleted(0);
            req.setCreatorCode(currentUserCode);
            ccLikeDao.insert(req);
            upCcCommentEntity.setLikeCount(ccCommentEntity.getLikeCount() + 1);
        } else {
            CcLikeEntity ccLikeEntity = ccLikeEntities.get(0);
            if (DeletedEnum.DELETE.getCode().equals(ccLikeEntity.getDeleted())){
                upCcCommentEntity.setLikeCount(ccCommentEntity.getLikeCount() + 1);
                ccLikeEntity.setDeleted(DeletedEnum.NOT_DELETE.getCode());
                ccLikeDao.updateByPrimaryKeySelective(ccLikeEntity);
            }else {
                upCcCommentEntity.setLikeCount(ccCommentEntity.getLikeCount() - 1);
                ccLikeEntity.setDeleted(DeletedEnum.DELETE.getCode());
                ccLikeDao.updateByPrimaryKeySelective(ccLikeEntity);
            }
        }
        return ccCommentDao.updateByPrimaryKeySelective(upCcCommentEntity);
    }

    @Override
    public int remove(String commentCode) {
        String currentUserCode = ControllerLog.currentUserCodeThreadLocal.get();
        Assert.hasLength(currentUserCode, "currentUserCode is null");
        Assert.hasLength(commentCode, "commentCode is null");
        CcCommentEntityExample example = new CcCommentEntityExample();
        example.createCriteria().andCommentCodeEqualTo(commentCode).andDeletedEqualTo(DeletedEnum.NOT_DELETE.getCode());
        List<CcCommentEntity> ccCommentEntities = ccCommentDao.selectByExample(example);
        Assert.notEmpty(ccCommentEntities, "Comment is null");
        CcCommentEntity ccCommentEntity = ccCommentEntities.get(0);
        if (!currentUserCode.equals(ccCommentEntity.getCreatorCode())) {
            throw new CcBizException(CcErrorCodeEnum.NO_PERMISSION);
        }
        ccCommentEntity.setDeleted(DeletedEnum.DELETE.getCode());
        // 评论count --
        if (ccCommentEntity.getBizTypeCode().equals(CcBizTypeEnum.HEAD_LINE.getCode())){
            headLineCommentDownDown(ccCommentEntity.getBizCode());
        }
        return ccCommentDao.updateByPrimaryKeySelective(ccCommentEntity);
    }
}
