package com.notname.callcar.service.impl;

import com.notname.callcar.aop.ControllerLog;
import com.notname.callcar.common.enums.CcActivityStatusEnum;
import com.notname.callcar.common.enums.CcActivityTypeEnum;
import com.notname.callcar.common.enums.DeletedEnum;
import com.notname.callcar.config.VOConverter;
import com.notname.callcar.dao.CcActivityDao;
import com.notname.callcar.entity.CcActivityEntity;
import com.notname.callcar.entity.CcActivityEntityExample;
import com.notname.callcar.module.req.CcActivityPageQueryReq;
import com.notname.callcar.module.res.CcActivityVO;
import com.notname.callcar.module.res.GlobalConfigVO;
import com.notname.callcar.module.res.PageResult;
import com.notname.callcar.service.ICcActivityService;
import com.notname.callcar.utils.SnowFlakeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.ObjectUtils;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;



@Service
public class CcActivityServiceImpl extends BaseService implements ICcActivityService {

    @Autowired
    private CcActivityDao ccActivityDao;

    @Autowired
    private SnowFlakeUtil snowFlakeUtil;


    @Override
    public int save(CcActivityEntity req) {
        String currentUserCode = ControllerLog.currentUserCodeThreadLocal.get();
        Assert.notNull(req, "req is null");
        Assert.hasLength(currentUserCode, "currentUserCode is null");
        Assert.hasLength(req.getShowText(), "ShowText is null");
        Assert.hasLength(req.getImgUrl(), "ImgUrl is null");
        req.setActivityCode(snowFlakeUtil.snowflakeStringId());
        req.setCreatorCode(currentUserCode);
        req.setBizType(CcActivityTypeEnum.YI_QING.getCode());
        req.setDeleted(0);
        req.setActivityStatus(CcActivityStatusEnum.DOWN.getCode());
        Date date = new Date();
        req.setGmtCreate(date);
        req.setGmtModified(date);
        return ccActivityDao.insert(req);
    }

    @Override
    public int modify(CcActivityEntity req) {
        String currentUserCode = ControllerLog.currentUserCodeThreadLocal.get();
        Assert.notNull(req, "req is null");
        Assert.notNull(req.getId(), "req is null");
        Assert.hasLength(currentUserCode, "currentUserCode is null");
        CcActivityEntity ccActivityEntity = ccActivityDao.selectByPrimaryKey(req.getId());
        Assert.notNull(ccActivityEntity, "req is null");
        CcActivityEntity upCcActivityEntity = new CcActivityEntity();
        upCcActivityEntity.setId(req.getId());
        upCcActivityEntity.setGmtModified(new Date());
        if (!ObjectUtils.isEmpty(req.getImgUrl())){
            upCcActivityEntity.setImgUrl(req.getImgUrl());
        }
        if (!ObjectUtils.isEmpty(req.getShowText())){
            upCcActivityEntity.setShowText(req.getShowText());
        }
        return ccActivityDao.updateByPrimaryKeySelective(upCcActivityEntity);
    }

    @Override
    public int upOrDown(Long id) {
        String currentUserCode = ControllerLog.currentUserCodeThreadLocal.get();
        Assert.notNull(id, "req is null");
        Assert.hasLength(currentUserCode, "currentUserCode is null");
        CcActivityEntity ccActivityEntity = ccActivityDao.selectByPrimaryKey(id);
        Assert.notNull(ccActivityEntity, "ccActivityEntity is null");
        CcActivityEntity upCcActivityEntity = new CcActivityEntity();
        upCcActivityEntity.setId(id);
        upCcActivityEntity.setGmtModified(new Date());
        if (CcActivityStatusEnum.DOWN.getCode().equals(ccActivityEntity.getActivityStatus())){
            upCcActivityEntity.setActivityStatus(CcActivityStatusEnum.UP.getCode());
        }else {
            upCcActivityEntity.setActivityStatus(CcActivityStatusEnum.DOWN.getCode());
        }
        return ccActivityDao.updateByPrimaryKeySelective(upCcActivityEntity);
    }

    @Override
    public int remove(Long id) {
        String currentUserCode = ControllerLog.currentUserCodeThreadLocal.get();
        Assert.notNull(id, "req is null");
        Assert.hasLength(currentUserCode, "currentUserCode is null");
        CcActivityEntity ccActivityEntity = ccActivityDao.selectByPrimaryKey(id);
        Assert.notNull(ccActivityEntity, "ccActivityEntity is null");
        CcActivityEntity upCcActivityEntity = new CcActivityEntity();
        upCcActivityEntity.setId(id);
        upCcActivityEntity.setGmtModified(new Date());
        upCcActivityEntity.setDeleted(DeletedEnum.DELETE.getCode());
        return ccActivityDao.updateByPrimaryKeySelective(upCcActivityEntity);
    }

    @Override
    public PageResult<CcActivityVO> pageQuery(CcActivityPageQueryReq req) {
        Assert.notNull(req, "req is null");
        Assert.hasLength(req.getBizType(), "currentUserCode is null");
        CcActivityTypeEnum ccActivityTypeEnum = CcActivityTypeEnum.valueOf(req.getBizType());
        Assert.notNull(ccActivityTypeEnum, "BizType is null");
        CcActivityEntityExample example = new CcActivityEntityExample();
        example.createCriteria()
                .andBizTypeEqualTo(ccActivityTypeEnum.getCode())
                .andActivityStatusEqualTo(CcActivityStatusEnum.UP.getCode())
                .andDeletedEqualTo(DeletedEnum.NOT_DELETE.getCode());
        example.setOrderByClause("`gmt_modified` DESC");
        example.setLimit(req.getPageSize());
        Long offset = (long) (req.getPageNum() - 1) * req.getPageSize();
        example.setOffset(offset);
        long count = ccActivityDao.countByExample(example);
        List<CcActivityEntity> ccActivityEntities = ccActivityDao.selectByExample(example);
        PageResult<CcActivityVO> pageResult = new PageResult<>();
        pageResult.setTotal(count);
        pageResult.setPageNum(req.getPageNum());
        pageResult.setPageSize(req.getPageSize());
        if (ObjectUtils.isEmpty(ccActivityEntities)) {
            return pageResult;
        }
        List<CcActivityVO> collect = ccActivityEntities.stream().map(VOConverter.INSTANCE::conver).collect(Collectors.toList());
        pageResult.setList(collect);
        return pageResult;
    }

    @Override
    public GlobalConfigVO queryConfig() {
        CcActivityEntityExample example = new CcActivityEntityExample();
        example.createCriteria()
                .andBizTypeEqualTo(CcActivityTypeEnum.YI_QING.getCode())
                .andActivityStatusEqualTo(CcActivityStatusEnum.UP.getCode())
                .andDeletedEqualTo(DeletedEnum.NOT_DELETE.getCode());
        example.setOrderByClause("`gmt_modified` DESC");
        List<CcActivityEntity> ccActivityEntities = ccActivityDao.selectByExample(example);
        List<CcActivityVO> collect = ccActivityEntities.stream().map(VOConverter.INSTANCE::conver).collect(Collectors.toList());
        GlobalConfigVO globalConfigVO = new GlobalConfigVO();
        globalConfigVO.setCcActivityVOS(collect);
        return globalConfigVO;
    }
}
