package com.notname.callcar.service.impl;

import com.notname.callcar.aop.ControllerLog;
import com.notname.callcar.common.enums.CcCollectionBizTypeEnum;
import com.notname.callcar.common.enums.DeletedEnum;
import com.notname.callcar.common.exception.CcBizException;
import com.notname.callcar.common.exception.CcErrorCodeEnum;
import com.notname.callcar.config.VOConverter;
import com.notname.callcar.dao.CcCollectionDao;
import com.notname.callcar.entity.CcCollectionEntity;
import com.notname.callcar.entity.CcCollectionEntityExample;
import com.notname.callcar.module.req.CcCollectionPageQueryReq;
import com.notname.callcar.module.req.CcCollectionSaveReq;
import com.notname.callcar.module.res.*;
import com.notname.callcar.service.ICcCollectionService;
import com.notname.callcar.utils.SnowFlakeUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.ObjectUtils;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service
@Slf4j
public class CcCollectionServiceImpl extends BaseService implements ICcCollectionService {

    @Autowired
    private CcCollectionDao ccCollectionDao;

    @Autowired
    private SnowFlakeUtil snowFlakeUtil;

    @Override
    public PageResult<CcCollectionVO> pageQuery(CcCollectionPageQueryReq req) {
        String currentUserCode = ControllerLog.currentUserCodeThreadLocal.get();
        Assert.hasLength(currentUserCode, "currentUserCode is null");
        CcCollectionEntityExample example = new CcCollectionEntityExample();
        CcCollectionEntityExample.Criteria criteria = example.createCriteria();
        criteria.andCreateCodeEqualTo(currentUserCode);
        if (!ObjectUtils.isEmpty(req) && ObjectUtils.isEmpty(req.getBizType())) {
            CcCollectionBizTypeEnum ccCollectionBizTypeEnum = CcCollectionBizTypeEnum.valueOf(req.getBizType());
            criteria.andBizTypeCodeEqualTo(ccCollectionBizTypeEnum.getCode());
        }
        example.setOrderByClause("`gmt_create` DESC");
        example.setLimit(req.getPageSize());
        Long offset = (long) (req.getPageNum() - 1) * req.getPageSize();
        example.setOffset(offset);
        long count = ccCollectionDao.countByExample(example);
        PageResult<CcCollectionVO> pageResult = new PageResult<>();
        pageResult.setTotal(count);
        pageResult.setPageNum(req.getPageNum());
        pageResult.setPageSize(req.getPageSize());
        List<CcCollectionEntity> ccCollectionEntities = ccCollectionDao.selectByExample(example);
        if (ObjectUtils.isEmpty(ccCollectionEntities)) {
            return pageResult;
        }
        List<CcCollectionVO> ccCollectionVOList = ccCollectionEntities.stream().map(VOConverter.INSTANCE::conver).collect(Collectors.toList());
        List<String> umCode = ccCollectionEntities.stream().filter(item -> item.getBizTypeCode().equals(CcCollectionBizTypeEnum.USER_MSG.getCode())).map(CcCollectionEntity::getBizCode).collect(Collectors.toList());
        Map<String, CcUserMsgVO> userMsgEntity = findUserMsgEntity(umCode);
        List<String> dmCode = ccCollectionEntities.stream().filter(item -> item.getBizTypeCode().equals(CcCollectionBizTypeEnum.DRIVER_MSG.getCode())).map(CcCollectionEntity::getBizCode).collect(Collectors.toList());
        Map<String, CcDriverMsgVO> driverMsgEntity = findDriverMsgEntity(dmCode);
        List<String> hlCode = ccCollectionEntities.stream().filter(item -> item.getBizTypeCode().equals(CcCollectionBizTypeEnum.HEAD_LINE.getCode())).map(CcCollectionEntity::getBizCode).collect(Collectors.toList());
        Map<String, CcHeadLineVO> headLineEntity = findHeadLineEntity(hlCode);
        for (CcCollectionVO ccCollectionVO : ccCollectionVOList) {
            String bizCode = ccCollectionVO.getBizCode();
            if (!ObjectUtils.isEmpty(userMsgEntity.get(bizCode))) {
                ccCollectionVO.setCcUserMsgVO(userMsgEntity.get(bizCode));
                continue;
            }
            if (!ObjectUtils.isEmpty(driverMsgEntity.get(bizCode))) {
                ccCollectionVO.setCcDriverMsgVO(driverMsgEntity.get(bizCode));
                continue;
            }
            if (!ObjectUtils.isEmpty(headLineEntity.get(bizCode))) {
                ccCollectionVO.setCcHeadLineVO(headLineEntity.get(bizCode));
            }
        }
        pageResult.setList(ccCollectionVOList);
        return pageResult;
    }

    @Override
    public int save(CcCollectionSaveReq req) {
        String currentUserCode = ControllerLog.currentUserCodeThreadLocal.get();
        Assert.hasLength(currentUserCode, "currentUserCode is null");
        Assert.notNull(req, "req is null");
        Assert.hasLength(req.getBizCode(), "BizCode is null");
        Assert.hasLength(req.getBizType(), "BizType is null");
        CcCollectionBizTypeEnum ccCollectionBizTypeEnum = CcCollectionBizTypeEnum.valueOf(req.getBizType());
        Assert.notNull(ccCollectionBizTypeEnum, "BizType illegal");
        req.setCollectionCode(snowFlakeUtil.snowflakeStringId());
        req.setBizTypeCode(ccCollectionBizTypeEnum.getCode());
        req.setCreateCode(currentUserCode);
        req.setDeleted(DeletedEnum.NOT_DELETE.getCode());
        Date date = new Date();
        req.setGmtCreate(date);
        req.setGmtModified(date);
        return ccCollectionDao.insert(req);
    }

    @Override
    public int remove(String collectionCode) {
        String currentUserCode = ControllerLog.currentUserCodeThreadLocal.get();
        Assert.hasLength(currentUserCode, "currentUserCode is null");
        Assert.hasLength(collectionCode, "commentCode is null");
        CcCollectionEntityExample example = new CcCollectionEntityExample();
        example.createCriteria().andCollectionCodeEqualTo(collectionCode);
        List<CcCollectionEntity> ccCollectionEntities = ccCollectionDao.selectByExample(example);
        Assert.notEmpty(ccCollectionEntities, "Collection is null");
        CcCollectionEntity ccCollectionEntity = ccCollectionEntities.get(0);
        if (!currentUserCode.equals(ccCollectionEntity.getCreateCode())) {
            throw new CcBizException(CcErrorCodeEnum.NO_PERMISSION);
        }
        return ccCollectionDao.deleteByPrimaryKey(ccCollectionEntity.getId());
    }

}
