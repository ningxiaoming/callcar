package com.notname.callcar.service.impl;

import com.alibaba.fastjson.JSON;
import com.notname.callcar.aop.ControllerLog;
import com.notname.callcar.common.enums.CcBizTypeEnum;
import com.notname.callcar.common.enums.DeletedEnum;
import com.notname.callcar.common.exception.CcBizException;
import com.notname.callcar.common.exception.CcErrorCodeEnum;
import com.notname.callcar.config.VOConverter;
import com.notname.callcar.dao.CcCommentDao;
import com.notname.callcar.dao.CcHeadLineDao;
import com.notname.callcar.dao.CcLikeDao;
import com.notname.callcar.entity.*;
import com.notname.callcar.infrastructure.module.gaode.AddressComponent;
import com.notname.callcar.infrastructure.module.gaode.RegeoCode;
import com.notname.callcar.module.req.CcCommentSaveReq;
import com.notname.callcar.module.req.CcHeadLineQueryReq;
import com.notname.callcar.module.req.CcHeadLineSaveReq;
import com.notname.callcar.module.req.LonLatVO;
import com.notname.callcar.module.res.CcHeadLineVO;
import com.notname.callcar.module.res.CcLikeVO;
import com.notname.callcar.module.res.CcUserVO;
import com.notname.callcar.module.res.PageResult;
import com.notname.callcar.service.ICcHeadLineService;
import com.notname.callcar.utils.DateUtils;
import com.notname.callcar.utils.SnowFlakeUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.ObjectUtils;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;


@Service
@Slf4j
public class CcHeadLineServiceImpl extends BaseService implements ICcHeadLineService {

    @Autowired
    private CcHeadLineDao ccHeadLineDao;

    @Autowired
    private SnowFlakeUtil snowFlakeUtil;

    @Autowired
    private CcLikeDao ccLikeDao;

    @Autowired
    private CcCommentDao ccCommentDao;

    @Value("${cos.hosts}")
    private String hosts;


    @Override
    public int save(CcHeadLineSaveReq req) {
        String currentUserCode = ControllerLog.currentUserCodeThreadLocal.get();
        Assert.notNull(req, "req is null");
        req.setBizTypeCode(CcBizTypeEnum.YI_QING.getCode());
        Assert.hasLength(req.getStartProvinceCode(), "StartProvinceCode is null");
        Assert.hasLength(req.getStartCityCode(), "StartCityCode is null");
        Assert.hasLength(req.getStartAreaCode(), "StartAreaCode is null");
        Assert.hasLength(req.getEndProvinceCode(), "EndProvinceCode is null");
        Assert.hasLength(req.getEndCityCode(), "EndCityCode is null");
        Assert.hasLength(req.getEndAreaCode(), "EndAreaCode is null");
        Assert.hasLength(currentUserCode, "currentUserCode is null");
        Assert.notNull(req.getHeadLineTime(), "HeadLineTime is null");

        LonLatVO lonLatVO = ControllerLog.lonLatThreadLocal.get();
        try {
            if (!ObjectUtils.isEmpty(lonLatVO)) {
                RegeoCode address = getAddress(lonLatVO.getLon(), lonLatVO.getLat());
                if (!ObjectUtils.isEmpty(address)) {
                    AddressComponent addressComponent = address.getAddressComponent();
                    if (!ObjectUtils.isEmpty(addressComponent)) {
                        req.setPublishAddress(addressComponent.getCity().equals("[]") ? addressComponent.getProvince() : addressComponent.getCity());
                    }
                }
            }
        } catch (IOException e) {
            log.error("调用高德异常", e);
        }

        req.setHlCode(snowFlakeUtil.snowflakeStringId());
        req.setBizTypeCode(CcBizTypeEnum.YI_QING.getCode());
        req.setCreatorCode(currentUserCode);
        if (!ObjectUtils.isEmpty(req.getHlImgUrls())) {
            req.setHlUrls(JSON.toJSONString(req.getHlImgUrls()));
        }
        Date date = new Date();
        req.setGmtCreate(date);
        req.setGmtModified(date);
        req.setCommentCount(0);
        req.setLikeCount(0);
        req.setDeleted(0);
        return ccHeadLineDao.insert(req);
    }

    @Override
    public int modify(CcHeadLineSaveReq req) {
        CcHeadLineEntity ccHeadLineEntity = ccHeadLineDao.selectByPrimaryKey(req.getId());
        Assert.notNull(ccHeadLineEntity, "msg is null");
        if (!req.getCreatorCode().equals(ccHeadLineEntity.getCreatorCode())) {
            throw new CcBizException(CcErrorCodeEnum.NO_PERMISSION);
        }
        CcHeadLineEntityExample example = new CcHeadLineEntityExample();
        example.createCriteria().andIdEqualTo(req.getId());
        req.setGmtModified(new Date());
        if (!ObjectUtils.isEmpty(req.getHlImgUrls())) {
            req.setHlUrls(JSON.toJSONString(req.getHlImgUrls()));
        }
        return ccHeadLineDao.updateByExampleSelective(req, example);
    }

    @Override
    public PageResult<CcHeadLineVO> pageQuery(CcHeadLineQueryReq req) {
        CcHeadLineEntityExample example = new CcHeadLineEntityExample();
        List<CcHeadLineEntity> ccHeadLineEntities = firstGetCcHeadLineEntities(req, example);
        example.clear();
        if (ObjectUtils.isEmpty(ccHeadLineEntities)) {
            ccHeadLineEntities = secondGetCcHeadLineEntities(req, example);
            example.clear();
        }
        if (ObjectUtils.isEmpty(ccHeadLineEntities)) {
            ccHeadLineEntities = thirdGetCcHeadLineEntities(req, example);
            example.clear();
        }
        PageResult<CcHeadLineVO> pageResult = new PageResult<>();
        pageResult.setPageNum(req.getPageNum());
        pageResult.setPageSize(req.getPageSize());
        if (ObjectUtils.isEmpty(ccHeadLineEntities)) {
            return pageResult;
        }
        List<String> userCodes = ccHeadLineEntities.stream().map(CcHeadLineEntity::getCreatorCode).collect(Collectors.toList());
        Map<String, CcUserVO> ccUserVOMap = findUserEntity(userCodes);
        List<String> hlCode = ccHeadLineEntities.stream().map(CcHeadLineEntity::getHlCode).collect(Collectors.toList());
        Map<String, CcLikeVO> likeEntityMap = findLikeEntity(hlCode);
        Map<String, CcCityEntity> cityEntityMap = getCityEntityMap(ccHeadLineEntities);
        List<CcHeadLineVO> collect = ccHeadLineEntities.stream().map(VOConverter.INSTANCE::conver)
                .peek(item -> {
                    item.setCcUserVO(ccUserVOMap.get(item.getCreatorCode()));
                    if (!ObjectUtils.isEmpty(item.getHlUrls())) {
                        item.setHlImgUrls(JSON.parseArray(item.getHlUrls(), String.class).stream().map(item1 -> hosts + item1).collect(Collectors.toList()));
                    }
                    item.setLiked(!ObjectUtils.isEmpty(likeEntityMap.get(item.getHlCode())));
                    buildCityName(cityEntityMap, item);
                })
                .collect(Collectors.toList());
        pageResult.setList(collect);
        return pageResult;
    }

    // 第一次精确查询
    private List<CcHeadLineEntity> firstGetCcHeadLineEntities(CcHeadLineQueryReq req, CcHeadLineEntityExample example) {
        CcHeadLineEntityExample.Criteria criteria = example.createCriteria();
        criteria.andDeletedEqualTo(DeletedEnum.NOT_DELETE.getCode());
        if (!ObjectUtils.isEmpty(req.getStartProvinceCode())) {
            criteria.andStartProvinceCodeEqualTo(req.getStartProvinceCode());
        }
        if (!ObjectUtils.isEmpty(req.getStartCityCode())) {
            criteria.andStartCityCodeEqualTo(req.getStartCityCode());
        }
        if (!ObjectUtils.isEmpty(req.getStartAreaCode())) {
            criteria.andStartAreaCodeEqualTo(req.getStartAreaCode());
        }
        if (!ObjectUtils.isEmpty(req.getEndProvinceCode())) {
            criteria.andEndProvinceCodeEqualTo(req.getEndProvinceCode());
        }
        if (!ObjectUtils.isEmpty(req.getEndCityCode())) {
            criteria.andEndCityCodeEqualTo(req.getEndCityCode());
        }
        if (!ObjectUtils.isEmpty(req.getEndAreaCode())) {
            criteria.andEndAreaCodeEqualTo(req.getEndAreaCode());
        }
        if (ObjectUtils.isEmpty(req.getHeadLineTime())) {
            criteria.andHeadLineTimeGreaterThanOrEqualTo(DateUtils.initDateByDay().getTime() - 7 * 24 * 60 * 60 * 1000);
        } else {
            criteria.andHeadLineTimeGreaterThanOrEqualTo(req.getHeadLineTime());
        }
        example.setOrderByClause("`gmt_modified` DESC,`like_count` DESC");
        example.setLimit(req.getPageSize());
        Long offset = (long) (req.getPageNum() - 1) * req.getPageSize();
        example.setOffset(offset);
        return ccHeadLineDao.selectByExample(example);
    }

    // 第二次查询 到市
    private List<CcHeadLineEntity> secondGetCcHeadLineEntities(CcHeadLineQueryReq req, CcHeadLineEntityExample example) {
        CcHeadLineEntityExample.Criteria criteria = example.createCriteria();
        criteria.andDeletedEqualTo(DeletedEnum.NOT_DELETE.getCode());
        if (!ObjectUtils.isEmpty(req.getStartProvinceCode())) {
            criteria.andStartProvinceCodeEqualTo(req.getStartProvinceCode());
        }
        if (!ObjectUtils.isEmpty(req.getStartCityCode())) {
            criteria.andStartCityCodeEqualTo(req.getStartCityCode());
        }
        if (!ObjectUtils.isEmpty(req.getEndProvinceCode())) {
            criteria.andEndProvinceCodeEqualTo(req.getEndProvinceCode());
        }
        if (!ObjectUtils.isEmpty(req.getEndCityCode())) {
            criteria.andEndCityCodeEqualTo(req.getEndCityCode());
        }
        if (ObjectUtils.isEmpty(req.getHeadLineTime())) {
            criteria.andHeadLineTimeGreaterThanOrEqualTo(DateUtils.initDateByDay().getTime() - 15 * 24 * 60 * 60 * 1000);
        } else {
            criteria.andHeadLineTimeGreaterThanOrEqualTo(req.getHeadLineTime());
        }
        example.setOrderByClause("`gmt_modified` DESC,`like_count` DESC");
        example.setLimit(req.getPageSize());
        Long offset = (long) (req.getPageNum() - 1) * req.getPageSize();
        example.setOffset(offset);
        return ccHeadLineDao.selectByExample(example);
    }

    // 第三次查询 到省
    private List<CcHeadLineEntity> thirdGetCcHeadLineEntities(CcHeadLineQueryReq req, CcHeadLineEntityExample example) {
        CcHeadLineEntityExample.Criteria criteria = example.createCriteria();
        criteria.andDeletedEqualTo(DeletedEnum.NOT_DELETE.getCode());
        if (!ObjectUtils.isEmpty(req.getStartProvinceCode())) {
            criteria.andStartProvinceCodeEqualTo(req.getStartProvinceCode());
        }
        if (!ObjectUtils.isEmpty(req.getEndProvinceCode())) {
            criteria.andEndProvinceCodeEqualTo(req.getEndProvinceCode());
        }
        if (ObjectUtils.isEmpty(req.getHeadLineTime())) {
            criteria.andHeadLineTimeGreaterThanOrEqualTo(DateUtils.initDateByDay().getTime() - 15 * 24 * 60 * 60 * 1000);
        } else {
            criteria.andHeadLineTimeGreaterThanOrEqualTo(req.getHeadLineTime());
        }
        example.setOrderByClause("`gmt_modified` DESC,`like_count` DESC");
        example.setLimit(req.getPageSize());
        Long offset = (long) (req.getPageNum() - 1) * req.getPageSize();
        example.setOffset(offset);
        return ccHeadLineDao.selectByExample(example);
    }

    @Override
    public PageResult<CcHeadLineVO> pageQueryMyHeadline(CcHeadLineQueryReq req) {
        Assert.notNull(req, "req is null");
        String currentUserCode = ControllerLog.currentUserCodeThreadLocal.get();
        Assert.hasLength(currentUserCode, "CurrentUserCode is null");
        CcHeadLineEntityExample example = new CcHeadLineEntityExample();
        example.createCriteria()
                .andDeletedEqualTo(DeletedEnum.NOT_DELETE.getCode())
                .andCreatorCodeEqualTo(currentUserCode);
        example.setOrderByClause("`gmt_modified` DESC,`like_count` DESC");
        example.setLimit(req.getPageSize());
        Long offset = (long) (req.getPageNum() - 1) * req.getPageSize();
        example.setOffset(offset);
        long count = ccHeadLineDao.countByExample(example);
        List<CcHeadLineEntity> ccHeadLineEntities = ccHeadLineDao.selectByExample(example);
        PageResult<CcHeadLineVO> pageResult = new PageResult<>();
        pageResult.setTotal(count);
        pageResult.setPageNum(req.getPageNum());
        pageResult.setPageSize(req.getPageSize());
        if (ObjectUtils.isEmpty(pageResult)) {
            return pageResult;
        }
        List<String> userCodes = ccHeadLineEntities.stream().map(CcHeadLineEntity::getCreatorCode).collect(Collectors.toList());
        Map<String, CcUserVO> ccUserVOMap = findUserEntity(userCodes);

        Map<String, CcCityEntity> cityEntityMap = getCityEntityMap(ccHeadLineEntities);
        List<CcHeadLineVO> collect = ccHeadLineEntities.stream().map(VOConverter.INSTANCE::conver)
                .peek(item -> {
                    if (!ObjectUtils.isEmpty(item.getHlUrls())) {
                        item.setHlImgUrls(JSON.parseArray(item.getHlUrls(), String.class).stream().map(item1 -> hosts + item1).collect(Collectors.toList()));
                    }
                    item.setCcUserVO(ccUserVOMap.get(item.getCreatorCode()));
                    buildCityName(cityEntityMap, item);
                })
                .collect(Collectors.toList());
        pageResult.setList(collect);
        return pageResult;
    }

    @Override
    public int like(CcLikeEntity req) {
        Assert.notNull(req, "req is null");
        Assert.hasLength(req.getBizCode(), "BizCode is null");
        String currentUserCode = ControllerLog.currentUserCodeThreadLocal.get();
        Assert.hasLength(currentUserCode, "currentUserCode is null");
        CcHeadLineEntityExample glExample = new CcHeadLineEntityExample();
        glExample.createCriteria().andHlCodeEqualTo(req.getBizCode());
        List<CcHeadLineEntity> ccHeadLineEntities = ccHeadLineDao.selectByExample(glExample);
        Assert.notEmpty(ccHeadLineEntities, "HeadLine is null");
        CcHeadLineEntity ccHeadLineEntity = ccHeadLineEntities.get(0);
        CcHeadLineEntity upCcHeadLineEntity = new CcHeadLineEntity();
        Assert.notNull(ccHeadLineEntity, "HeadLine is null");
        upCcHeadLineEntity.setId(ccHeadLineEntity.getId());
        if (ObjectUtils.isEmpty(ccHeadLineEntity.getLikeCount())) {
            ccHeadLineEntity.setLikeCount(0);
        }
        CcLikeEntityExample example = new CcLikeEntityExample();
        example.createCriteria().andBizCodeEqualTo(req.getBizCode()).andCreatorCodeEqualTo(currentUserCode);
        List<CcLikeEntity> ccLikeEntities = ccLikeDao.selectByExample(example);
        if (ObjectUtils.isEmpty(ccLikeEntities)) {
            req.setLikeCode(snowFlakeUtil.snowflakeStringId());
            Date date = new Date();
            req.setGmtCreate(date);
            req.setGmtModified(date);
            req.setBizTypeCode(CcBizTypeEnum.YI_QING.getCode());
            req.setDeleted(0);
            req.setCreatorCode(currentUserCode);
            ccLikeDao.insert(req);
            upCcHeadLineEntity.setLikeCount(ccHeadLineEntity.getLikeCount() + 1);
        } else {
            CcLikeEntity ccLikeEntity = ccLikeEntities.get(0);
            if (DeletedEnum.DELETE.getCode().equals(ccLikeEntity.getDeleted())) {
                upCcHeadLineEntity.setLikeCount(ccHeadLineEntity.getLikeCount() + 1);
                ccLikeEntity.setDeleted(DeletedEnum.NOT_DELETE.getCode());
                ccLikeDao.updateByPrimaryKeySelective(ccLikeEntity);
            } else {
                upCcHeadLineEntity.setLikeCount(ccHeadLineEntity.getLikeCount() - 1);
                ccLikeEntity.setDeleted(DeletedEnum.DELETE.getCode());
                ccLikeDao.updateByPrimaryKeySelective(ccLikeEntity);
            }
        }
        return ccHeadLineDao.updateByPrimaryKeySelective(upCcHeadLineEntity);
    }

    @Override
    public String comment(CcCommentSaveReq req) {
        Assert.notNull(req, "req is null");
        CcBizTypeEnum ccBizTypeEnum = CcBizTypeEnum.valueOf(req.getBizType());
        Assert.notNull(ccBizTypeEnum, "BizType is null");
        req.setBizTypeCode(ccBizTypeEnum.getCode());
        Assert.notNull(req.getBizCode(), "BizType is null");
        Assert.hasLength(req.getContent(), "Content is null");
        String currentUserCode = ControllerLog.currentUserCodeThreadLocal.get();
        Assert.hasLength(currentUserCode, "currentUserCode is null");
        CcHeadLineEntityExample glExample = new CcHeadLineEntityExample();
        glExample.createCriteria().andHlCodeEqualTo(req.getBizCode());
        List<CcHeadLineEntity> ccHeadLineEntities = ccHeadLineDao.selectByExample(glExample);
        Assert.notNull(ccHeadLineEntities, "HeadLine is null");
        CcHeadLineEntity ccHeadLineEntity = ccHeadLineEntities.get(0);
        CcHeadLineEntity upCcHeadLineEntity = new CcHeadLineEntity();
        Assert.notNull(ccHeadLineEntity, "HeadLine is null");
        upCcHeadLineEntity.setId(ccHeadLineEntity.getId());
        if (ObjectUtils.isEmpty(ccHeadLineEntity.getCommentCount())) {
            ccHeadLineEntity.setCommentCount(0);
        }
        upCcHeadLineEntity.setCommentCount(ccHeadLineEntity.getCommentCount() + 1);
        String code = addComment(req);
        ccHeadLineDao.updateByPrimaryKeySelective(upCcHeadLineEntity);
        return code;
    }

    @Override
    public int remove(String hlCode) {
        String currentUserCode = ControllerLog.currentUserCodeThreadLocal.get();
        Assert.hasLength(currentUserCode, "currentUserCode is null");
        Assert.hasLength(hlCode, "hlCode is null");
        CcHeadLineEntityExample example = new CcHeadLineEntityExample();
        example.createCriteria().andHlCodeEqualTo(hlCode).andDeletedEqualTo(DeletedEnum.NOT_DELETE.getCode());
        List<CcHeadLineEntity> ccHeadLineEntities = ccHeadLineDao.selectByExample(example);
        Assert.notEmpty(ccHeadLineEntities, "HeadLine is null");
        CcHeadLineEntity ccHeadLineEntity = ccHeadLineEntities.get(0);
        if (!currentUserCode.equals(ccHeadLineEntity.getCreatorCode())) {
            throw new CcBizException(CcErrorCodeEnum.NO_PERMISSION);
        }
        ccHeadLineEntity.setDeleted(DeletedEnum.DELETE.getCode());
        return ccHeadLineDao.updateByPrimaryKeySelective(ccHeadLineEntity);
    }


    private String addComment(CcCommentEntity req) {
        String code = snowFlakeUtil.snowflakeStringId();
        req.setCommentCode(code);
        req.setCreatorCode(ControllerLog.currentUserCodeThreadLocal.get());
        req.setBizTypeCode(CcBizTypeEnum.HEAD_LINE.getCode());
        req.setDeleted(0);
        Date date = new Date();
        req.setGmtCreate(date);
        req.setGmtModified(date);
        req.setLikeCount(0);
        ccCommentDao.insert(req);
        return code;
    }


    private void buildCityName(Map<String, CcCityEntity> cityEntityMap, CcHeadLineVO item) {
        item.setStartProvinceName(cityEntityMap.get(item.getStartProvinceCode()) == null ? "" : cityEntityMap.get(item.getStartProvinceCode()).getCityName());
        item.setStartCityName(cityEntityMap.get(item.getStartCityCode()) == null ? "" : cityEntityMap.get(item.getStartCityCode()).getCityName());
        item.setStartAreaName(cityEntityMap.get(item.getStartAreaCode()) == null ? "" : cityEntityMap.get(item.getStartAreaCode()).getCityName());
        item.setEndProvinceName(cityEntityMap.get(item.getEndProvinceCode()) == null ? "" : cityEntityMap.get(item.getEndProvinceCode()).getCityName());
        item.setEndCityName(cityEntityMap.get(item.getEndCityCode()) == null ? "" : cityEntityMap.get(item.getEndCityCode()).getCityName());
        item.setEndAreaName(cityEntityMap.get(item.getEndAreaCode()) == null ? "" : cityEntityMap.get(item.getEndAreaCode()).getCityName());
    }

    private Map<String, CcCityEntity> getCityEntityMap(List<CcHeadLineEntity> ccHeadLineEntities) {
        Set<String> startCityCodes1 = ccHeadLineEntities.stream().map(CcHeadLineEntity::getStartProvinceCode).collect(Collectors.toSet());
        Set<String> startCityCodes2 = ccHeadLineEntities.stream().map(CcHeadLineEntity::getStartCityCode).collect(Collectors.toSet());
        Set<String> startCityCodes3 = ccHeadLineEntities.stream().map(CcHeadLineEntity::getStartAreaCode).collect(Collectors.toSet());
        Set<String> endCityCodes1 = ccHeadLineEntities.stream().map(CcHeadLineEntity::getEndProvinceCode).collect(Collectors.toSet());
        Set<String> endCityCodes2 = ccHeadLineEntities.stream().map(CcHeadLineEntity::getEndCityCode).collect(Collectors.toSet());
        Set<String> endCityCodes3 = ccHeadLineEntities.stream().map(CcHeadLineEntity::getEndAreaCode).collect(Collectors.toSet());
        Set<String> set = new HashSet<>();
        set.addAll(startCityCodes1);
        set.addAll(startCityCodes2);
        set.addAll(startCityCodes3);
        set.addAll(endCityCodes1);
        set.addAll(endCityCodes2);
        set.addAll(endCityCodes3);
        return findCityEntity(new ArrayList<>(set));
    }


}
