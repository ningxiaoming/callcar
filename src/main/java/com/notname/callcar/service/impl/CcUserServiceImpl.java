package com.notname.callcar.service.impl;

import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.bean.WxMaJscode2SessionResult;
import cn.binarywang.wx.miniapp.bean.WxMaPhoneNumberInfo;
import com.alibaba.fastjson.JSON;
import com.notname.callcar.aop.ControllerLog;
import com.notname.callcar.common.enums.CcUserCompletedEnum;
import com.notname.callcar.common.enums.DeletedEnum;
import com.notname.callcar.common.exception.CcBizException;
import com.notname.callcar.common.exception.CcErrorCodeEnum;
import com.notname.callcar.config.VOConverter;
import com.notname.callcar.dao.CcUserDao;
import com.notname.callcar.entity.CcUserEntity;
import com.notname.callcar.entity.CcUserEntityExample;
import com.notname.callcar.module.req.WxLoginReq;
import com.notname.callcar.module.res.CcUserVO;
import com.notname.callcar.service.ICcUserService;
import com.notname.callcar.utils.PhoneEncryptUtil;
import com.notname.callcar.utils.SnowFlakeUtil;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.error.WxErrorException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;


@Service
@Slf4j
public class CcUserServiceImpl implements ICcUserService {

    @Autowired
    private CcUserDao ccUserDao;

    @Autowired
    private SnowFlakeUtil snowFlakeUtil;

    @Autowired
    private WxMaService wxMaService;

    @Value("${cos.hosts}")
    private String hosts;


    @Override
    public int save(CcUserEntity req) {
        Assert.notNull(req, "req is null");
        Assert.hasLength(req.getOpenId(), "OpenId is null");
        req.setUserCode(snowFlakeUtil.snowflakeStringId());
        Date date = new Date();
        req.setGmtCreate(date);
        req.setGmtModified(date);
        req.setDeleted(0);
        req.setIsLocked(0);
        return ccUserDao.insert(req);
    }

    @Override
    public CcUserEntity login(WxLoginReq req) {
        Assert.notNull(req, "code is null");
        Assert.hasLength(req.getLoginCode(), "LoginCode is null");
        WxMaJscode2SessionResult session;
        try {
            session = wxMaService.getUserService().getSessionInfo(req.getLoginCode());
            log.info("微信返回openId:{}", JSON.toJSONString(session));
        } catch (WxErrorException e) {
            log.error("调用微信获取信息(openId)失败,", e);
            throw new CcBizException(CcErrorCodeEnum.WX_SYSTEM_ERROR);
        }
        Assert.notNull(session, "session is null");
        CcUserEntityExample example = new CcUserEntityExample();
        example.createCriteria().andOpenIdEqualTo(session.getOpenid());
        List<CcUserEntity> ccUserEntities = ccUserDao.selectByExample(example);
        if (ObjectUtils.isEmpty(ccUserEntities)) {
            Assert.hasLength(req.getPhoneCode(), "PhoneCode is null");
            WxMaPhoneNumberInfo newPhoneNoInfo;
            try {
                newPhoneNoInfo = wxMaService.getUserService().getNewPhoneNoInfo(req.getPhoneCode());
                log.info("微信返回手机号码:{}", JSON.toJSONString(newPhoneNoInfo));
            } catch (WxErrorException e) {
                log.error("获取用户手机号异常", e);
                throw new CcBizException(CcErrorCodeEnum.WX_SYSTEM_ERROR);
            }
            CcUserEntity ccUserEntity = new CcUserEntity();
            String encryptBase64 = PhoneEncryptUtil.encryptBase64(newPhoneNoInfo.getPurePhoneNumber());

            int random = (int) (Math.random() * 80) + 1;
            ccUserEntity.setAvatarUrl("callcar/avatar/" + random + ".jpeg");
            ccUserEntity.setNickName("微信用户" + newPhoneNoInfo.getPurePhoneNumber().substring(7));
            ccUserEntity.setCompleted(CcUserCompletedEnum.NOT_COMPLETED.getCode());
            ccUserEntity.setPhone(encryptBase64);
            ccUserEntity.setUserCode(snowFlakeUtil.snowflakeStringId());
            ccUserEntity.setOpenId(session.getOpenid());
            ccUserEntity.setUnionId(session.getUnionid());
            Date date = new Date();
            ccUserEntity.setGmtCreate(date);
            ccUserEntity.setGmtModified(date);
            ccUserEntity.setDeleted(0);
            ccUserEntity.setIsLocked(0);
            ccUserDao.insert(ccUserEntity);
            ccUserEntity.setPhone(PhoneEncryptUtil.mobileEncrypt(ccUserEntity.getPhone()));
            ccUserEntity.setAvatarUrl(hosts + ccUserEntity.getAvatarUrl());
            return ccUserEntity;
        }
        CcUserEntity ccUserEntity = ccUserEntities.get(0);
        ccUserEntity.setPhone(PhoneEncryptUtil.mobileEncrypt(ccUserEntity.getPhone()));
        if (!ObjectUtils.isEmpty(ccUserEntity.getAvatarUrl())) {
            ccUserEntity.setAvatarUrl(hosts + ccUserEntity.getAvatarUrl());
        }

        return ccUserEntity;
    }

    @Override
    public int completion(CcUserEntity req) {
        String currentUserCode = ControllerLog.currentUserCodeThreadLocal.get();
        Assert.notNull(req, "req is null");
        Assert.hasLength(currentUserCode, "currentUserCode is null");
        Assert.hasLength(req.getNickName(), "NickName is null");
        Assert.hasLength(req.getAvatarUrl(), "AvatarUrl is null");
        CcUserVO ccUserVO = queryByUserCode(currentUserCode);
        CcUserEntity ccUserEntity = new CcUserEntity();
        ccUserEntity.setId(ccUserVO.getId());
        if (!ObjectUtils.isEmpty(req.getAge())) {
            ccUserEntity.setAge(req.getAge());
        }
        if (!ObjectUtils.isEmpty(req.getGender())) {
            ccUserEntity.setGender(req.getGender());
        }
        if (!ObjectUtils.isEmpty(req.getUseLanguage())) {
            ccUserEntity.setUseLanguage(req.getUseLanguage());
        }
        if (!ObjectUtils.isEmpty(req.getNickName())) {
            ccUserEntity.setNickName(req.getNickName());
        }
        ccUserEntity.setCompleted(CcUserCompletedEnum.COMPLETED.getCode());
        return ccUserDao.updateByPrimaryKeySelective(ccUserEntity);
    }

    @Override
    public String decryptPhone(String phoneStr) {
        Assert.hasLength(phoneStr, "phoneStr is null");
        return PhoneEncryptUtil.decryptStr(phoneStr);
    }

    @Override
    public int modify(CcUserEntity req) {
        return 0;
    }

    @Override
    public CcUserVO queryByCode() {
        String currentUserCode = ControllerLog.currentUserCodeThreadLocal.get();
        Assert.hasLength(currentUserCode, "UserCode is null");
        CcUserVO ccUserVO = queryByUserCode(currentUserCode);
        if (!ObjectUtils.isEmpty(ccUserVO.getAvatarUrl())) {
            ccUserVO.setAvatarUrl(hosts + ccUserVO.getAvatarUrl());
        }
        return ccUserVO;
    }

    public CcUserVO queryByUserCode(String userCode) {
        Assert.hasLength(userCode, "userCode is null");
        CcUserEntityExample example = new CcUserEntityExample();
        example.createCriteria().andUserCodeEqualTo(userCode).andDeletedEqualTo(DeletedEnum.NOT_DELETE.getCode());
        List<CcUserEntity> ccUserEntities = ccUserDao.selectByExample(example);
        if (ObjectUtils.isEmpty(ccUserEntities)) {
            throw new CcBizException(CcErrorCodeEnum.USER_NOT_EXIST_BIZ_ERROR);
        }
        return VOConverter.INSTANCE.conver(ccUserEntities.get(0));
    }
}
