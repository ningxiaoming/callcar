package com.notname.callcar.service.impl;

import com.notname.callcar.aop.ControllerLog;
import com.notname.callcar.common.enums.DeletedEnum;
import com.notname.callcar.common.exception.CcBizException;
import com.notname.callcar.common.exception.CcErrorCodeEnum;
import com.notname.callcar.config.VOConverter;
import com.notname.callcar.dao.CcCityDao;
import com.notname.callcar.dao.CcDriverMsgDao;
import com.notname.callcar.dao.CcUserDao;
import com.notname.callcar.entity.*;
import com.notname.callcar.module.req.CcMsgPageQueryReq;
import com.notname.callcar.module.res.CcDriverMsgVO;
import com.notname.callcar.module.res.CcUserVO;
import com.notname.callcar.module.res.PageResult;
import com.notname.callcar.service.ICcDriverMsgService;
import com.notname.callcar.utils.DateUtils;
import com.notname.callcar.utils.PhoneEncryptUtil;
import com.notname.callcar.utils.SnowFlakeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.ObjectUtils;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;


@Service
public class CcDriverMsgServiceImpl extends BaseService implements ICcDriverMsgService {

    @Autowired
    private CcDriverMsgDao ccDriverMsgDao;

    @Autowired
    private SnowFlakeUtil snowFlakeUtil;


    @Override
    public int save(CcDriverMsgEntity req) {
        String currentUserCode = ControllerLog.currentUserCodeThreadLocal.get();
        Assert.notNull(req, "req is null");
        Assert.hasLength(req.getStartProvinceCode(), "StartProvinceCode is null");
        Assert.hasLength(req.getPhone(), "Phone is null");
        Assert.hasLength(req.getStartCityCode(), "StartCityCode is null");
        Assert.hasLength(req.getStartAreaCode(), "StartAreaCode is null");
        Assert.hasLength(req.getEndProvinceCode(), "EndProvinceCode is null");
        Assert.hasLength(req.getEndCityCode(), "EndCityCode is null");
        Assert.hasLength(req.getEndAreaCode(), "EndAreaCode is null");
        Assert.hasLength(currentUserCode, "currentUserCode is null");
        Assert.notNull(req.getStartDate(), "StartDate is null");
        if (req.getPhone().contains("****")) {
            CcUserEntity userEntityByCode = findUserEntityByCode(currentUserCode);
            req.setPhone(userEntityByCode.getPhone());
        }
        Assert.isTrue(PhoneEncryptUtil.isValidPhoneNumber(req.getPhone()),"非法的电话号");
        String encryptPhone = PhoneEncryptUtil.encryptBase64(req.getPhone());
        req.setPhone(encryptPhone);
        req.setDmCode(snowFlakeUtil.snowflakeStringId());
        req.setCreatorCode(currentUserCode);
        Date date = new Date();
        req.setGmtCreate(date);
        req.setGmtModified(date);
        req.setDeleted(0);
        return ccDriverMsgDao.insert(req);
    }

    @Override
    public int modify(CcDriverMsgEntity req) {
        CcDriverMsgEntity ccDriverMsgEntity = ccDriverMsgDao.selectByPrimaryKey(req.getId());
        Assert.notNull(ccDriverMsgEntity, "msg is null");
        if (!req.getCreatorCode().equals(ccDriverMsgEntity.getCreatorCode())) {
            throw new CcBizException(CcErrorCodeEnum.NO_PERMISSION);
        }
        CcDriverMsgEntityExample example = new CcDriverMsgEntityExample();
        example.createCriteria().andIdEqualTo(req.getId());
        req.setGmtModified(new Date());
        if (!ObjectUtils.isEmpty(req.getPhone())){
            req.setPhone(PhoneEncryptUtil.encryptBase64(req.getPhone()));
            Assert.isTrue(PhoneEncryptUtil.isValidPhoneNumber(req.getPhone()),"非法的电话号");
        }
        return ccDriverMsgDao.updateByExampleSelective(req, example);
    }

    @Override
    public CcDriverMsgVO queryDriverMsgByCode(String dmCode) {
        Assert.hasLength(dmCode, "biz is null");
        CcDriverMsgEntityExample example = new CcDriverMsgEntityExample();
        example.createCriteria().andDmCodeEqualTo(dmCode).andDeletedEqualTo(DeletedEnum.NOT_DELETE.getCode());
        List<CcDriverMsgEntity> ccDriverMsgEntities = ccDriverMsgDao.selectByExample(example);
        Assert.notEmpty(ccDriverMsgEntities, "DriverMsg is null");
        Map<String, CcCityEntity> cityEntityMap = getCityEntityMap(ccDriverMsgEntities);
        CcDriverMsgVO ccDriverMsgVO = VOConverter.INSTANCE.conver(ccDriverMsgEntities.get(0));
        buildCityName(cityEntityMap, ccDriverMsgVO);
        if (!ObjectUtils.isEmpty(ccDriverMsgVO.getPhone())){
            ccDriverMsgVO.setPhone(PhoneEncryptUtil.decryptStr(ccDriverMsgVO.getPhone()));
        }
        return ccDriverMsgVO;
    }

    @Override
    public PageResult<CcDriverMsgVO> pageQuery(CcMsgPageQueryReq req) {
        CcDriverMsgEntityExample example = new CcDriverMsgEntityExample();
        List<CcDriverMsgEntity> ccDriverMsgEntities = firstGetCcDriverMsgEntities(req, example);
        example.clear();
        if (ObjectUtils.isEmpty(ccDriverMsgEntities)){
            ccDriverMsgEntities = secondGetCcDriverMsgEntities(req,example);
            example.clear();
        }
        if (ObjectUtils.isEmpty(ccDriverMsgEntities)){
            ccDriverMsgEntities = thirdGetCcDriverMsgEntities(req,example);
            example.clear();
        }
        PageResult<CcDriverMsgVO> pageResult = new PageResult<>();
        pageResult.setPageNum(req.getPageNum());
        pageResult.setPageSize(req.getPageSize());
        if (ObjectUtils.isEmpty(ccDriverMsgEntities)) {
            return pageResult;
        }
        Map<String, CcCityEntity> cityEntityMap = getCityEntityMap(ccDriverMsgEntities);
        List<String> userCodes = ccDriverMsgEntities.stream().map(CcDriverMsgEntity::getCreatorCode).collect(Collectors.toList());
        Map<String, CcUserVO> ccUserVOMap = findUserEntity(userCodes);
        List<CcDriverMsgVO> collect = ccDriverMsgEntities.stream().map(VOConverter.INSTANCE::conver).peek(item -> {
            item.setCcUserVO(ccUserVOMap.get(item.getCreatorCode()));
            buildCityName(cityEntityMap, item);
        }).collect(Collectors.toList());
        pageResult.setList(collect);
        return pageResult;
    }

    // 第一次精确查询
    private List<CcDriverMsgEntity> firstGetCcDriverMsgEntities(CcMsgPageQueryReq req, CcDriverMsgEntityExample example) {
        CcDriverMsgEntityExample.Criteria criteria = example.createCriteria();
        criteria.andDeletedEqualTo(DeletedEnum.NOT_DELETE.getCode());
        if (!ObjectUtils.isEmpty(req.getStartProvinceCode())) {
            criteria.andStartProvinceCodeEqualTo(req.getStartProvinceCode());
        }
        if (!ObjectUtils.isEmpty(req.getStartCityCode())) {
            criteria.andStartCityCodeEqualTo(req.getStartCityCode());
        }
        if (!ObjectUtils.isEmpty(req.getStartAreaCode())) {
            criteria.andStartAreaCodeEqualTo(req.getStartAreaCode());
        }
        if (!ObjectUtils.isEmpty(req.getEndProvinceCode())) {
            criteria.andEndProvinceCodeEqualTo(req.getEndProvinceCode());
        }
        if (!ObjectUtils.isEmpty(req.getEndCityCode())) {
            criteria.andEndCityCodeEqualTo(req.getEndCityCode());
        }
        if (!ObjectUtils.isEmpty(req.getEndAreaCode())) {
            criteria.andEndAreaCodeEqualTo(req.getEndAreaCode());
        }
        if (ObjectUtils.isEmpty(req.getStartDate())) {
            criteria.andStartDateGreaterThanOrEqualTo(DateUtils.initDateByDay().getTime());
        } else {
            criteria.andStartDateGreaterThanOrEqualTo(req.getStartDate());
        }
        example.setOrderByClause("`gmt_modified` DESC");
        example.setLimit(req.getPageSize());
        Long offset = (long) (req.getPageNum() - 1) * req.getPageSize();
        example.setOffset(offset);
        return ccDriverMsgDao.selectByExample(example);
    }

    // 第二次查询到市
    private List<CcDriverMsgEntity> secondGetCcDriverMsgEntities(CcMsgPageQueryReq req, CcDriverMsgEntityExample example) {
        CcDriverMsgEntityExample.Criteria criteria = example.createCriteria();
        criteria.andDeletedEqualTo(DeletedEnum.NOT_DELETE.getCode());
        if (!ObjectUtils.isEmpty(req.getStartProvinceCode())) {
            criteria.andStartProvinceCodeEqualTo(req.getStartProvinceCode());
        }
        if (!ObjectUtils.isEmpty(req.getStartCityCode())) {
            criteria.andStartCityCodeEqualTo(req.getStartCityCode());
        }
        if (!ObjectUtils.isEmpty(req.getEndProvinceCode())) {
            criteria.andEndProvinceCodeEqualTo(req.getEndProvinceCode());
        }
        if (!ObjectUtils.isEmpty(req.getEndCityCode())) {
            criteria.andEndCityCodeEqualTo(req.getEndCityCode());
        }
        if (ObjectUtils.isEmpty(req.getStartDate())) {
            criteria.andStartDateGreaterThanOrEqualTo(DateUtils.initDateByDay().getTime());
        } else {
            criteria.andStartDateGreaterThanOrEqualTo(req.getStartDate());
        }
        example.setOrderByClause("`gmt_modified` DESC");
        example.setLimit(req.getPageSize());
        Long offset = (long) (req.getPageNum() - 1) * req.getPageSize();
        example.setOffset(offset);
        return ccDriverMsgDao.selectByExample(example);
    }

    // 第三次查询到省
    private List<CcDriverMsgEntity> thirdGetCcDriverMsgEntities(CcMsgPageQueryReq req, CcDriverMsgEntityExample example) {
        CcDriverMsgEntityExample.Criteria criteria = example.createCriteria();
        criteria.andDeletedEqualTo(DeletedEnum.NOT_DELETE.getCode());
        if (!ObjectUtils.isEmpty(req.getStartProvinceCode())) {
            criteria.andStartProvinceCodeEqualTo(req.getStartProvinceCode());
        }
        if (!ObjectUtils.isEmpty(req.getEndProvinceCode())) {
            criteria.andEndProvinceCodeEqualTo(req.getEndProvinceCode());
        }
        if (ObjectUtils.isEmpty(req.getStartDate())) {
            criteria.andStartDateGreaterThanOrEqualTo(DateUtils.initDateByDay().getTime());
        } else {
            criteria.andStartDateGreaterThanOrEqualTo(req.getStartDate());
        }
        example.setOrderByClause("`gmt_modified` DESC");
        example.setLimit(req.getPageSize());
        Long offset = (long) (req.getPageNum() - 1) * req.getPageSize();
        example.setOffset(offset);
        return ccDriverMsgDao.selectByExample(example);
    }

    private void buildCityName(Map<String, CcCityEntity> cityEntityMap, CcDriverMsgVO item) {
        item.setStartProvinceName(cityEntityMap.get(item.getStartProvinceCode()) == null ? "" : cityEntityMap.get(item.getStartProvinceCode()).getCityName());
        item.setStartCityName(cityEntityMap.get(item.getStartCityCode()) == null ? "" : cityEntityMap.get(item.getStartCityCode()).getCityName());
        item.setStartAreaName(cityEntityMap.get(item.getStartAreaCode()) == null ? "" : cityEntityMap.get(item.getStartAreaCode()).getCityName());
        item.setEndProvinceName(cityEntityMap.get(item.getEndProvinceCode()) == null ? "" : cityEntityMap.get(item.getEndProvinceCode()).getCityName());
        item.setEndCityName(cityEntityMap.get(item.getEndCityCode()) == null ? "" : cityEntityMap.get(item.getEndCityCode()).getCityName());
        item.setEndAreaName(cityEntityMap.get(item.getEndAreaCode()) == null ? "" : cityEntityMap.get(item.getEndAreaCode()).getCityName());
    }

    private Map<String, CcCityEntity> getCityEntityMap(List<CcDriverMsgEntity> ccDriverMsgEntities) {
        Set<String> startCityCodes1 = ccDriverMsgEntities.stream().map(CcDriverMsgEntity::getStartProvinceCode).collect(Collectors.toSet());
        Set<String> startCityCodes2 = ccDriverMsgEntities.stream().map(CcDriverMsgEntity::getStartCityCode).collect(Collectors.toSet());
        Set<String> startCityCodes3 = ccDriverMsgEntities.stream().map(CcDriverMsgEntity::getStartAreaCode).collect(Collectors.toSet());
        Set<String> endCityCodes1 = ccDriverMsgEntities.stream().map(CcDriverMsgEntity::getEndProvinceCode).collect(Collectors.toSet());
        Set<String> endCityCodes2 = ccDriverMsgEntities.stream().map(CcDriverMsgEntity::getEndCityCode).collect(Collectors.toSet());
        Set<String> endCityCodes3 = ccDriverMsgEntities.stream().map(CcDriverMsgEntity::getEndAreaCode).collect(Collectors.toSet());
        Set<String> set = new HashSet<>();
        set.addAll(startCityCodes1);
        set.addAll(startCityCodes2);
        set.addAll(startCityCodes3);
        set.addAll(endCityCodes1);
        set.addAll(endCityCodes2);
        set.addAll(endCityCodes3);
        return findCityEntity(new ArrayList<>(set));
    }


    @Override
    public PageResult<CcDriverMsgVO> pageQueryMyDriverMsg(CcMsgPageQueryReq req) {
        Assert.notNull(req, "userCode is null");
        String currentUserCode = ControllerLog.currentUserCodeThreadLocal.get();
        Assert.hasLength(currentUserCode, "CurrentUserCode is null");
        CcDriverMsgEntityExample example = new CcDriverMsgEntityExample();
        example.createCriteria().andDeletedEqualTo(DeletedEnum.NOT_DELETE.getCode()).andCreatorCodeEqualTo(currentUserCode);
        example.setOrderByClause("`gmt_modified` DESC");
        example.setLimit(req.getPageSize());
        Long offset = (long) (req.getPageNum() - 1) * req.getPageSize();
        example.setOffset(offset);
        long count = ccDriverMsgDao.countByExample(example);
        List<CcDriverMsgEntity> ccDriverMsgEntities = ccDriverMsgDao.selectByExample(example);
        PageResult<CcDriverMsgVO> pageResult = new PageResult<>();
        pageResult.setTotal(count);
        pageResult.setPageNum(req.getPageNum());
        pageResult.setPageSize(req.getPageSize());
        if (ObjectUtils.isEmpty(ccDriverMsgEntities)) {
            return pageResult;
        }
        Map<String, CcCityEntity> cityEntityMap = getCityEntityMap(ccDriverMsgEntities);
        List<String> userCodes = ccDriverMsgEntities.stream().map(CcDriverMsgEntity::getCreatorCode).collect(Collectors.toList());
        Map<String, CcUserVO> ccUserVOMap = findUserEntity(userCodes);
        List<CcDriverMsgVO> collect = ccDriverMsgEntities.stream()
                .map(VOConverter.INSTANCE::conver)
                .peek(item -> {
                    item.setCcUserVO(ccUserVOMap.get(item.getCreatorCode()));
                    item.setPhone(PhoneEncryptUtil.decryptStr(item.getPhone()));
                    buildCityName(cityEntityMap, item);
                })
                .collect(Collectors.toList());
        pageResult.setList(collect);
        return pageResult;
    }

    @Override
    public int remove(String dmCode) {
        String currentUserCode = ControllerLog.currentUserCodeThreadLocal.get();
        Assert.hasLength(currentUserCode, "currentUserCode is null");
        Assert.hasLength(dmCode, "dmCode is null");
        CcDriverMsgEntityExample example = new CcDriverMsgEntityExample();
        example.createCriteria().andDmCodeEqualTo(dmCode).andDeletedEqualTo(DeletedEnum.NOT_DELETE.getCode());
        List<CcDriverMsgEntity> ccDriverMsgEntities = ccDriverMsgDao.selectByExample(example);
        Assert.notEmpty(ccDriverMsgEntities, "DriverMsg is null");
        CcDriverMsgEntity ccDriverMsgEntity = ccDriverMsgEntities.get(0);
        if (!currentUserCode.equals(ccDriverMsgEntity.getCreatorCode())) {
            throw new CcBizException(CcErrorCodeEnum.NO_PERMISSION);
        }
        ccDriverMsgEntity.setDeleted(DeletedEnum.DELETE.getCode());
        return ccDriverMsgDao.updateByPrimaryKeySelective(ccDriverMsgEntity);
    }

}
