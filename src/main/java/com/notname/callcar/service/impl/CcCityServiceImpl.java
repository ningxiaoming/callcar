package com.notname.callcar.service.impl;

import com.notname.callcar.dao.CcCityDao;
import com.notname.callcar.entity.CcCityEntity;
import com.notname.callcar.entity.CcCityEntityExample;
import com.notname.callcar.infrastructure.module.gaode.AddressComponent;
import com.notname.callcar.infrastructure.module.gaode.RegeoCode;
import com.notname.callcar.module.req.LonLatVO;
import com.notname.callcar.module.res.CcRegionVO;
import com.notname.callcar.service.ICcCityService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.ObjectUtils;

import java.util.List;

@Service
@Slf4j
public class CcCityServiceImpl extends BaseService implements ICcCityService {

    @Autowired
    private CcCityDao ccCityDao;

    @Override
    public CcRegionVO queryReginByLonLat(LonLatVO req) {
        Assert.notNull(req, "req is null");
        Assert.hasLength(req.getLat(), "Lat is null");
        Assert.hasLength(req.getLon(), "Lon is null");
        RegeoCode address;
        CcRegionVO ccRegionVO = new CcRegionVO();
        try {
            address = getAddress(req.getLon(), req.getLat());
        } catch (Exception e) {
            log.error("调用高德接口失败");
            ccRegionVO.setProvinceCode("11e4");
            ccRegionVO.setProvinceName("北京市");
            ccRegionVO.setCityCode("110100");
            ccRegionVO.setCityName("北京市");
            ccRegionVO.setAreaCode("110105");
            ccRegionVO.setAreaName("朝阳区");
            return ccRegionVO;
        }

        AddressComponent addressComponent = address.getAddressComponent();
        ccRegionVO.setProvinceName(addressComponent.getProvince());
        ccRegionVO.setCityName(addressComponent.getCity().equals("[]") ? addressComponent.getProvince() : addressComponent.getCity());
        ccRegionVO.setAreaName(addressComponent.getDistrict());
        // 根据name 查询code
        CcCityEntityExample example = new CcCityEntityExample();
        example.createCriteria().andCityNameEqualTo(ccRegionVO.getProvinceName()).andTypeEqualTo("省");
        List<CcCityEntity> provinces = ccCityDao.selectByExample(example);
        example.clear();
        if (!ObjectUtils.isEmpty(provinces)) {
            ccRegionVO.setProvinceCode(provinces.get(0).getCityCode());
            example.createCriteria().andCityNameEqualTo(ccRegionVO.getCityName()).andTypeEqualTo("市").andParentCodeEqualTo(provinces.get(0).getCityCode());
            List<CcCityEntity> citys = ccCityDao.selectByExample(example);
            example.clear();
            if (!ObjectUtils.isEmpty(citys)) {
                ccRegionVO.setCityCode(citys.get(0).getCityCode());
                example.createCriteria().andCityNameEqualTo(ccRegionVO.getAreaName()).andTypeEqualTo("区").andParentCodeEqualTo(citys.get(0).getCityCode());
                List<CcCityEntity> areas = ccCityDao.selectByExample(example);
                if (!ObjectUtils.isEmpty(areas))
                    ccRegionVO.setAreaCode(areas.get(0).getCityCode());
            }
        }
        return ccRegionVO;
    }



}
