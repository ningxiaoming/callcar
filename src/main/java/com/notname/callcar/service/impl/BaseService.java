package com.notname.callcar.service.impl;


import com.alibaba.fastjson.JSONObject;
import com.notname.callcar.aop.ControllerLog;
import com.notname.callcar.common.enums.DeletedEnum;
import com.notname.callcar.config.VOConverter;
import com.notname.callcar.dao.*;
import com.notname.callcar.entity.*;
import com.notname.callcar.infrastructure.module.gaode.GaodeBaseRes;
import com.notname.callcar.infrastructure.module.gaode.RegeoCode;
import com.notname.callcar.module.res.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;


@Service
public class BaseService {

    @Autowired
    private CcUserDao ccUserDao;

    @Autowired
    private CcCityDao ccCityDao;

    @Autowired
    private CcLikeDao ccLikeDao;

    @Autowired
    private CcHeadLineDao ccHeadLineDao;

    @Autowired
    private CcUserMsgDao ccUserMsgDao;

    @Autowired
    private CcDriverMsgDao ccDriverMsgDao;

    @Value("${gaode.key}")
    private String key;

    @Value("${cos.hosts}")
    private String hosts;


    public void headLineCommentDownDown(String hlCode) {
        if (ObjectUtils.isEmpty(hlCode)) {
            return;
        }
        CcHeadLineEntityExample example = new CcHeadLineEntityExample();
        example.createCriteria().andHlCodeEqualTo(hlCode).andDeletedEqualTo(DeletedEnum.NOT_DELETE.getCode());
        List<CcHeadLineEntity> ccHeadLineEntities = ccHeadLineDao.selectByExample(example);
        if (!ObjectUtils.isEmpty(ccHeadLineEntities)) {
            CcHeadLineEntity ccHeadLineEntity = ccHeadLineEntities.get(0);
            ccHeadLineEntity.setCommentCount(ccHeadLineEntity.getCommentCount() == 0 ? 0 : ccHeadLineEntity.getCommentCount() - 1);
            ccHeadLineDao.updateByPrimaryKeySelective(ccHeadLineEntity);
        }
    }

    public Map<String, CcLikeVO> findLikeEntity(List<String> bizCodes) {
        String currentUserCode = ControllerLog.currentUserCodeThreadLocal.get();
        if (ObjectUtils.isEmpty(bizCodes) || ObjectUtils.isEmpty(currentUserCode)) {
            return new HashMap<>();
        }
        CcLikeEntityExample example = new CcLikeEntityExample();
        example.createCriteria().andCreatorCodeEqualTo(currentUserCode).andBizCodeIn(bizCodes).andDeletedEqualTo(DeletedEnum.NOT_DELETE.getCode());
        List<CcLikeEntity> ccLikeEntities = ccLikeDao.selectByExample(example);
        if (ObjectUtils.isEmpty(ccLikeEntities)) {
            return new HashMap<>();
        }
        return ccLikeEntities.stream().map(VOConverter.INSTANCE::conver).collect(Collectors.toMap(CcLikeEntity::getBizCode, Function.identity(), (key1, key2) -> key2));
    }

    public Map<String, CcHeadLineVO> findHeadLineEntity(List<String> hlCode) {
        if (ObjectUtils.isEmpty(hlCode)) {
            return new HashMap<>();
        }
        CcHeadLineEntityExample example = new CcHeadLineEntityExample();
        example.createCriteria().andHlCodeIn(hlCode).andDeletedEqualTo(DeletedEnum.NOT_DELETE.getCode());
        List<CcHeadLineEntity> ccHeadLineEntities = ccHeadLineDao.selectByExample(example);
        if (ObjectUtils.isEmpty(ccHeadLineEntities)) {
            return new HashMap<>();
        }
        return ccHeadLineEntities.stream().map(VOConverter.INSTANCE::conver).collect(Collectors.toMap(CcHeadLineEntity::getHlCode, Function.identity(), (key1, key2) -> key2));
    }

    public Map<String, CcUserMsgVO> findUserMsgEntity(List<String> umCode) {
        if (ObjectUtils.isEmpty(umCode)) {
            return new HashMap<>();
        }
        CcUserMsgEntityExample example = new CcUserMsgEntityExample();
        example.createCriteria().andUmCodeIn(umCode).andDeletedEqualTo(DeletedEnum.NOT_DELETE.getCode());
        List<CcUserMsgEntity> ccHeadLineEntities = ccUserMsgDao.selectByExample(example);
        if (ObjectUtils.isEmpty(ccHeadLineEntities)) {
            return new HashMap<>();
        }
        return ccHeadLineEntities.stream().map(VOConverter.INSTANCE::conver).collect(Collectors.toMap(CcUserMsgEntity::getUmCode, Function.identity(), (key1, key2) -> key2));
    }

    public Map<String, CcDriverMsgVO> findDriverMsgEntity(List<String> dmCode) {
        if (ObjectUtils.isEmpty(dmCode)) {
            return new HashMap<>();
        }
        CcDriverMsgEntityExample example = new CcDriverMsgEntityExample();
        example.createCriteria().andDmCodeIn(dmCode).andDeletedEqualTo(DeletedEnum.NOT_DELETE.getCode());
        List<CcDriverMsgEntity> ccHeadLineEntities = ccDriverMsgDao.selectByExample(example);
        if (ObjectUtils.isEmpty(ccHeadLineEntities)) {
            return new HashMap<>();
        }
        return ccHeadLineEntities.stream().map(VOConverter.INSTANCE::conver).collect(Collectors.toMap(CcDriverMsgEntity::getDmCode, Function.identity(), (key1, key2) -> key2));
    }

    public Map<String, CcUserVO> findUserEntity(List<String> userCodes) {
        if (ObjectUtils.isEmpty(userCodes)) {
            return new HashMap<>();
        }
        CcUserEntityExample example = new CcUserEntityExample();
        example.createCriteria().andUserCodeIn(userCodes);
        List<CcUserEntity> ccUserEntities = ccUserDao.selectByExample(example);
        if (ObjectUtils.isEmpty(ccUserEntities)) {
            return new HashMap<>();
        }
        return ccUserEntities.stream().map(VOConverter.INSTANCE::conver)
                .peek(item -> {
                    if (!ObjectUtils.isEmpty(item.getAvatarUrl())) {
                        if (!item.getAvatarUrl().contains("https://")) {
                            item.setAvatarUrl(hosts + item.getAvatarUrl());
                        }
                    }
                })
                .collect(Collectors.toMap(CcUserEntity::getUserCode, Function.identity(), (key1, key2) -> key2));
    }

    public CcUserEntity findUserEntityByCode(String userCode) {
        if (ObjectUtils.isEmpty(userCode)) {
            return new CcUserEntity();
        }
        CcUserEntityExample example = new CcUserEntityExample();
        example.createCriteria().andUserCodeEqualTo(userCode);
        List<CcUserEntity> ccUserEntities = ccUserDao.selectByExample(example);
        if (ObjectUtils.isEmpty(ccUserEntities)) {
            return new CcUserEntity();
        }
        return ccUserEntities.get(0);
    }

    public Map<String, CcCityEntity> findCityEntity(List<String> cityCodes) {
        if (ObjectUtils.isEmpty(cityCodes)) {
            return new HashMap<>();
        }
        CcCityEntityExample example = new CcCityEntityExample();
        example.createCriteria().andCityCodeIn(cityCodes);
        List<CcCityEntity> ccCityEntities = ccCityDao.selectByExample(example);
        if (ObjectUtils.isEmpty(ccCityEntities)) {
            return new HashMap<>();
        }
        return ccCityEntities.stream().collect(Collectors.toMap(CcCityEntity::getCityCode, Function.identity(), (key1, key2) -> key2));
    }

    public RegeoCode getAddress(String lon, String lat) throws IOException {
        //lat 小  log  大
        //参数解释: 纬度,经度 采用高德API可参考高德文档https://lbs.amap.com/
        //注意key是在高德开放平台申请的key,具体获得key的步骤请查看网址:https://developer.amap.com/api/webservice/guide/create-project/get-key
        String parameters = "?key=" + key;
//        parameters+="&location="+"116.481488,39.990464";
        parameters += "&location=" + lon + "," + lat;//经纬度坐标
        parameters += "&extensions=true";//返回结果控制，extensions 参数取值为 all 时会返回基本地址信息、附近 POI 内容、道路信息以及道路交叉口信息。
        parameters += "&radius=10";//搜索半径，radius取值范围在0~3000，默认是1000。单位：米
        parameters += "&batch=false";//批量查询控制，batch 参数设置为 false 时进行单点查询，此时即使传入多个经纬度也只返回第一个经纬度的地址解析查询结果。
        parameters += "&roadlevel=0";//道路等级，当 roadlevel = 0 时，显示所有道路
//        String urlString = "https://restapi.amap.com/v3/geocode/regeo?location="+lat+","+log+"&extensions=base&batch=false&roadlevel=0&key="+key;
        String urlString = "https://restapi.amap.com/v3/geocode/regeo" + parameters;
        StringBuilder res = new StringBuilder();
        URL url = new URL(urlString);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setDoOutput(true);
        conn.setRequestMethod("POST");
        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
        String line;
        while ((line = in.readLine()) != null) {
            res.append(line).append("\n");
        }
        in.close();
        GaodeBaseRes gaodeBaseRes = JSONObject.parseObject(res.toString(), GaodeBaseRes.class);
        return gaodeBaseRes.getRegeocode();
    }


}

