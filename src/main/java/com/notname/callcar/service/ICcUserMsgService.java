package com.notname.callcar.service;

import com.notname.callcar.entity.CcDriverMsgEntity;
import com.notname.callcar.entity.CcUserMsgEntity;
import com.notname.callcar.module.req.CcMsgPageQueryReq;
import com.notname.callcar.module.req.CcRemoveReq;
import com.notname.callcar.module.res.CcDriverMsgVO;
import com.notname.callcar.module.res.CcUserMsgVO;
import com.notname.callcar.module.res.PageResult;

public interface ICcUserMsgService {

    int save(CcUserMsgEntity req);


    int modify(CcUserMsgEntity req);

    CcUserMsgVO queryUserMsgByCode(String umCode);

    PageResult<CcUserMsgVO> pageQuery(CcMsgPageQueryReq req);

    PageResult<CcUserMsgVO> pageQueryUserMsg(CcMsgPageQueryReq req);

    int remove(String req);


}
