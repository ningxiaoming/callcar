package com.notname.callcar.service;

import com.notname.callcar.entity.CcDriverMsgEntity;
import com.notname.callcar.entity.CcUserMsgEntity;
import com.notname.callcar.module.req.CcMsgPageQueryReq;
import com.notname.callcar.module.res.CcDriverMsgVO;
import com.notname.callcar.module.res.CcUserMsgVO;
import com.notname.callcar.module.res.PageResult;

import java.util.List;

public interface ICcDriverMsgService {

    int save(CcDriverMsgEntity req);

    int modify(CcDriverMsgEntity req);

    CcDriverMsgVO queryDriverMsgByCode(String dmCode);


    PageResult<CcDriverMsgVO> pageQuery(CcMsgPageQueryReq req);


    PageResult<CcDriverMsgVO> pageQueryMyDriverMsg(CcMsgPageQueryReq req);

    int remove(String req);




}
