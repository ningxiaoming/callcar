package com.notname.callcar.aop;

import com.alibaba.fastjson.JSONObject;
import com.notname.callcar.common.exception.CcBizException;
import com.notname.callcar.common.exception.CcErrorCodeEnum;
import com.notname.callcar.module.req.LonLatVO;
import com.notname.callcar.module.res.CcUserVO;
import com.notname.callcar.service.ICcUserService;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Aspect
@Component
@Slf4j
public class ControllerLog {

    public static ThreadLocal<String> currentUserCodeThreadLocal = new ThreadLocal<>();

    public static ThreadLocal<LonLatVO> lonLatThreadLocal = new ThreadLocal<>();


    @Autowired
    private ICcUserService iCcUserService;

    @Pointcut("execution(public * com.notname.callcar.controller..*.*(..))")
    public void anyController() {
    }

    /**
     * 环绕
     *
     * @param proceedingJoinPoint
     * @throws Throwable
     */
    @Around("anyController()")
    public Object around(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        long start = System.currentTimeMillis();
        MethodSignature signature = (MethodSignature) proceedingJoinPoint.getSignature();
        String name = signature.getMethod().getName();
        String declaringTypeName = signature.getDeclaringTypeName();
        String methodName = declaringTypeName.replace("com.notname.callcar.controller.", "") + "_" + name;
        String reqParams = "";
        try {
            Map<String, Object> requestParams = getRequestParams(proceedingJoinPoint);
            reqParams = JSONObject.toJSONString(requestParams);
        }catch (Exception e){
            log.error("json序列化失败",e);
        }
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (!ObjectUtils.isEmpty(requestAttributes)){
            HttpServletRequest request = requestAttributes.getRequest();
            String currentUserCode = request.getHeader("currentUserCode");
            if (!ObjectUtils.isEmpty(currentUserCode)){
                // 校验用户的真实性
                iCcUserService.queryByUserCode(currentUserCode);
                currentUserCodeThreadLocal.set(currentUserCode);
            }
        }
        Object proceed;
        String result = "";
        try {
            proceed = proceedingJoinPoint.proceed();
            result = JSONObject.toJSONString(proceed);
        } finally {
            currentUserCodeThreadLocal.remove();
            lonLatThreadLocal.remove();
            log.info("controller 层日志打印;methodName: {};req: {},result: {};timeCost: {}", methodName, reqParams, result, System.currentTimeMillis() - start);
        }

        return proceed;
    }

    private Map<String, Object> getRequestParams(ProceedingJoinPoint proceedingJoinPoint) {
        Map<String, Object> requestParams = new HashMap<>();
        //参数名
        String[] paramNames = ((MethodSignature) proceedingJoinPoint.getSignature()).getParameterNames();
        //参数值
        Object[] paramValues = proceedingJoinPoint.getArgs();

        for (int i = 0; i < paramNames.length; i++) {
            Object value = paramValues[i];
            //如果是文件对象
            if (value instanceof MultipartFile) {
                MultipartFile file = (MultipartFile) value;
                value = file.getOriginalFilename();  //获取文件名
            }
            requestParams.put(paramNames[i], value);
        }
        return requestParams;
    }
}