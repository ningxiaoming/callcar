package com.notname.callcar.dao;

import com.notname.callcar.entity.CcCityEntity;
import com.notname.callcar.entity.CcCityEntityExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface CcCityDao {
    long countByExample(CcCityEntityExample example);

    int deleteByExample(CcCityEntityExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CcCityEntity record);

    int insertSelective(CcCityEntity record);

    List<CcCityEntity> selectByExample(CcCityEntityExample example);

    CcCityEntity selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CcCityEntity record, @Param("example") CcCityEntityExample example);

    int updateByExample(@Param("record") CcCityEntity record, @Param("example") CcCityEntityExample example);

    int updateByPrimaryKeySelective(CcCityEntity record);

    int updateByPrimaryKey(CcCityEntity record);
}