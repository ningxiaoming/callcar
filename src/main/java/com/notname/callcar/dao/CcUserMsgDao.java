package com.notname.callcar.dao;

import com.notname.callcar.entity.CcUserMsgEntity;
import com.notname.callcar.entity.CcUserMsgEntityExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface CcUserMsgDao {
    long countByExample(CcUserMsgEntityExample example);

    int deleteByExample(CcUserMsgEntityExample example);

    int deleteByPrimaryKey(Long id);

    int insert(CcUserMsgEntity record);

    int insertSelective(CcUserMsgEntity record);

    List<CcUserMsgEntity> selectByExample(CcUserMsgEntityExample example);

    CcUserMsgEntity selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") CcUserMsgEntity record, @Param("example") CcUserMsgEntityExample example);

    int updateByExample(@Param("record") CcUserMsgEntity record, @Param("example") CcUserMsgEntityExample example);

    int updateByPrimaryKeySelective(CcUserMsgEntity record);

    int updateByPrimaryKey(CcUserMsgEntity record);
}