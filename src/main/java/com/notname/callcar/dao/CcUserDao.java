package com.notname.callcar.dao;

import com.notname.callcar.entity.CcUserEntity;
import com.notname.callcar.entity.CcUserEntityExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface CcUserDao {
    long countByExample(CcUserEntityExample example);

    int deleteByExample(CcUserEntityExample example);

    int deleteByPrimaryKey(Long id);

    int insert(CcUserEntity record);

    int insertSelective(CcUserEntity record);

    List<CcUserEntity> selectByExample(CcUserEntityExample example);

    CcUserEntity selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") CcUserEntity record, @Param("example") CcUserEntityExample example);

    int updateByExample(@Param("record") CcUserEntity record, @Param("example") CcUserEntityExample example);

    int updateByPrimaryKeySelective(CcUserEntity record);

    int updateByPrimaryKey(CcUserEntity record);
}