package com.notname.callcar.dao;

import com.notname.callcar.entity.CcLikeEntity;
import com.notname.callcar.entity.CcLikeEntityExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface CcLikeDao {
    long countByExample(CcLikeEntityExample example);

    int deleteByExample(CcLikeEntityExample example);

    int deleteByPrimaryKey(Long id);

    int insert(CcLikeEntity record);

    int insertSelective(CcLikeEntity record);

    List<CcLikeEntity> selectByExample(CcLikeEntityExample example);

    CcLikeEntity selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") CcLikeEntity record, @Param("example") CcLikeEntityExample example);

    int updateByExample(@Param("record") CcLikeEntity record, @Param("example") CcLikeEntityExample example);

    int updateByPrimaryKeySelective(CcLikeEntity record);

    int updateByPrimaryKey(CcLikeEntity record);
}