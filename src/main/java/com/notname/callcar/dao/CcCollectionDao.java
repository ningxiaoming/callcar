package com.notname.callcar.dao;

import com.notname.callcar.entity.CcCollectionEntity;
import com.notname.callcar.entity.CcCollectionEntityExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface CcCollectionDao {
    long countByExample(CcCollectionEntityExample example);

    int deleteByExample(CcCollectionEntityExample example);

    int deleteByPrimaryKey(Long id);

    int insert(CcCollectionEntity record);

    int insertSelective(CcCollectionEntity record);

    List<CcCollectionEntity> selectByExample(CcCollectionEntityExample example);

    CcCollectionEntity selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") CcCollectionEntity record, @Param("example") CcCollectionEntityExample example);

    int updateByExample(@Param("record") CcCollectionEntity record, @Param("example") CcCollectionEntityExample example);

    int updateByPrimaryKeySelective(CcCollectionEntity record);

    int updateByPrimaryKey(CcCollectionEntity record);
}