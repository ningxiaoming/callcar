package com.notname.callcar.dao;

import com.notname.callcar.entity.CcCommentEntity;
import com.notname.callcar.entity.CcCommentEntityExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface CcCommentDao {
    long countByExample(CcCommentEntityExample example);

    int deleteByExample(CcCommentEntityExample example);

    int deleteByPrimaryKey(Long id);

    int insert(CcCommentEntity record);

    int insertSelective(CcCommentEntity record);

    List<CcCommentEntity> selectByExample(CcCommentEntityExample example);

    CcCommentEntity selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") CcCommentEntity record, @Param("example") CcCommentEntityExample example);

    int updateByExample(@Param("record") CcCommentEntity record, @Param("example") CcCommentEntityExample example);

    int updateByPrimaryKeySelective(CcCommentEntity record);

    int updateByPrimaryKey(CcCommentEntity record);
}