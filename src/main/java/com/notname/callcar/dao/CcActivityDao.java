package com.notname.callcar.dao;

import com.notname.callcar.entity.CcActivityEntity;
import com.notname.callcar.entity.CcActivityEntityExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface CcActivityDao {
    long countByExample(CcActivityEntityExample example);

    int deleteByExample(CcActivityEntityExample example);

    int deleteByPrimaryKey(Long id);

    int insert(CcActivityEntity record);

    int insertSelective(CcActivityEntity record);

    List<CcActivityEntity> selectByExample(CcActivityEntityExample example);

    CcActivityEntity selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") CcActivityEntity record, @Param("example") CcActivityEntityExample example);

    int updateByExample(@Param("record") CcActivityEntity record, @Param("example") CcActivityEntityExample example);

    int updateByPrimaryKeySelective(CcActivityEntity record);

    int updateByPrimaryKey(CcActivityEntity record);
}