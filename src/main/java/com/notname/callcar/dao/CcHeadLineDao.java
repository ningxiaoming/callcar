package com.notname.callcar.dao;

import com.notname.callcar.entity.CcHeadLineEntity;
import com.notname.callcar.entity.CcHeadLineEntityExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface CcHeadLineDao {
    long countByExample(CcHeadLineEntityExample example);

    int deleteByExample(CcHeadLineEntityExample example);

    int deleteByPrimaryKey(Long id);

    int insert(CcHeadLineEntity record);

    int insertSelective(CcHeadLineEntity record);

    List<CcHeadLineEntity> selectByExample(CcHeadLineEntityExample example);

    CcHeadLineEntity selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") CcHeadLineEntity record, @Param("example") CcHeadLineEntityExample example);

    int updateByExample(@Param("record") CcHeadLineEntity record, @Param("example") CcHeadLineEntityExample example);

    int updateByPrimaryKeySelective(CcHeadLineEntity record);

    int updateByPrimaryKey(CcHeadLineEntity record);
}