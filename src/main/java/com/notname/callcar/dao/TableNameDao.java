package com.notname.callcar.dao;

import com.notname.callcar.entity.TableNameEntity;
import com.notname.callcar.entity.TableNameEntityExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface TableNameDao {
    long countByExample(TableNameEntityExample example);

    int deleteByExample(TableNameEntityExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(TableNameEntity record);

    int insertSelective(TableNameEntity record);

    List<TableNameEntity> selectByExample(TableNameEntityExample example);

    TableNameEntity selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") TableNameEntity record, @Param("example") TableNameEntityExample example);

    int updateByExample(@Param("record") TableNameEntity record, @Param("example") TableNameEntityExample example);

    int updateByPrimaryKeySelective(TableNameEntity record);

    int updateByPrimaryKey(TableNameEntity record);
}