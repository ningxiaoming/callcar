package com.notname.callcar.dao;

import com.notname.callcar.entity.CcDriverMsgEntity;
import com.notname.callcar.entity.CcDriverMsgEntityExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface CcDriverMsgDao {
    long countByExample(CcDriverMsgEntityExample example);

    int deleteByExample(CcDriverMsgEntityExample example);

    int deleteByPrimaryKey(Long id);

    int insert(CcDriverMsgEntity record);

    int insertSelective(CcDriverMsgEntity record);

    List<CcDriverMsgEntity> selectByExample(CcDriverMsgEntityExample example);

    CcDriverMsgEntity selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") CcDriverMsgEntity record, @Param("example") CcDriverMsgEntityExample example);

    int updateByExample(@Param("record") CcDriverMsgEntity record, @Param("example") CcDriverMsgEntityExample example);

    int updateByPrimaryKeySelective(CcDriverMsgEntity record);

    int updateByPrimaryKey(CcDriverMsgEntity record);
}