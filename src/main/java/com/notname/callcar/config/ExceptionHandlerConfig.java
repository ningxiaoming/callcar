package com.notname.callcar.config;

import com.notname.callcar.common.CommonRes;
import com.notname.callcar.common.CommonResUtil;
import com.notname.callcar.common.exception.CcBizException;
import com.notname.callcar.common.exception.CcErrorCodeEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * RestControllerAdvice，统一异常处理
 */
@Slf4j
@RestControllerAdvice
public class ExceptionHandlerConfig {

    /**
     * 业务异常处理
     *
     * @param e 业务异常
     * @return
     */
    @ExceptionHandler(value = CcBizException.class)
    @ResponseBody
    public CommonRes exceptionHandler(CcBizException e) {
        log.error("",e);
        return CommonResUtil.buildCommonRes(e.getErrorCode(), e.getErrorMsg());
    }

    /**
     * 未知异常处理
     */
    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public CommonRes exceptionHandler(Exception e) {
        log.error("《系统异常》", e);
        return CommonResUtil.buildCommonRes(CcErrorCodeEnum.SYSTEM_ERROR);

    }

    /**
     * 参数异常
     */
    @ExceptionHandler(value = IllegalArgumentException.class)
    @ResponseBody
    public CommonRes exceptionHandler(IllegalArgumentException e) {
        log.error("《参数异常》", e);
        return CommonResUtil.buildCommonRes(CcErrorCodeEnum.INVALID_PARAMS.getErrCode(),e.getMessage());
    }

}
