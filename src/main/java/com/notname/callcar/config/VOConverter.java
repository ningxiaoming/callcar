package com.notname.callcar.config;

import com.notname.callcar.entity.*;
import com.notname.callcar.module.res.*;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * https://blog.csdn.net/qq_44732146/article/details/119968376
 */
@Mapper
public interface VOConverter {

    VOConverter INSTANCE = Mappers.getMapper(VOConverter.class);


    CcDriverMsgVO conver(CcDriverMsgEntity entity);

    CcUserMsgVO conver(CcUserMsgEntity entity);


    CcHeadLineVO conver(CcHeadLineEntity ccHeadLineEntity);

    CcUserVO conver(CcUserEntity ccUserEntity);

    CcLikeVO conver(CcLikeEntity ccLikeEntity);

    CcCommentVO conver(CcCommentEntity ccCommentEntity);

    CcActivityVO conver(CcActivityEntity ccActivityEntity);

    CcCollectionVO conver(CcCollectionEntity ccCollectionEntity);
}
