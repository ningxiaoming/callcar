package com.notname.callcar.controller.admin;

import com.notname.callcar.common.CommonReq;
import com.notname.callcar.common.CommonRes;
import com.notname.callcar.common.CommonResUtil;
import com.notname.callcar.entity.CcActivityEntity;
import com.notname.callcar.entity.CcDriverMsgEntity;
import com.notname.callcar.service.ICcActivityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/admin/activity")
@Api(tags = {"管理员页面，管理活动"})
@Slf4j
public class AdminActivityController {


    @Autowired
    private ICcActivityService iCcActivityService;

    @PostMapping("/save")
    @ApiOperation("插入活动")
    @ResponseBody
    public CommonRes<Integer> save(@RequestBody CommonReq<CcActivityEntity> req) {
        CcActivityEntity reqData = req.getData();
        Integer i = iCcActivityService.save(reqData);
        return CommonResUtil.buildCommonRes(i);
    }

    @PostMapping("/modify")
    @ApiOperation("修改活动")
    @ResponseBody
    public CommonRes<Integer> modify(@RequestBody CommonReq<CcActivityEntity> req) {
        CcActivityEntity reqData = req.getData();
        Integer i = iCcActivityService.modify(reqData);
        return CommonResUtil.buildCommonRes(i);
    }

    @PostMapping("/upOrDown")
    @ApiOperation("活动上架下架")
    @ResponseBody
    public CommonRes<Integer> upOrDown(@RequestBody CommonReq<Long> req) {
        Long reqData = req.getData();
        Integer i = iCcActivityService.upOrDown(reqData);
        return CommonResUtil.buildCommonRes(i);
    }

    @PostMapping("/remove")
    @ApiOperation("活动删除")
    @ResponseBody
    public CommonRes<Integer> remove(@RequestBody CommonReq<Long> req) {
        Long reqData = req.getData();
        Integer i = iCcActivityService.remove(reqData);
        return CommonResUtil.buildCommonRes(i);
    }


}
