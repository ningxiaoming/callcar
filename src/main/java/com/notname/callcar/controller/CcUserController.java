package com.notname.callcar.controller;

import com.alibaba.fastjson.JSONObject;
import com.notname.callcar.common.CommonReq;
import com.notname.callcar.common.CommonRes;
import com.notname.callcar.common.CommonResUtil;
import com.notname.callcar.dao.CcCityDao;
import com.notname.callcar.entity.CcCityEntity;
import com.notname.callcar.entity.CcCityEntityExample;
import com.notname.callcar.entity.CcUserEntity;
import com.notname.callcar.infrastructure.module.gaode.GaodeBaseRes;
import com.notname.callcar.module.req.CitiVO;
import com.notname.callcar.module.req.PhoneDecryptReq;
import com.notname.callcar.module.req.WxLoginReq;
import com.notname.callcar.module.res.CcUserVO;
import com.notname.callcar.service.ICcUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.bind.annotation.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

@RestController
@RequestMapping("/callcar/user")
@Api(tags = {"用户信息"})
@Slf4j
public class CcUserController {


    @Autowired
    private ICcUserService iCcUserService;

    @Autowired
    private CcCityDao ccCityDao;

    public static void main(String[] args) {


        String s = "123456";
        System.out.println(s.substring(3));

//        System.out.println(getAddressStr("116.481488", "39.990464"));
    }

    /**
     * 获取文件内文本
     *
     * @param filepath
     * @return
     */
    public String getFileText(String filepath) {
        StringBuffer sb = new StringBuffer();
        try {
            ClassPathResource classPathResource = new ClassPathResource(filepath);
            Reader reader = new InputStreamReader(classPathResource.getInputStream());
            int ch = 0;
            while ((ch = reader.read()) != -1) {
                sb.append((char) ch);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sb.toString().replaceAll("\r\n", "");
    }


    /**
     * 根据经纬度获取省市区
     */
    public static String getAddressStr(String log, String lat) {
        //lat 小  log  大
        //参数解释: 纬度,经度 采用高德API可参考高德文档https://lbs.amap.com/
        //注意key是在高德开放平台申请的key,具体获得key的步骤请查看网址:https://developer.amap.com/api/webservice/guide/create-project/get-key
        String key = "09c97d8f6db1652c51c62644eeb1b961";
        String parameters = "?key=" + key;
//        parameters+="&location="+"116.481488,39.990464";
        parameters += "&location=" + log + "," + lat;//经纬度坐标
        parameters += "&extensions=true";//返回结果控制，extensions 参数取值为 all 时会返回基本地址信息、附近 POI 内容、道路信息以及道路交叉口信息。
        parameters += "&radius=10";//搜索半径，radius取值范围在0~3000，默认是1000。单位：米
        parameters += "&batch=false";//批量查询控制，batch 参数设置为 false 时进行单点查询，此时即使传入多个经纬度也只返回第一个经纬度的地址解析查询结果。
        parameters += "&roadlevel=0";//道路等级，当 roadlevel = 0 时，显示所有道路
//        String urlString = "https://restapi.amap.com/v3/geocode/regeo?location="+lat+","+log+"&extensions=base&batch=false&roadlevel=0&key="+key;
        String urlString = "https://restapi.amap.com/v3/geocode/regeo" + parameters;
        StringBuilder res = new StringBuilder();
        try {
            URL url = new URL(urlString);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
            String line;
            while ((line = in.readLine()) != null) {
                res.append(line).append("\n");
            }
            in.close();
            //解析结果
            GaodeBaseRes gaodeBaseRes = JSONObject.parseObject(res.toString(), GaodeBaseRes.class);
            com.alibaba.fastjson.JSONObject jsonObject = com.alibaba.fastjson.JSONObject.parseObject(res.toString());
            System.out.println("jsonObject = " + jsonObject);
            com.alibaba.fastjson.JSONObject jsonObject1 = jsonObject.getJSONObject("regeocode");
            res = new StringBuilder(jsonObject1.getString("formatted_address"));
        } catch (Exception e) {
            System.out.println("获取地址信息异常");
            e.printStackTrace();
            return null;
        }
        System.out.println("通过API获取到具体位置:" + res);
        return res.toString();
    }


    @RequestMapping(method = RequestMethod.POST, value = "/queryByCode")
    @ApiOperation("通过openId获取用户信息")
    public CommonRes<CcUserVO> queryByOpenId() {
        CcUserVO ccUserVO = iCcUserService.queryByCode();
        return CommonResUtil.buildCommonRes(ccUserVO);

    }


    @PostMapping("/login")
    @ApiOperation("登录(也就是授权)")
    public CommonRes<CcUserEntity> login(@RequestBody CommonReq<WxLoginReq> req) {
        WxLoginReq reqData = req.getData();
        CcUserEntity result = iCcUserService.login(reqData);
        return CommonResUtil.buildCommonRes(result);
    }

    @PostMapping("/completion")
    @ApiOperation("个人信息补全，昵称，头像等信息")
    public CommonRes<Integer> completion(@RequestBody CommonReq<CcUserEntity> req) {
        CcUserEntity reqData = req.getData();
        Integer i = iCcUserService.completion(reqData);
        return CommonResUtil.buildCommonRes(i);
    }

    @PostMapping("/decryptPhone")
    @ApiOperation("解密手机号")
    public CommonRes<String> decryptPhone(@RequestBody CommonReq<PhoneDecryptReq> req) {
        PhoneDecryptReq data = req.getData();
        String phone = iCcUserService.decryptPhone(data.getPhoneStr());
        return CommonResUtil.buildCommonRes(phone);
    }

    @PostMapping("test1")
    public String test1() {
        CcCityEntityExample example = new CcCityEntityExample();
        ccCityDao.deleteByExample(example);
        return "";
    }


    @PostMapping("test")
    public String test() {


        String fileText = getFileText("data(1).json");
        CitiVO citiVO = JSONObject.parseObject(fileText, CitiVO.class);

        Map<String, String> province_list = citiVO.getProvince_list();
        for (Map.Entry<String, String> stringStringEntry : province_list.entrySet()) {
            CcCityEntity record = new CcCityEntity();
            record.setCityCode(stringStringEntry.getKey());
            record.setCityName(stringStringEntry.getValue());
            record.setType("省");
            record.setParentCode("0");
            ccCityDao.insert(record);
        }

        Map<String, String> city_list = citiVO.getCity_list();
        for (Map.Entry<String, String> stringStringEntry : province_list.entrySet()) {
            for (Map.Entry<String, String> stringEntry : city_list.entrySet()) {
                String substring = stringStringEntry.getKey().substring(0, 2);
                String substring1 = stringEntry.getKey().substring(0, 2);
                if (substring.equals(substring1)) {
                    CcCityEntity record = new CcCityEntity();
                    record.setCityCode(stringEntry.getKey());
                    record.setCityName(stringEntry.getValue());
                    record.setType("市");
                    record.setParentCode(stringStringEntry.getKey());
                    ccCityDao.insert(record);
                }
            }
        }

        Map<String, String> unty_list = citiVO.getUnty_list();
        for (Map.Entry<String, String> stringEntry : city_list.entrySet()) {
            for (Map.Entry<String, String> stringStringEntry : unty_list.entrySet()) {
                String substring = stringEntry.getKey().substring(0, 4);
                String substring1 = stringStringEntry.getKey().substring(0, 4);
                if (substring.equals(substring1)) {
                    CcCityEntity record = new CcCityEntity();
                    record.setCityCode(stringStringEntry.getKey());
                    record.setCityName(stringStringEntry.getValue());
                    record.setType("区");
                    record.setParentCode(stringEntry.getKey());
                    ccCityDao.insert(record);
                }
            }

        }
        return "111";
    }

}