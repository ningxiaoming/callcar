package com.notname.callcar.controller;

import com.notname.callcar.common.CommonReq;
import com.notname.callcar.common.CommonRes;
import com.notname.callcar.common.CommonResUtil;
import com.notname.callcar.common.exception.CcBizException;
import com.notname.callcar.common.exception.CcErrorCodeEnum;
import com.notname.callcar.entity.CcDriverMsgEntity;
import com.notname.callcar.entity.CcUserMsgEntity;
import com.notname.callcar.module.req.CcMsgPageQueryReq;
import com.notname.callcar.module.res.CcDriverMsgVO;
import com.notname.callcar.module.res.CcUserMsgVO;
import com.notname.callcar.module.res.PageResult;
import com.notname.callcar.service.ICcDriverMsgService;
import com.notname.callcar.service.ICcUserMsgService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/callcar/userMsg")
@Api(tags = {"车找人页面"})
@Slf4j
public class CcUserMsgController extends BaseController {

    @Autowired
    private ICcUserMsgService iCcUserMsgService;

    @Autowired
    private ICcDriverMsgService iCcDriverMsgService;

    @PostMapping("/pageQuery")
    @ApiOperation("分页查询")
    public CommonRes<PageResult<CcDriverMsgVO>> pageQuery(@RequestBody CommonReq<CcMsgPageQueryReq> req) {
        CcMsgPageQueryReq reqData = req.getData();
        PageResult<CcDriverMsgVO> pageResult = iCcDriverMsgService.pageQuery(reqData);
        return CommonResUtil.buildCommonRes(pageResult);
    }


    @PostMapping("/save")
    @ApiOperation("插入车找人信息")
    public CommonRes<Integer> save(@RequestBody CommonReq<CcUserMsgEntity> req) {
        CcUserMsgEntity reqData = req.getData();
        int i;
        if (ObjectUtils.isEmpty(reqData.getId())) {
            i = iCcUserMsgService.save(reqData);
        } else {
            i = iCcUserMsgService.modify(reqData);
        }
        return CommonResUtil.buildCommonRes(i);

    }

}
