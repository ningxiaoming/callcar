package com.notname.callcar.controller;

import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import com.notname.callcar.common.CommonReq;
import com.notname.callcar.common.CommonRes;
import com.notname.callcar.common.CommonResUtil;
import com.notname.callcar.common.exception.CcErrorCodeEnum;
import com.notname.callcar.utils.SnowFlakeUtil;
import com.notname.callcar.utils.TencentCosUtils;
import com.notname.callcar.utils.WxCosUtils;
import com.qcloud.cos.model.COSObject;
import com.qcloud.cos.model.PutObjectResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@RestController
@RequestMapping("/callcar/file")
@Api(tags = {"文件操作页面"})
@Slf4j
public class FileController {

    @Autowired
    private SnowFlakeUtil snowFlakeUtil;

    @Autowired
    private TencentCosUtils tencentCosUtils;

    @Autowired
    private WxCosUtils wxCosUtils;

    @Value("${cos.hosts}")
    private String hosts;

    @ApiOperation(value = "测试 cos")
    @PostMapping("/testcos")
    public CommonRes<String> testCos(String fileId) {
        Assert.notNull(fileId, "file is null");
        COSObject object = wxCosUtils.getObject(fileId);
        return CommonResUtil.buildCommonRes(JSON.toJSONString(object));
    }

    @ApiOperation(value = "上传文件")
    @PostMapping("/upload")
    public CommonRes<String> uploadFile(MultipartFile file) {
        Assert.notNull(file, "file is null");
        try {
            byte[] bytes = file.getBytes();
            String fileSuffix = Objects.requireNonNull(file.getOriginalFilename()).substring(file.getOriginalFilename().lastIndexOf("."));
            String filename = "headLine/"+snowFlakeUtil.snowflakeStringId() + fileSuffix;
            tencentCosUtils.upload2cloud(bytes, filename);
            return CommonResUtil.buildCommonRes(hosts+filename);
        }catch (Exception e){
            log.error("上传文件保存",e);
            return CommonResUtil.buildCommonRes(CcErrorCodeEnum.SYSTEM_ERROR);
        }
    }

    @ApiOperation(value = "删除文件")
    @PostMapping("/remove")
    public CommonRes<String> removeFile(@RequestBody CommonReq<String> req) {
        String fileName = req.getData();
        tencentCosUtils.deleteFileFromcloud(fileName);
        return CommonResUtil.buildCommonRes("true");
    }
}
