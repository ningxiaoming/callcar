package com.notname.callcar.controller;

import com.notname.callcar.common.CommonReq;
import com.notname.callcar.common.CommonRes;
import com.notname.callcar.common.CommonResUtil;
import com.notname.callcar.entity.CcCollectionEntity;
import com.notname.callcar.module.req.CcCollectionPageQueryReq;
import com.notname.callcar.module.req.CcCollectionSaveReq;
import com.notname.callcar.module.res.CcCollectionVO;
import com.notname.callcar.module.res.PageResult;
import com.notname.callcar.service.ICcCollectionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/callcar/collection")
@Api(tags = {"我的收藏"})
@Slf4j
public class CcCollectionController {

    @Autowired
    private ICcCollectionService iCcCollectionService;

    @PostMapping("/pageQuery")
    @ApiOperation("分页查询")
    public CommonRes<PageResult<CcCollectionVO>> pageQuery(@RequestBody CommonReq<CcCollectionPageQueryReq> req) {
        CcCollectionPageQueryReq reqData = req.getData();
        PageResult<CcCollectionVO> pageResult = iCcCollectionService.pageQuery(reqData);
        return CommonResUtil.buildCommonRes(pageResult);
    }

    @PostMapping("/save")
    @ApiOperation("插入收藏")
    @ResponseBody
    public CommonRes<Integer> save(@RequestBody CommonReq<CcCollectionSaveReq> req) {
        CcCollectionSaveReq reqData = req.getData();
        Integer i = iCcCollectionService.save(reqData);
        return CommonResUtil.buildCommonRes(i);
    }

    @PostMapping("/remove")
    @ApiOperation("删除收藏")
    @ResponseBody
    public CommonRes<Integer> remove(@RequestBody CommonReq<CcCollectionEntity> req) {
        CcCollectionEntity reqData = req.getData();
        String collectionCode = reqData.getCollectionCode();
        Integer i = iCcCollectionService.remove(collectionCode);
        return CommonResUtil.buildCommonRes(i);
    }
}
