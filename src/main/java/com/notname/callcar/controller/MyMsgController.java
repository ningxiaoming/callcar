package com.notname.callcar.controller;

import com.notname.callcar.common.CommonReq;
import com.notname.callcar.common.CommonRes;
import com.notname.callcar.common.CommonResUtil;
import com.notname.callcar.common.enums.CcCollectionBizTypeEnum;
import com.notname.callcar.entity.CcCommentEntity;
import com.notname.callcar.entity.CcDriverMsgEntity;
import com.notname.callcar.entity.CcHeadLineEntity;
import com.notname.callcar.entity.CcUserMsgEntity;
import com.notname.callcar.module.req.CcHeadLineQueryReq;
import com.notname.callcar.module.req.CcHeadLineSaveReq;
import com.notname.callcar.module.req.CcMsgPageQueryReq;
import com.notname.callcar.module.req.CcRemoveReq;
import com.notname.callcar.module.res.CcDriverMsgVO;
import com.notname.callcar.module.res.CcHeadLineVO;
import com.notname.callcar.module.res.CcUserMsgVO;
import com.notname.callcar.module.res.PageResult;
import com.notname.callcar.service.ICcDriverMsgService;
import com.notname.callcar.service.ICcHeadLineService;
import com.notname.callcar.service.ICcUserMsgService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@Api(tags = {"我发布的信息"})
@RequestMapping("/callcar/my/")
public class MyMsgController extends BaseController {

    @Autowired
    private ICcDriverMsgService iCcDriverMsgService;

    @Autowired
    private ICcUserMsgService iCcUserMsgService;

    @Autowired
    private ICcHeadLineService iCcHeadLineService;


    @PostMapping("pageQueryUserMsg")
    @ApiOperation("分页查询我发布的人找车信息")
    public CommonRes<PageResult<CcUserMsgVO>> pageQueryUserMsg(@RequestBody CommonReq<CcMsgPageQueryReq> req) {
        CcMsgPageQueryReq reqData = req.getData();
        PageResult<CcUserMsgVO> pageResult = iCcUserMsgService.pageQueryUserMsg(reqData);
        return CommonResUtil.buildCommonRes(pageResult);
    }

    @PostMapping("pageQueryDriverMsg")
    @ApiOperation("分页查询我的车找人信息")
    public CommonRes<PageResult<CcDriverMsgVO>> pageQueryMyDriverMsg(@RequestBody CommonReq<CcMsgPageQueryReq> req) {
        CcMsgPageQueryReq reqData = req.getData();
        PageResult<CcDriverMsgVO> pageResult = iCcDriverMsgService.pageQueryMyDriverMsg(reqData);
        return CommonResUtil.buildCommonRes(pageResult);

    }

    @PostMapping("pageQueryHeadline")
    @ApiOperation("分页查询我发布的头条信息")
    public CommonRes<PageResult<CcHeadLineVO>> pageQueryMyHeadline(@RequestBody CommonReq<CcHeadLineQueryReq> req) {
        CcHeadLineQueryReq reqData = req.getData();
        PageResult<CcHeadLineVO> pageResult = iCcHeadLineService.pageQueryMyHeadline(reqData);
        return CommonResUtil.buildCommonRes(pageResult);

    }

    @PostMapping("modifyHeadline")
    @ApiOperation("修改头条信息")
    public CommonRes<Integer> modifyHeadline(@RequestBody CommonReq<CcHeadLineSaveReq> req) {
        CcHeadLineSaveReq reqData = req.getData();
        Integer i = iCcHeadLineService.modify(reqData);
        return CommonResUtil.buildCommonRes(i);
    }

    @PostMapping("/modifyDriverMsg")
    @ApiOperation("修改人找车信息")
    public CommonRes<?> modifyDriverMsg(@RequestBody CommonReq<CcDriverMsgEntity> req) {
        CcDriverMsgEntity reqData = req.getData();
        Integer i = iCcDriverMsgService.modify(reqData);
        return CommonResUtil.buildCommonRes(i);
    }

    @PostMapping("/modifyUserMsg")
    @ApiOperation("修改车找人信息")
    public CommonRes<Integer> modifyUserMsg(@RequestBody CommonReq<CcUserMsgEntity> req) {
        CcUserMsgEntity reqData = req.getData();
        Integer i = iCcUserMsgService.modify(reqData);
        return CommonResUtil.buildCommonRes(i);

    }

    @PostMapping("/queryDriverMsgByCode")
    @ApiOperation("根据code查询人找车信息")
    public CommonRes<CcDriverMsgVO> queryDriverMsgByCode(@RequestBody CommonReq<CcRemoveReq> req) {
        CcRemoveReq reqData = req.getData();
        CcDriverMsgVO ccDriverMsgVO = iCcDriverMsgService.queryDriverMsgByCode(reqData.getBizCode());
        return CommonResUtil.buildCommonRes(ccDriverMsgVO);
    }

    @PostMapping("/queryUserMsgByCode")
    @ApiOperation("根据code查询车找人信息")
    public CommonRes<CcUserMsgVO> queryUserMsgByCode(@RequestBody CommonReq<CcRemoveReq> req) {
        CcRemoveReq reqData = req.getData();
        CcUserMsgVO ccUserMsgVO = iCcUserMsgService.queryUserMsgByCode(reqData.getBizCode());
        return CommonResUtil.buildCommonRes(ccUserMsgVO);
    }

    @PostMapping("/remove")
    @ApiOperation("删除我发布的信息")
    @ResponseBody
    public CommonRes<String> remove(@RequestBody CommonReq<CcRemoveReq> req) {
        CcRemoveReq reqData = req.getData();
        Assert.hasLength(reqData.getBizType(), "bizType is null");
        CcCollectionBizTypeEnum bizType = CcCollectionBizTypeEnum.valueOf(reqData.getBizType());
        String bizCode = reqData.getBizCode();
        Assert.notNull(bizType, "BizType is null");
        Assert.hasLength(bizCode, "bizCode is null");
        if (CcCollectionBizTypeEnum.USER_MSG.getCode().equals(bizType.getCode())){
            iCcUserMsgService.remove(bizCode);
        }else if (CcCollectionBizTypeEnum.DRIVER_MSG.getCode().equals(bizType.getCode())){
            iCcDriverMsgService.remove(bizCode);
        } else if (CcCollectionBizTypeEnum.HEAD_LINE.getCode().equals(bizType.getCode())) {
            iCcHeadLineService.remove(bizCode);
        }
        return CommonResUtil.buildCommonRes(bizCode);
    }








}
