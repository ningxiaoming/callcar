package com.notname.callcar.controller;

import com.notname.callcar.aop.ControllerLog;
import com.notname.callcar.common.CommonReq;
import com.notname.callcar.common.CommonRes;
import com.notname.callcar.common.CommonResUtil;
import com.notname.callcar.common.exception.CcBizException;
import com.notname.callcar.common.exception.CcErrorCodeEnum;
import com.notname.callcar.entity.CcCommentEntity;
import com.notname.callcar.entity.CcHeadLineEntity;
import com.notname.callcar.entity.CcLikeEntity;
import com.notname.callcar.entity.CcUserMsgEntity;
import com.notname.callcar.module.req.CcCommentSaveReq;
import com.notname.callcar.module.req.CcHeadLineQueryReq;
import com.notname.callcar.module.req.CcHeadLineSaveReq;
import com.notname.callcar.module.req.CcMsgPageQueryReq;
import com.notname.callcar.module.res.CcHeadLineVO;
import com.notname.callcar.module.res.CcUserMsgVO;
import com.notname.callcar.module.res.PageResult;
import com.notname.callcar.service.ICcHeadLineService;
import com.notname.callcar.service.ICcUserMsgService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/callcar/headLine")
@Api(tags = {"头条页面(疫情咨询)"})
@Slf4j
public class CcHeadLineController extends BaseController {

    @Autowired
    private ICcHeadLineService iCcHeadLineService;

    @PostMapping("/pageQuery")
    @ApiOperation("分页查询")
    public CommonRes<PageResult<CcHeadLineVO>> pageQuery(@RequestBody CommonReq<CcHeadLineQueryReq> req) {
        CcHeadLineQueryReq reqData = req.getData();
        PageResult<CcHeadLineVO> pageResult = iCcHeadLineService.pageQuery(reqData);
        return CommonResUtil.buildCommonRes(pageResult);

    }

    @PostMapping("/save")
    @ApiOperation("插入头条信息")
    public CommonRes<Integer> save(@RequestBody CommonReq<CcHeadLineSaveReq> req) {
        CcHeadLineSaveReq reqData = req.getData();
        ControllerLog.lonLatThreadLocal.set(req.getLonLatVO());
        Integer i = iCcHeadLineService.save(reqData);
        return CommonResUtil.buildCommonRes(i);

    }


    @PostMapping("/like")
    @ApiOperation("头条信息点赞")
    public CommonRes<Integer> like(@RequestBody CommonReq<CcLikeEntity> req) {
        CcLikeEntity reqData = req.getData();
        Integer i = iCcHeadLineService.like(reqData);
        return CommonResUtil.buildCommonRes(i);
    }

    @PostMapping("/comment")
    @ApiOperation("头条信息评论")
    public CommonRes<CcCommentEntity> comment(@RequestBody CommonReq<CcCommentSaveReq> req) {
        CcCommentSaveReq reqData = req.getData();
        String code = iCcHeadLineService.comment(reqData);
        CcCommentEntity ccCommentEntity = new CcCommentEntity();
        ccCommentEntity.setCommentCode(code);
        return CommonResUtil.buildCommonRes(ccCommentEntity);
    }

}
