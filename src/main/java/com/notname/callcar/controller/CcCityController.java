package com.notname.callcar.controller;

import com.notname.callcar.common.CommonReq;
import com.notname.callcar.common.CommonRes;
import com.notname.callcar.common.CommonResUtil;
import com.notname.callcar.module.req.LonLatVO;
import com.notname.callcar.module.res.CcRegionVO;
import com.notname.callcar.service.ICcCityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/callcar/city")
@Api(tags = {"地区转化"})
@Slf4j
public class CcCityController extends BaseController {

    @Autowired
    private ICcCityService iCcCityService;


    @PostMapping("/queryReginByLonLat")
    @ApiOperation("根据经纬度获取详细地址")
    public CommonRes<CcRegionVO> queryReginByLonLat(@RequestBody CommonReq<LonLatVO> req) {
        LonLatVO reqData = req.getData();
        CcRegionVO ccRegionVO = iCcCityService.queryReginByLonLat(reqData);
        return CommonResUtil.buildCommonRes(ccRegionVO);
    }


}
