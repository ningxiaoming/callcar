package com.notname.callcar.controller;

import com.notname.callcar.common.CommonReq;
import com.notname.callcar.common.CommonRes;
import com.notname.callcar.common.CommonResUtil;
import com.notname.callcar.entity.CcCommentEntity;
import com.notname.callcar.entity.CcLikeEntity;
import com.notname.callcar.module.req.CcCommentPageQueryReq;
import com.notname.callcar.module.res.CcCommentVO;
import com.notname.callcar.module.res.PageResult;
import com.notname.callcar.service.ICcCommentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/callcar/comment")
@Api(tags = {"评论页面"})
@Slf4j
public class CcCommentController extends BaseController {

    @Autowired
    private ICcCommentService iCcCommentService;

    @PostMapping("/pageQuery")
    @ApiOperation("分页查询")
    public CommonRes<PageResult<CcCommentVO>> pageQuery(@RequestBody CommonReq<CcCommentPageQueryReq> req) {
        CcCommentPageQueryReq reqData = req.getData();
        PageResult<CcCommentVO> pageResult = iCcCommentService.pageQuery(reqData);
        return CommonResUtil.buildCommonRes(pageResult);
    }

    @PostMapping("/like")
    @ApiOperation("点赞评论")
    @ResponseBody
    public CommonRes<Integer> save(@RequestBody CommonReq<CcLikeEntity> req) {
        CcLikeEntity reqData = req.getData();
        Integer i = iCcCommentService.like(reqData);
        return CommonResUtil.buildCommonRes(i);
    }

    @PostMapping("/remove")
    @ApiOperation("删除评论")
    @ResponseBody
    public CommonRes<Integer> remove(@RequestBody CommonReq<CcCommentEntity> req) {
        CcCommentEntity reqData = req.getData();
        String commentCode = reqData.getCommentCode();
        Integer i = iCcCommentService.remove(commentCode);
        return CommonResUtil.buildCommonRes(i);
    }


}
