package com.notname.callcar.controller;

import com.notname.callcar.common.CommonReq;
import com.notname.callcar.common.CommonRes;
import com.notname.callcar.common.CommonResUtil;
import com.notname.callcar.common.exception.CcBizException;
import com.notname.callcar.common.exception.CcErrorCodeEnum;
import com.notname.callcar.entity.CcDriverMsgEntity;
import com.notname.callcar.module.req.CcMsgPageQueryReq;
import com.notname.callcar.module.res.CcDriverMsgVO;
import com.notname.callcar.module.res.CcUserMsgVO;
import com.notname.callcar.module.res.PageResult;
import com.notname.callcar.service.ICcDriverMsgService;
import com.notname.callcar.service.ICcUserMsgService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/callcar/driverMsg")
@Api(tags = {"人找车页面"})
@Slf4j
public class CcDriverMsgController extends BaseController {

    @Autowired
    private ICcDriverMsgService iCcDriverMsgService;

    @Autowired
    private ICcUserMsgService iCcUserMsgService;


    @PostMapping("/pageQuery")
    @ApiOperation("分页查询")
    public CommonRes<PageResult<CcUserMsgVO>> pageQuery(@RequestBody CommonReq<CcMsgPageQueryReq> req) {
        CcMsgPageQueryReq reqData = req.getData();
        PageResult<CcUserMsgVO> pageResult = iCcUserMsgService.pageQuery(reqData);
        return CommonResUtil.buildCommonRes(pageResult);

    }

    @PostMapping("/save")
    @ApiOperation("插入人找车信息")
    @ResponseBody
    public CommonRes<Integer> save(@RequestBody CommonReq<CcDriverMsgEntity> req) {
        CcDriverMsgEntity reqData = req.getData();
        int i;
        if (ObjectUtils.isEmpty(reqData.getId())) {
            i = iCcDriverMsgService.save(reqData);
        } else {
            i = iCcDriverMsgService.modify(reqData);
        }
        return CommonResUtil.buildCommonRes(i);
    }


}
