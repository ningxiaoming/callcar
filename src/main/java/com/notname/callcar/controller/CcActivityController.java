package com.notname.callcar.controller;

import com.notname.callcar.common.CommonReq;
import com.notname.callcar.common.CommonRes;
import com.notname.callcar.common.CommonResUtil;
import com.notname.callcar.module.req.CcActivityPageQueryReq;
import com.notname.callcar.module.res.CcActivityVO;
import com.notname.callcar.module.res.GlobalConfigVO;
import com.notname.callcar.module.res.PageResult;
import com.notname.callcar.service.ICcActivityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/callcar/activity")
@Api(tags = {"活动信息页面"})
@Slf4j
public class CcActivityController extends BaseController {

    @Autowired
    private ICcActivityService iCcActivityService;

    @Value("${cos.hosts}")
    private String hosts;

    @PostMapping("/pageQuery")
    @ApiOperation("分页查询")
    public CommonRes<PageResult<CcActivityVO>> pageQuery(@RequestBody CommonReq<CcActivityPageQueryReq> req) {
        CcActivityPageQueryReq reqData = req.getData();
        PageResult<CcActivityVO> pageResult = iCcActivityService.pageQuery(reqData);
        return CommonResUtil.buildCommonRes(pageResult);
    }

    @PostMapping("/query")
    @ApiOperation("查询配置")
    public CommonRes<GlobalConfigVO> pageQuery() {
        GlobalConfigVO globalConfigVO = iCcActivityService.queryConfig();
        globalConfigVO.setLogoUrl(hosts + "config/logo.png");
        return CommonResUtil.buildCommonRes(globalConfigVO);
    }

}
