package com.notname.callcar.module.req;


import lombok.Data;

@Data
public class WxLoginReq {

    private String loginCode;

    private String phoneCode;

}
