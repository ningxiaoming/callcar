package com.notname.callcar.module.req;

import com.notname.callcar.common.CommonPageQueryReq;
import lombok.Data;

@Data
public class CcCollectionPageQueryReq extends CommonPageQueryReq {


    /**
     * 帖子code
     */
    private String bizType;


}
