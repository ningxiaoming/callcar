package com.notname.callcar.module.req;

import lombok.Data;

@Data
public class PhoneDecryptReq {

    private String phoneStr;
}
