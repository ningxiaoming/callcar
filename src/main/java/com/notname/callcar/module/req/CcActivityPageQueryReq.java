package com.notname.callcar.module.req;

import com.notname.callcar.common.CommonPageQueryReq;
import lombok.Data;

@Data
public class CcActivityPageQueryReq extends CommonPageQueryReq {


    /**
     * 业务类型
     */
    private String bizType;


}
