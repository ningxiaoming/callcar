package com.notname.callcar.module.req;

import com.notname.callcar.common.CommonPageQueryReq;
import lombok.Data;

@Data
public class CcHeadLineQueryReq extends CommonPageQueryReq {


    /**
     * 起始省code
     */
    private String startProvinceCode;

    /**
     * 起始市code
     */
    private String startCityCode;

    /**
     * 起始区code
     */
    private String startAreaCode;

    /**
     * 目的省code
     */
    private String endProvinceCode;

    /**
     * 目的市code
     */
    private String endCityCode;

    /**
     * 目睹区code
     */
    private String endAreaCode;

    /**
     * 出发日期
     */
    private Long headLineTime;

    /**
     * 当前userCode
     */
    private String currentUserCode;

}
