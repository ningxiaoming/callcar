package com.notname.callcar.module.req;

import com.notname.callcar.entity.CcCollectionEntity;
import lombok.Data;

@Data
public class CcCollectionSaveReq extends CcCollectionEntity {

    private String bizType;

}
