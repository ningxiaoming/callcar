package com.notname.callcar.module.req;

import lombok.Data;

import java.util.Map;

@Data
public class CitiVO {

    private Map<String,String> province_list;

    private Map<String,String> city_list;

    private Map<String,String> unty_list;


}
