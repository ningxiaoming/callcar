package com.notname.callcar.module.req;

import com.notname.callcar.entity.CcCommentEntity;
import lombok.Data;

@Data
public class CcCommentSaveReq extends CcCommentEntity {


    /**
     * 业务类型,头条信息:2,评论是3
     */
    private String bizType;


}
