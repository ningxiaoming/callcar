package com.notname.callcar.module.req;

import com.notname.callcar.entity.CcHeadLineEntity;
import com.notname.callcar.module.res.CcUserVO;
import lombok.Data;

import java.util.List;

@Data
public class CcHeadLineSaveReq extends CcHeadLineEntity {

    /**
     * 图片urls
     */
    private List<String> hlImgUrls;

    /**
     * 业务类型code:疫情信息:1，头条信息:2 ....
     */
    private String bizType;



}
