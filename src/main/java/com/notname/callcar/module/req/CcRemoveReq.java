package com.notname.callcar.module.req;

import lombok.Data;

@Data
public class CcRemoveReq {

    private String bizType;

    private String bizCode;
}
