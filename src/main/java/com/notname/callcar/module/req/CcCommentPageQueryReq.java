package com.notname.callcar.module.req;

import com.notname.callcar.common.CommonPageQueryReq;
import lombok.Data;

@Data
public class CcCommentPageQueryReq extends CommonPageQueryReq {


    /**
     * 帖子code
     */
    private String bizCode;


    /**
     * 当前userCode
     */
    private String currentUserCode;

}
