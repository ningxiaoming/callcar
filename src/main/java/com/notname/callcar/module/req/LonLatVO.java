package com.notname.callcar.module.req;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Data;

@Data
@ApiOperation("经纬度入参")
public class LonLatVO {

    @ApiModelProperty("经度")
    private String lon;

    @ApiModelProperty("维度")
    private String lat;

}
