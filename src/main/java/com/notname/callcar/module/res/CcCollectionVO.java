package com.notname.callcar.module.res;

import com.notname.callcar.entity.CcActivityEntity;
import com.notname.callcar.entity.CcCollectionEntity;
import lombok.Data;

@Data
public class CcCollectionVO extends CcCollectionEntity {

    private CcUserMsgVO ccUserMsgVO;

    private CcDriverMsgVO ccDriverMsgVO;

    private CcHeadLineVO ccHeadLineVO;
}
