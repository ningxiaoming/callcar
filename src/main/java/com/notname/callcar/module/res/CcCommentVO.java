package com.notname.callcar.module.res;

import com.notname.callcar.entity.CcCommentEntity;
import lombok.Data;

@Data
public class CcCommentVO extends CcCommentEntity {

    /**
     * 当前浏览用户是否有点赞
     */
    private Boolean liked;

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 头像URL
     */
    private String avatarUrl;


}
