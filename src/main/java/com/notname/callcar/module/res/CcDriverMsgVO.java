package com.notname.callcar.module.res;

import com.notname.callcar.entity.CcDriverMsgEntity;
import com.notname.callcar.entity.CcUserEntity;
import lombok.Data;

import java.io.Serializable;

@Data
public class CcDriverMsgVO extends CcDriverMsgEntity implements Serializable {

    private CcUserVO ccUserVO;


    /**
     * 起始省Name
     */
    private String startProvinceName;

    /**
     * 起始市Name
     */
    private String startCityName;

    /**
     * 起始区Name
     */
    private String startAreaName;

    /**
     * 目的省Name
     */
    private String endProvinceName;

    /**
     * 目的市Name
     */
    private String endCityName;

    /**
     * 目睹区Name
     */
    private String endAreaName;


}
