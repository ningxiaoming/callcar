package com.notname.callcar.module.res;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@ApiModel
@Data
public class PageResult<T> {

    @ApiModelProperty(
        name = "pageNum",
        value = "页号"
    )
    private Integer pageNum;
    @ApiModelProperty(
        name = "pageSize",
        value = "页大小"
    )
    private Integer pageSize;
    @ApiModelProperty(
        name = "total",
        value = "总条数"
    )
    private Long total;
    @ApiModelProperty(
        name = "pages",
        value = "总页数"
    )
    private Integer pages;
    @ApiModelProperty(
        name = "list",
        value = "结果列表"
    )
    private List<T> list;

}
