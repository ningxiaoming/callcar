package com.notname.callcar.module.res;

import com.notname.callcar.dao.CcHeadLineDao;
import com.notname.callcar.entity.CcHeadLineEntity;
import com.notname.callcar.entity.CcUserMsgEntity;
import lombok.Data;

import java.util.List;

@Data
public class CcHeadLineVO extends CcHeadLineEntity {

    private CcUserVO ccUserVO;


    /**
     * 起始省Name
     */
    private String startProvinceName;

    /**
     * 起始市Name
     */
    private String startCityName;

    /**
     * 起始区Name
     */
    private String startAreaName;

    /**
     * 目的省Name
     */
    private String endProvinceName;

    /**
     * 目的市Name
     */
    private String endCityName;

    /**
     * 目睹区Name
     */
    private String endAreaName;


    /**
     * 图片urls
     */
    private List<String> hlImgUrls;

    /**
     * 当前浏览用户是否有点赞
     */
    private Boolean liked;

}
