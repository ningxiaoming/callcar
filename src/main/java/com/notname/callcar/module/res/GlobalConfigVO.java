package com.notname.callcar.module.res;

import lombok.Data;

import java.util.List;

@Data
public class GlobalConfigVO {


    private List<CcActivityVO> ccActivityVOS;

    private String logoUrl;

    // todo 其他
}
