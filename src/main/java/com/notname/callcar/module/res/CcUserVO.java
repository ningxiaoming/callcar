package com.notname.callcar.module.res;

import com.notname.callcar.dao.CcUserDao;
import com.notname.callcar.entity.CcUserEntity;
import lombok.Data;

@Data
public class CcUserVO extends CcUserEntity {
}
