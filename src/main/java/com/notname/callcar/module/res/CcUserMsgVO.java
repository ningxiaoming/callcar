package com.notname.callcar.module.res;

import com.notname.callcar.entity.CcUserMsgEntity;
import lombok.Data;

@Data
public class CcUserMsgVO extends CcUserMsgEntity {

    private CcUserVO ccUserVO;



    /**
     * 起始省Name
     */
    private String startProvinceName;

    /**
     * 起始市Name
     */
    private String startCityName;

    /**
     * 起始区Name
     */
    private String startAreaName;

    /**
     * 目的省Name
     */
    private String endProvinceName;

    /**
     * 目的市Name
     */
    private String endCityName;

    /**
     * 目睹区Name
     */
    private String endAreaName;


}
