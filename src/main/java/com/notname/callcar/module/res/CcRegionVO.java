package com.notname.callcar.module.res;

import lombok.Data;

@Data
public class CcRegionVO {

    /**
     * 省code
     */
    private String provinceCode;

    /**
     * 市code
     */
    private String cityCode;

    /**
     * 区code
     */
    private String areaCode;


    /**
     * 省Name
     */
    private String provinceName;

    /**
     * 市Name
     */
    private String cityName;

    /**
     * 区Name
     */
    private String areaName;


}
