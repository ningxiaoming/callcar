package com.notname.callcar.common.exception;


public enum CcErrorCodeEnum {

    /**
     * 系统错误
     */
    SYSTEM_ERROR("-1", "系统错误"),

    /**
     * 微信系统异常
     */
    WX_SYSTEM_ERROR("-2", "微信系统异常"),

    /**
     * 业务逻辑异常
     */
    BIZ_ERROR("1001", "业务逻辑异常"),

    /**
     * 参数错误
     */
    INVALID_PARAMS("1002", "参数错误"),

    /**
     * 重复错误
     */
    DUPLICATE_ERROR("1003", "重复错误"),

    /**
     * 并发异常
     */
    SYNCHRONIZED_EXCEPTION("1004",
            "并发异常!"),

    /**
     * 幂等异常
     */
    IDEMPOTENT_EXCEPTION("1005",
            "幂等异常!"),
    /**
     * 用户不存在
     */
    USER_NOT_EXIST_BIZ_ERROR("1006", "用户不存在"),

    /**
     * 无权限
     */
    NO_PERMISSION("1007", "无权限"),

    /**
     * 订单已存在异常
     */
    ORDER_ALREADY_EXIST_BIZ_ERROR("ORDER_ALREADY_EXIST_BIZ_ERROR", "pt order already exist"),
    /**
     * 订单不存在异常
     */
    ORDER_NOT_EXIST_ERROR("ORDER_NOT_EXIST_ERROR", "pt order not exist"),


    /**
     * 业务编码不存在异常
     */
    BUSINESS_CODE_NOT_EXIST_BIZ_ERROR("BUSINESS_CODE_NOT_EXIST_BIZ_ERROR", "Business code not exist"),


    /**
     * 发送消息异常
     */
    SEND_MESSAGE_TASK_BIZ_ERROR("SEND_MESSAGE_TASK_BIZ_ERROR", "发送消息异常"),
    /**
     * 消费消息异常
     */
    CONSUMER_MESSAGE_BIZ_ERROR("CONSUMER_MESSAGE_BIZ_ERROR", "消费消息异常"),

    /**
     * 获取角色失败
     */
    OBTAIN_OPERATOR_ROLE_FAIL("OBTAIN_OPERATOR_ROLE_FAIL", "obtain operator role fail"),


    ;

    private String errCode;
    private String errDesc;

    CcErrorCodeEnum(String errCode, String errDesc) {
        this.errCode = errCode;
        this.errDesc = errDesc;
    }

    public String getErrCode() {
        return errCode;
    }

    public String getErrDesc() {
        return errDesc;
    }

}

