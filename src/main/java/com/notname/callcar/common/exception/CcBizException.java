package com.notname.callcar.common.exception;

/**
 * 业务异常
 *
 * @author 诣林
 * @since 201/11/26
 */
public class CcBizException extends RuntimeException {
    private static final long serialVersionUID = -2380155128704037445L;
    private String errorCode;
    private String errorMsg;

    public CcBizException(String errorCode, String errorMsg) {
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }
    public CcBizException(CcErrorCodeEnum errorCodeEnum) {
        super(String.format("ErrorCode:%s, ErrorMsg:%s",
                errorCodeEnum.getErrCode(), errorCodeEnum.getErrDesc()));
        this.errorCode = errorCodeEnum.getErrCode();
        this.errorMsg = errorCodeEnum.getErrDesc();
    }

    public CcBizException(CcErrorCodeEnum errorCodeEnum, String message) {
        super(String.format("ErrorCode:%s, ErrorMsg:%s",
                errorCodeEnum == null ? "" : String.valueOf(errorCodeEnum.getErrCode()),
                message == null ? String.valueOf(errorCodeEnum.getErrDesc()) :message ));
        this.errorCode = errorCodeEnum.getErrCode();
        this.errorMsg = message;
    }

    public CcBizException(String orderCode, CcErrorCodeEnum errorCodeEnum, String message) {
        super(String.format("OrderCode:%s, ErrorCode:%s, ErrorMsg:%s",
                orderCode, errorCodeEnum == null ? "" : String.valueOf(errorCodeEnum.getErrCode()), message == null ? "" : String.valueOf(errorCodeEnum.getErrDesc())));
        this.errorCode = errorCodeEnum.getErrCode();
        this.errorMsg = message;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

}
