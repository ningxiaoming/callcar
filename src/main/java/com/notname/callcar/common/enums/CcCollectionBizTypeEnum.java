package com.notname.callcar.common.enums;


import lombok.Data;

public enum CcCollectionBizTypeEnum {

    USER_MSG(1,"人找车"),

    DRIVER_MSG(2,"车找人"),

    HEAD_LINE(3,"头条信息"),



    ;

    private Integer code;
    private String desc;

    CcCollectionBizTypeEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

}

