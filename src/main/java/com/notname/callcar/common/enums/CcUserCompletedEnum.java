package com.notname.callcar.common.enums;

import lombok.Getter;

@Getter
public enum CcUserCompletedEnum {


    COMPLETED(1,"已补全"),

    NOT_COMPLETED(0,"没有补全"),


            ;

    private Integer code;

    private String desc;

    CcUserCompletedEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

}
