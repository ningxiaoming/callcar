package com.notname.callcar.common.enums;


public enum CcActivityStatusEnum {

    UP(1, "上线"),
    DOWN(0, "下线"),


    ;

    private Integer code;
    private String desc;

    CcActivityStatusEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

}

