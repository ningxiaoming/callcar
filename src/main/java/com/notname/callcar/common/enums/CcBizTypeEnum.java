package com.notname.callcar.common.enums;


public enum CcBizTypeEnum {

    /**
     * 疫情快讯
     */
    YI_QING(1, "疫情快讯"),

    HEAD_LINE(2,"头条信息"),

    COMMENT(3,"评论"),


    ;

    private Integer code;
    private String desc;

    CcBizTypeEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

}

