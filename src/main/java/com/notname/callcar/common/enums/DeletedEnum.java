package com.notname.callcar.common.enums;

import lombok.Data;
import lombok.Getter;

@Getter
public enum DeletedEnum {

    DELETE(1,"删除"),

    NOT_DELETE(0,"不删除"),



    ;


    private Integer code;

    private String desc;

    DeletedEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
