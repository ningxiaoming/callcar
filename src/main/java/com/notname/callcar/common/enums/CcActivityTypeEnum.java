package com.notname.callcar.common.enums;


public enum CcActivityTypeEnum {

    /**
     * 疫情快讯轮播图
     */
    YI_QING(1, "疫情快讯"),


    ;

    private Integer code;
    private String desc;

    CcActivityTypeEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

}

