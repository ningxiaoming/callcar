package com.notname.callcar.common;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class CommonRes<T> {

    private String code;

    private String msg;

    private T data;


}
