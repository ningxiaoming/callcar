package com.notname.callcar.common;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class CommonPageQueryReq {

    @ApiModelProperty(name = "pageSize", value = "分页数量", required = true, example = "1")
    private Integer pageSize = 10;

    @ApiModelProperty(name = "pageNum", value = "分页页数", required = true, example = "1")
    private Integer pageNum = 1;

}
