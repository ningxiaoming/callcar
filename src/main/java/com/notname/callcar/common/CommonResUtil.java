package com.notname.callcar.common;

import com.notname.callcar.common.exception.CcErrorCodeEnum;

public class CommonResUtil {

    public static <T> CommonRes<T> buildCommonRes(T data) {
        return buildCommonRes("0","success",data);
    }

    public static <T> CommonRes<T> buildCommonRes(String code, String msg, T data) {
        CommonRes<T> commonRes = new CommonRes<>();
        commonRes.setCode(code);
        commonRes.setMsg(msg);
        commonRes.setData(data);
        return commonRes;
    }

    public static <T> CommonRes<T> buildCommonRes(String code, String msg) {
        CommonRes<T> response = new CommonRes<>();
        response.setCode(code);
        response.setMsg(msg);
        return response;
    }
    public static <T> CommonRes<T> buildCommonRes(CcErrorCodeEnum errorCodeEnum) {
        CommonRes<T> response = new CommonRes<>();
        response.setCode(errorCodeEnum.getErrCode());
        response.setMsg(errorCodeEnum.getErrDesc());
        T t = null;
        response.setData(t);
        return response;
    }

}
