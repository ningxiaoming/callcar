package com.notname.callcar.common;

import com.notname.callcar.module.req.LonLatVO;
import lombok.Data;

@Data
public class CommonReq<T> {

    private T data;

    private LonLatVO lonLatVO;

    private String version;

}
