package com.notname.callcar.infrastructure.module.wx;

import lombok.Data;

@Data
public class WxCosBaseRes {

    private String code;

    private String msg;

    private WxCosConfig data;

}
