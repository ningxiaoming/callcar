package com.notname.callcar.infrastructure.module.gaode;

import lombok.Data;

@Data
public class GaodeBaseRes {

    private String infocode;

    private String status;

    private String info;

    private RegeoCode regeocode;


}
