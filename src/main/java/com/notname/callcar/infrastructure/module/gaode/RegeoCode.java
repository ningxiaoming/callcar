package com.notname.callcar.infrastructure.module.gaode;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

@Data
public class RegeoCode {


    @JSONField(name = "formatted_address")
    private String formattedAddress;

    private AddressComponent addressComponent;

}
