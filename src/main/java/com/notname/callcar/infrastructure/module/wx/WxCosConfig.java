package com.notname.callcar.infrastructure.module.wx;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

@Data
public class WxCosConfig {

    @JSONField(name = "TmpSecretId")
    private String TmpSecretId;

    @JSONField(name = "TmpSecretKey")
    private String TmpSecretKey;

    @JSONField(name = "Token")
    private String Token;

    @JSONField(name = "ExpiredTime")
    private Long ExpiredTime;
}
