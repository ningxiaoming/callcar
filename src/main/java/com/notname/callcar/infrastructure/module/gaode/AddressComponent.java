package com.notname.callcar.infrastructure.module.gaode;

import lombok.Data;

@Data
public class AddressComponent {

    private String country;

    private String province;
    private String city;
    private String district;
    private String township;

    private String citycode;
    private String adcode;
    private String towncode;


}
