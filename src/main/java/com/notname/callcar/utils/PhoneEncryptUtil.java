package com.notname.callcar.utils;

import cn.hutool.crypto.Mode;
import cn.hutool.crypto.Padding;
import cn.hutool.crypto.symmetric.AES;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.Assert;

import java.util.regex.Pattern;


@Slf4j
public class PhoneEncryptUtil {

    /**
     * 手机号加密的盐，长度必须是16
     */
    static String key = "callcarcallcarrr";

    private static AES aes = new AES(Mode.ECB, Padding.PKCS5Padding, key.getBytes());

    public static String encryptBase64(String str) {
        return aes.encryptBase64(str);
    }

    public static String decryptStr(String str) {
        try {
            str = aes.decryptStr(str);
        } catch (Exception e) {
            log.error("手机号解密失败", e);
        }
        return str;
    }

    // 手机号码前三后四脱敏
    public static String mobileEncrypt(String str) {
        String mobile = decryptStr(str);
        if (StringUtils.isEmpty(mobile) || (mobile.length() != 11)) {
            return mobile;
        }
        return mobile.replaceAll("(\\w{3})\\w*(\\w{4})", "$1****$2");
    }

    public static boolean isValidPhoneNumber(String phoneNumber) {
        if ((phoneNumber != null) && (!phoneNumber.isEmpty())) {
            return Pattern.matches("^1[3-9]\\d{9}$", phoneNumber);
        }
        return false;
    }


    /**
     * main
     *
     * @param args
     */
    public static void main(String[] args) {
        String val = "callcarcallcarrr";

        byte[] bytes = val.getBytes();

        String key = "5QV3rdu95YO2Eyjz40BbYw==";

        String s = mobileEncrypt(key);
        // 加密并进行Base转码
        String encrypt = aes.encryptBase64(key);
        System.out.println(encrypt);

        String decrypt = aes.decryptStr(encrypt);
        System.out.println(decrypt);


        Assert.isTrue(!PhoneEncryptUtil.isValidPhoneNumber("7693128385"),"非法的电话号");


        System.out.println(decryptStr("NocaI4fkTzgKuDA7gHkb8Q=="));
    }
}