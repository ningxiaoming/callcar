package com.notname.callcar.utils;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import com.notname.callcar.infrastructure.module.wx.WxCosBaseRes;
import com.notname.callcar.infrastructure.module.wx.WxCosConfig;
import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.auth.BasicSessionCredentials;
import com.qcloud.cos.auth.COSCredentials;
import com.qcloud.cos.http.HttpProtocol;
import com.qcloud.cos.model.COSObject;
import com.qcloud.cos.model.ObjectMetadata;
import com.qcloud.cos.model.PutObjectRequest;
import com.qcloud.cos.model.PutObjectResult;
import com.qcloud.cos.region.Region;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/*
 * 腾讯云cos工具类*/
@Component
@Slf4j
public class WxCosUtils {

    String bucketName = "7072-prod-9gvi1d20a207c70c-1314768774"; //桶的名称
    BasicSessionCredentials cred = null;
    ClientConfig clientConfig = null;
    COSClient cosClient = null;

    Long expiredTime = 1L;

//    @PostConstruct
    public synchronized void init() {
        if (System.currentTimeMillis() < expiredTime) {
            return;
        }
        String result = HttpUtil.createPost("http://api.weixin.qq.com/_/cos/getauth").body("").execute().body();
        if (ObjectUtil.isEmpty(result)) {
            // 告警
            log.error("获取wxCos配置异常");
            return;
        }
        WxCosConfig config = JSON.parseObject(result, WxCosConfig.class);
        log.info("config : {}",JSON.toJSONString(config));
        expiredTime = (config.getExpiredTime() * 1000) - (2 * 60 * 1000);
        // 1 初始化用户身份信息（secretId, secretKey）。
        cred = new BasicSessionCredentials(config.getTmpSecretId(), config.getTmpSecretKey(),config.getToken());
        // 2 设置 bucket 的地域, COS 地域的简称请参照 https://cloud.tencent.com/document/product/436/6224
        // clientConfig 中包含了设置 region, https(默认 http), 超时, 代理等 set 方法, 使用可参见源码或者常见问题 Java SDK 部分。
        Region region = new Region("ap-shanghai");
        clientConfig = new ClientConfig(region);
        // 这里建议设置使用 https 协议
        clientConfig.setHttpProtocol(HttpProtocol.https);
        // 3 生成 cos 客户端。
        cosClient = new COSClient(cred, clientConfig);
    }

    public static void main(String[] args) throws IOException {
//        upload2cloud("C:\\Users\\Administrator\\Desktop\\11.gif","test");
//        InputStream in = new FileInputStream("C:\\Users\\Administrator\\Desktop\\11.gif");
//        byte[] bytes = new byte[in.available()];
//        upload2cloud(bytes,"ss");
//        deleteFileFromcloud("test");
    }

    public void upload2cloud(String filepath, String fileName) {
        if (System.currentTimeMillis() > expiredTime) {
            init();
        }
        // 指定要上传的文件
        File localFile = new File(filepath);
        // 指定文件上传到 COS 上的路径，即对象键。例如对象键为folder/picture.jpg，则表示将文件 picture.jpg 上传到 folder 路径下
        PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, fileName, localFile);
        PutObjectResult putObjectResult = cosClient.putObject(putObjectRequest);

        String eTag = putObjectResult.getETag();
        System.out.println(eTag);
    }

    public COSObject getObject(String key) {
        if (System.currentTimeMillis() > expiredTime) {
            init();
        }

        return cosClient.getObject(bucketName, key);
    }

    public PutObjectResult upload2cloud(byte[] bytes, String fileName) {
        if (System.currentTimeMillis() > expiredTime) {
            init();
        }
        int length = bytes.length;
        // 获取文件流
        InputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
        ObjectMetadata objectMetadata = new ObjectMetadata();
// 从输入流上传必须制定content length, 否则http客户端可能会缓存所有数据，存在内存OOM的情况
        objectMetadata.setContentLength(length);
// 默认下载时根据cos路径key的后缀返回响应的contenttype, 上传时设置contenttype会覆盖默认值

        PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, fileName, byteArrayInputStream, objectMetadata);
        PutObjectResult putObjectResult = cosClient.putObject(putObjectRequest);
        return putObjectResult;
    }

    public void deleteFileFromcloud(String fileName) {
        if (System.currentTimeMillis() > expiredTime) {
            init();
        }
        cosClient.deleteObject(bucketName, fileName);
    }

    /**
     * 关闭 COSClient 实例，在 springboot 销毁时调用
     */
    @PreDestroy
    public void shutdownClient() {
        // 关闭客户端(关闭后台线程)
        log.warn("销毁COS实例");
        cosClient.shutdown();
    }


}