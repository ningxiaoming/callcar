package com.notname.callcar.entity;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * cc_like
 * @author 
 */
@Data
public class CcLikeEntity implements Serializable {
    private Long id;

    /**
     * 创建时间
     */
    private Date gmtCreate;

    /**
     * 修改时间
     */
    private Date gmtModified;

    /**
     * 逻辑删除标
     */
    private Integer deleted;

    /**
     * 点赞code
     */
    private String likeCode;

    /**
     * 业务类型code,1:疫情信息，2:头条信息，3:评论
     */
    private Integer bizTypeCode;

    /**
     * 业务code
     */
    private String bizCode;

    /**
     * 创建者code
     */
    private String creatorCode;

    /**
     * 扩展字段 json方式存储
     */
    private String feature;

    private static final long serialVersionUID = 1L;
}