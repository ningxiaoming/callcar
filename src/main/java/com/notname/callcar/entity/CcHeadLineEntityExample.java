package com.notname.callcar.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CcHeadLineEntityExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    private Integer limit;

    private Long offset;

    public CcHeadLineEntityExample() {
        oredCriteria = new ArrayList<>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Long offset) {
        this.offset = offset;
    }

    public Long getOffset() {
        return offset;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIsNull() {
            addCriterion("gmt_create is null");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIsNotNull() {
            addCriterion("gmt_create is not null");
            return (Criteria) this;
        }

        public Criteria andGmtCreateEqualTo(Date value) {
            addCriterion("gmt_create =", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotEqualTo(Date value) {
            addCriterion("gmt_create <>", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateGreaterThan(Date value) {
            addCriterion("gmt_create >", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateGreaterThanOrEqualTo(Date value) {
            addCriterion("gmt_create >=", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateLessThan(Date value) {
            addCriterion("gmt_create <", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateLessThanOrEqualTo(Date value) {
            addCriterion("gmt_create <=", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIn(List<Date> values) {
            addCriterion("gmt_create in", values, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotIn(List<Date> values) {
            addCriterion("gmt_create not in", values, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateBetween(Date value1, Date value2) {
            addCriterion("gmt_create between", value1, value2, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotBetween(Date value1, Date value2) {
            addCriterion("gmt_create not between", value1, value2, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedIsNull() {
            addCriterion("gmt_modified is null");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedIsNotNull() {
            addCriterion("gmt_modified is not null");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedEqualTo(Date value) {
            addCriterion("gmt_modified =", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedNotEqualTo(Date value) {
            addCriterion("gmt_modified <>", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedGreaterThan(Date value) {
            addCriterion("gmt_modified >", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedGreaterThanOrEqualTo(Date value) {
            addCriterion("gmt_modified >=", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedLessThan(Date value) {
            addCriterion("gmt_modified <", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedLessThanOrEqualTo(Date value) {
            addCriterion("gmt_modified <=", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedIn(List<Date> values) {
            addCriterion("gmt_modified in", values, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedNotIn(List<Date> values) {
            addCriterion("gmt_modified not in", values, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedBetween(Date value1, Date value2) {
            addCriterion("gmt_modified between", value1, value2, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedNotBetween(Date value1, Date value2) {
            addCriterion("gmt_modified not between", value1, value2, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andDeletedIsNull() {
            addCriterion("deleted is null");
            return (Criteria) this;
        }

        public Criteria andDeletedIsNotNull() {
            addCriterion("deleted is not null");
            return (Criteria) this;
        }

        public Criteria andDeletedEqualTo(Integer value) {
            addCriterion("deleted =", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedNotEqualTo(Integer value) {
            addCriterion("deleted <>", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedGreaterThan(Integer value) {
            addCriterion("deleted >", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedGreaterThanOrEqualTo(Integer value) {
            addCriterion("deleted >=", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedLessThan(Integer value) {
            addCriterion("deleted <", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedLessThanOrEqualTo(Integer value) {
            addCriterion("deleted <=", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedIn(List<Integer> values) {
            addCriterion("deleted in", values, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedNotIn(List<Integer> values) {
            addCriterion("deleted not in", values, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedBetween(Integer value1, Integer value2) {
            addCriterion("deleted between", value1, value2, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedNotBetween(Integer value1, Integer value2) {
            addCriterion("deleted not between", value1, value2, "deleted");
            return (Criteria) this;
        }

        public Criteria andHlCodeIsNull() {
            addCriterion("hl_code is null");
            return (Criteria) this;
        }

        public Criteria andHlCodeIsNotNull() {
            addCriterion("hl_code is not null");
            return (Criteria) this;
        }

        public Criteria andHlCodeEqualTo(String value) {
            addCriterion("hl_code =", value, "hlCode");
            return (Criteria) this;
        }

        public Criteria andHlCodeNotEqualTo(String value) {
            addCriterion("hl_code <>", value, "hlCode");
            return (Criteria) this;
        }

        public Criteria andHlCodeGreaterThan(String value) {
            addCriterion("hl_code >", value, "hlCode");
            return (Criteria) this;
        }

        public Criteria andHlCodeGreaterThanOrEqualTo(String value) {
            addCriterion("hl_code >=", value, "hlCode");
            return (Criteria) this;
        }

        public Criteria andHlCodeLessThan(String value) {
            addCriterion("hl_code <", value, "hlCode");
            return (Criteria) this;
        }

        public Criteria andHlCodeLessThanOrEqualTo(String value) {
            addCriterion("hl_code <=", value, "hlCode");
            return (Criteria) this;
        }

        public Criteria andHlCodeLike(String value) {
            addCriterion("hl_code like", value, "hlCode");
            return (Criteria) this;
        }

        public Criteria andHlCodeNotLike(String value) {
            addCriterion("hl_code not like", value, "hlCode");
            return (Criteria) this;
        }

        public Criteria andHlCodeIn(List<String> values) {
            addCriterion("hl_code in", values, "hlCode");
            return (Criteria) this;
        }

        public Criteria andHlCodeNotIn(List<String> values) {
            addCriterion("hl_code not in", values, "hlCode");
            return (Criteria) this;
        }

        public Criteria andHlCodeBetween(String value1, String value2) {
            addCriterion("hl_code between", value1, value2, "hlCode");
            return (Criteria) this;
        }

        public Criteria andHlCodeNotBetween(String value1, String value2) {
            addCriterion("hl_code not between", value1, value2, "hlCode");
            return (Criteria) this;
        }

        public Criteria andBizTypeCodeIsNull() {
            addCriterion("biz_type_code is null");
            return (Criteria) this;
        }

        public Criteria andBizTypeCodeIsNotNull() {
            addCriterion("biz_type_code is not null");
            return (Criteria) this;
        }

        public Criteria andBizTypeCodeEqualTo(Integer value) {
            addCriterion("biz_type_code =", value, "bizTypeCode");
            return (Criteria) this;
        }

        public Criteria andBizTypeCodeNotEqualTo(Integer value) {
            addCriterion("biz_type_code <>", value, "bizTypeCode");
            return (Criteria) this;
        }

        public Criteria andBizTypeCodeGreaterThan(Integer value) {
            addCriterion("biz_type_code >", value, "bizTypeCode");
            return (Criteria) this;
        }

        public Criteria andBizTypeCodeGreaterThanOrEqualTo(Integer value) {
            addCriterion("biz_type_code >=", value, "bizTypeCode");
            return (Criteria) this;
        }

        public Criteria andBizTypeCodeLessThan(Integer value) {
            addCriterion("biz_type_code <", value, "bizTypeCode");
            return (Criteria) this;
        }

        public Criteria andBizTypeCodeLessThanOrEqualTo(Integer value) {
            addCriterion("biz_type_code <=", value, "bizTypeCode");
            return (Criteria) this;
        }

        public Criteria andBizTypeCodeIn(List<Integer> values) {
            addCriterion("biz_type_code in", values, "bizTypeCode");
            return (Criteria) this;
        }

        public Criteria andBizTypeCodeNotIn(List<Integer> values) {
            addCriterion("biz_type_code not in", values, "bizTypeCode");
            return (Criteria) this;
        }

        public Criteria andBizTypeCodeBetween(Integer value1, Integer value2) {
            addCriterion("biz_type_code between", value1, value2, "bizTypeCode");
            return (Criteria) this;
        }

        public Criteria andBizTypeCodeNotBetween(Integer value1, Integer value2) {
            addCriterion("biz_type_code not between", value1, value2, "bizTypeCode");
            return (Criteria) this;
        }

        public Criteria andHeadLineTimeIsNull() {
            addCriterion("head_line_time is null");
            return (Criteria) this;
        }

        public Criteria andHeadLineTimeIsNotNull() {
            addCriterion("head_line_time is not null");
            return (Criteria) this;
        }

        public Criteria andHeadLineTimeEqualTo(Long value) {
            addCriterion("head_line_time =", value, "headLineTime");
            return (Criteria) this;
        }

        public Criteria andHeadLineTimeNotEqualTo(Long value) {
            addCriterion("head_line_time <>", value, "headLineTime");
            return (Criteria) this;
        }

        public Criteria andHeadLineTimeGreaterThan(Long value) {
            addCriterion("head_line_time >", value, "headLineTime");
            return (Criteria) this;
        }

        public Criteria andHeadLineTimeGreaterThanOrEqualTo(Long value) {
            addCriterion("head_line_time >=", value, "headLineTime");
            return (Criteria) this;
        }

        public Criteria andHeadLineTimeLessThan(Long value) {
            addCriterion("head_line_time <", value, "headLineTime");
            return (Criteria) this;
        }

        public Criteria andHeadLineTimeLessThanOrEqualTo(Long value) {
            addCriterion("head_line_time <=", value, "headLineTime");
            return (Criteria) this;
        }

        public Criteria andHeadLineTimeIn(List<Long> values) {
            addCriterion("head_line_time in", values, "headLineTime");
            return (Criteria) this;
        }

        public Criteria andHeadLineTimeNotIn(List<Long> values) {
            addCriterion("head_line_time not in", values, "headLineTime");
            return (Criteria) this;
        }

        public Criteria andHeadLineTimeBetween(Long value1, Long value2) {
            addCriterion("head_line_time between", value1, value2, "headLineTime");
            return (Criteria) this;
        }

        public Criteria andHeadLineTimeNotBetween(Long value1, Long value2) {
            addCriterion("head_line_time not between", value1, value2, "headLineTime");
            return (Criteria) this;
        }

        public Criteria andContentIsNull() {
            addCriterion("content is null");
            return (Criteria) this;
        }

        public Criteria andContentIsNotNull() {
            addCriterion("content is not null");
            return (Criteria) this;
        }

        public Criteria andContentEqualTo(String value) {
            addCriterion("content =", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotEqualTo(String value) {
            addCriterion("content <>", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentGreaterThan(String value) {
            addCriterion("content >", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentGreaterThanOrEqualTo(String value) {
            addCriterion("content >=", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLessThan(String value) {
            addCriterion("content <", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLessThanOrEqualTo(String value) {
            addCriterion("content <=", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLike(String value) {
            addCriterion("content like", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotLike(String value) {
            addCriterion("content not like", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentIn(List<String> values) {
            addCriterion("content in", values, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotIn(List<String> values) {
            addCriterion("content not in", values, "content");
            return (Criteria) this;
        }

        public Criteria andContentBetween(String value1, String value2) {
            addCriterion("content between", value1, value2, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotBetween(String value1, String value2) {
            addCriterion("content not between", value1, value2, "content");
            return (Criteria) this;
        }

        public Criteria andLikeCountIsNull() {
            addCriterion("like_count is null");
            return (Criteria) this;
        }

        public Criteria andLikeCountIsNotNull() {
            addCriterion("like_count is not null");
            return (Criteria) this;
        }

        public Criteria andLikeCountEqualTo(Integer value) {
            addCriterion("like_count =", value, "likeCount");
            return (Criteria) this;
        }

        public Criteria andLikeCountNotEqualTo(Integer value) {
            addCriterion("like_count <>", value, "likeCount");
            return (Criteria) this;
        }

        public Criteria andLikeCountGreaterThan(Integer value) {
            addCriterion("like_count >", value, "likeCount");
            return (Criteria) this;
        }

        public Criteria andLikeCountGreaterThanOrEqualTo(Integer value) {
            addCriterion("like_count >=", value, "likeCount");
            return (Criteria) this;
        }

        public Criteria andLikeCountLessThan(Integer value) {
            addCriterion("like_count <", value, "likeCount");
            return (Criteria) this;
        }

        public Criteria andLikeCountLessThanOrEqualTo(Integer value) {
            addCriterion("like_count <=", value, "likeCount");
            return (Criteria) this;
        }

        public Criteria andLikeCountIn(List<Integer> values) {
            addCriterion("like_count in", values, "likeCount");
            return (Criteria) this;
        }

        public Criteria andLikeCountNotIn(List<Integer> values) {
            addCriterion("like_count not in", values, "likeCount");
            return (Criteria) this;
        }

        public Criteria andLikeCountBetween(Integer value1, Integer value2) {
            addCriterion("like_count between", value1, value2, "likeCount");
            return (Criteria) this;
        }

        public Criteria andLikeCountNotBetween(Integer value1, Integer value2) {
            addCriterion("like_count not between", value1, value2, "likeCount");
            return (Criteria) this;
        }

        public Criteria andCommentCountIsNull() {
            addCriterion("comment_count is null");
            return (Criteria) this;
        }

        public Criteria andCommentCountIsNotNull() {
            addCriterion("comment_count is not null");
            return (Criteria) this;
        }

        public Criteria andCommentCountEqualTo(Integer value) {
            addCriterion("comment_count =", value, "commentCount");
            return (Criteria) this;
        }

        public Criteria andCommentCountNotEqualTo(Integer value) {
            addCriterion("comment_count <>", value, "commentCount");
            return (Criteria) this;
        }

        public Criteria andCommentCountGreaterThan(Integer value) {
            addCriterion("comment_count >", value, "commentCount");
            return (Criteria) this;
        }

        public Criteria andCommentCountGreaterThanOrEqualTo(Integer value) {
            addCriterion("comment_count >=", value, "commentCount");
            return (Criteria) this;
        }

        public Criteria andCommentCountLessThan(Integer value) {
            addCriterion("comment_count <", value, "commentCount");
            return (Criteria) this;
        }

        public Criteria andCommentCountLessThanOrEqualTo(Integer value) {
            addCriterion("comment_count <=", value, "commentCount");
            return (Criteria) this;
        }

        public Criteria andCommentCountIn(List<Integer> values) {
            addCriterion("comment_count in", values, "commentCount");
            return (Criteria) this;
        }

        public Criteria andCommentCountNotIn(List<Integer> values) {
            addCriterion("comment_count not in", values, "commentCount");
            return (Criteria) this;
        }

        public Criteria andCommentCountBetween(Integer value1, Integer value2) {
            addCriterion("comment_count between", value1, value2, "commentCount");
            return (Criteria) this;
        }

        public Criteria andCommentCountNotBetween(Integer value1, Integer value2) {
            addCriterion("comment_count not between", value1, value2, "commentCount");
            return (Criteria) this;
        }

        public Criteria andStartProvinceCodeIsNull() {
            addCriterion("start_province_code is null");
            return (Criteria) this;
        }

        public Criteria andStartProvinceCodeIsNotNull() {
            addCriterion("start_province_code is not null");
            return (Criteria) this;
        }

        public Criteria andStartProvinceCodeEqualTo(String value) {
            addCriterion("start_province_code =", value, "startProvinceCode");
            return (Criteria) this;
        }

        public Criteria andStartProvinceCodeNotEqualTo(String value) {
            addCriterion("start_province_code <>", value, "startProvinceCode");
            return (Criteria) this;
        }

        public Criteria andStartProvinceCodeGreaterThan(String value) {
            addCriterion("start_province_code >", value, "startProvinceCode");
            return (Criteria) this;
        }

        public Criteria andStartProvinceCodeGreaterThanOrEqualTo(String value) {
            addCriterion("start_province_code >=", value, "startProvinceCode");
            return (Criteria) this;
        }

        public Criteria andStartProvinceCodeLessThan(String value) {
            addCriterion("start_province_code <", value, "startProvinceCode");
            return (Criteria) this;
        }

        public Criteria andStartProvinceCodeLessThanOrEqualTo(String value) {
            addCriterion("start_province_code <=", value, "startProvinceCode");
            return (Criteria) this;
        }

        public Criteria andStartProvinceCodeLike(String value) {
            addCriterion("start_province_code like", value, "startProvinceCode");
            return (Criteria) this;
        }

        public Criteria andStartProvinceCodeNotLike(String value) {
            addCriterion("start_province_code not like", value, "startProvinceCode");
            return (Criteria) this;
        }

        public Criteria andStartProvinceCodeIn(List<String> values) {
            addCriterion("start_province_code in", values, "startProvinceCode");
            return (Criteria) this;
        }

        public Criteria andStartProvinceCodeNotIn(List<String> values) {
            addCriterion("start_province_code not in", values, "startProvinceCode");
            return (Criteria) this;
        }

        public Criteria andStartProvinceCodeBetween(String value1, String value2) {
            addCriterion("start_province_code between", value1, value2, "startProvinceCode");
            return (Criteria) this;
        }

        public Criteria andStartProvinceCodeNotBetween(String value1, String value2) {
            addCriterion("start_province_code not between", value1, value2, "startProvinceCode");
            return (Criteria) this;
        }

        public Criteria andStartCityCodeIsNull() {
            addCriterion("start_city_code is null");
            return (Criteria) this;
        }

        public Criteria andStartCityCodeIsNotNull() {
            addCriterion("start_city_code is not null");
            return (Criteria) this;
        }

        public Criteria andStartCityCodeEqualTo(String value) {
            addCriterion("start_city_code =", value, "startCityCode");
            return (Criteria) this;
        }

        public Criteria andStartCityCodeNotEqualTo(String value) {
            addCriterion("start_city_code <>", value, "startCityCode");
            return (Criteria) this;
        }

        public Criteria andStartCityCodeGreaterThan(String value) {
            addCriterion("start_city_code >", value, "startCityCode");
            return (Criteria) this;
        }

        public Criteria andStartCityCodeGreaterThanOrEqualTo(String value) {
            addCriterion("start_city_code >=", value, "startCityCode");
            return (Criteria) this;
        }

        public Criteria andStartCityCodeLessThan(String value) {
            addCriterion("start_city_code <", value, "startCityCode");
            return (Criteria) this;
        }

        public Criteria andStartCityCodeLessThanOrEqualTo(String value) {
            addCriterion("start_city_code <=", value, "startCityCode");
            return (Criteria) this;
        }

        public Criteria andStartCityCodeLike(String value) {
            addCriterion("start_city_code like", value, "startCityCode");
            return (Criteria) this;
        }

        public Criteria andStartCityCodeNotLike(String value) {
            addCriterion("start_city_code not like", value, "startCityCode");
            return (Criteria) this;
        }

        public Criteria andStartCityCodeIn(List<String> values) {
            addCriterion("start_city_code in", values, "startCityCode");
            return (Criteria) this;
        }

        public Criteria andStartCityCodeNotIn(List<String> values) {
            addCriterion("start_city_code not in", values, "startCityCode");
            return (Criteria) this;
        }

        public Criteria andStartCityCodeBetween(String value1, String value2) {
            addCriterion("start_city_code between", value1, value2, "startCityCode");
            return (Criteria) this;
        }

        public Criteria andStartCityCodeNotBetween(String value1, String value2) {
            addCriterion("start_city_code not between", value1, value2, "startCityCode");
            return (Criteria) this;
        }

        public Criteria andStartAreaCodeIsNull() {
            addCriterion("start_area_code is null");
            return (Criteria) this;
        }

        public Criteria andStartAreaCodeIsNotNull() {
            addCriterion("start_area_code is not null");
            return (Criteria) this;
        }

        public Criteria andStartAreaCodeEqualTo(String value) {
            addCriterion("start_area_code =", value, "startAreaCode");
            return (Criteria) this;
        }

        public Criteria andStartAreaCodeNotEqualTo(String value) {
            addCriterion("start_area_code <>", value, "startAreaCode");
            return (Criteria) this;
        }

        public Criteria andStartAreaCodeGreaterThan(String value) {
            addCriterion("start_area_code >", value, "startAreaCode");
            return (Criteria) this;
        }

        public Criteria andStartAreaCodeGreaterThanOrEqualTo(String value) {
            addCriterion("start_area_code >=", value, "startAreaCode");
            return (Criteria) this;
        }

        public Criteria andStartAreaCodeLessThan(String value) {
            addCriterion("start_area_code <", value, "startAreaCode");
            return (Criteria) this;
        }

        public Criteria andStartAreaCodeLessThanOrEqualTo(String value) {
            addCriterion("start_area_code <=", value, "startAreaCode");
            return (Criteria) this;
        }

        public Criteria andStartAreaCodeLike(String value) {
            addCriterion("start_area_code like", value, "startAreaCode");
            return (Criteria) this;
        }

        public Criteria andStartAreaCodeNotLike(String value) {
            addCriterion("start_area_code not like", value, "startAreaCode");
            return (Criteria) this;
        }

        public Criteria andStartAreaCodeIn(List<String> values) {
            addCriterion("start_area_code in", values, "startAreaCode");
            return (Criteria) this;
        }

        public Criteria andStartAreaCodeNotIn(List<String> values) {
            addCriterion("start_area_code not in", values, "startAreaCode");
            return (Criteria) this;
        }

        public Criteria andStartAreaCodeBetween(String value1, String value2) {
            addCriterion("start_area_code between", value1, value2, "startAreaCode");
            return (Criteria) this;
        }

        public Criteria andStartAreaCodeNotBetween(String value1, String value2) {
            addCriterion("start_area_code not between", value1, value2, "startAreaCode");
            return (Criteria) this;
        }

        public Criteria andEndProvinceCodeIsNull() {
            addCriterion("end_province_code is null");
            return (Criteria) this;
        }

        public Criteria andEndProvinceCodeIsNotNull() {
            addCriterion("end_province_code is not null");
            return (Criteria) this;
        }

        public Criteria andEndProvinceCodeEqualTo(String value) {
            addCriterion("end_province_code =", value, "endProvinceCode");
            return (Criteria) this;
        }

        public Criteria andEndProvinceCodeNotEqualTo(String value) {
            addCriterion("end_province_code <>", value, "endProvinceCode");
            return (Criteria) this;
        }

        public Criteria andEndProvinceCodeGreaterThan(String value) {
            addCriterion("end_province_code >", value, "endProvinceCode");
            return (Criteria) this;
        }

        public Criteria andEndProvinceCodeGreaterThanOrEqualTo(String value) {
            addCriterion("end_province_code >=", value, "endProvinceCode");
            return (Criteria) this;
        }

        public Criteria andEndProvinceCodeLessThan(String value) {
            addCriterion("end_province_code <", value, "endProvinceCode");
            return (Criteria) this;
        }

        public Criteria andEndProvinceCodeLessThanOrEqualTo(String value) {
            addCriterion("end_province_code <=", value, "endProvinceCode");
            return (Criteria) this;
        }

        public Criteria andEndProvinceCodeLike(String value) {
            addCriterion("end_province_code like", value, "endProvinceCode");
            return (Criteria) this;
        }

        public Criteria andEndProvinceCodeNotLike(String value) {
            addCriterion("end_province_code not like", value, "endProvinceCode");
            return (Criteria) this;
        }

        public Criteria andEndProvinceCodeIn(List<String> values) {
            addCriterion("end_province_code in", values, "endProvinceCode");
            return (Criteria) this;
        }

        public Criteria andEndProvinceCodeNotIn(List<String> values) {
            addCriterion("end_province_code not in", values, "endProvinceCode");
            return (Criteria) this;
        }

        public Criteria andEndProvinceCodeBetween(String value1, String value2) {
            addCriterion("end_province_code between", value1, value2, "endProvinceCode");
            return (Criteria) this;
        }

        public Criteria andEndProvinceCodeNotBetween(String value1, String value2) {
            addCriterion("end_province_code not between", value1, value2, "endProvinceCode");
            return (Criteria) this;
        }

        public Criteria andEndCityCodeIsNull() {
            addCriterion("end_city_code is null");
            return (Criteria) this;
        }

        public Criteria andEndCityCodeIsNotNull() {
            addCriterion("end_city_code is not null");
            return (Criteria) this;
        }

        public Criteria andEndCityCodeEqualTo(String value) {
            addCriterion("end_city_code =", value, "endCityCode");
            return (Criteria) this;
        }

        public Criteria andEndCityCodeNotEqualTo(String value) {
            addCriterion("end_city_code <>", value, "endCityCode");
            return (Criteria) this;
        }

        public Criteria andEndCityCodeGreaterThan(String value) {
            addCriterion("end_city_code >", value, "endCityCode");
            return (Criteria) this;
        }

        public Criteria andEndCityCodeGreaterThanOrEqualTo(String value) {
            addCriterion("end_city_code >=", value, "endCityCode");
            return (Criteria) this;
        }

        public Criteria andEndCityCodeLessThan(String value) {
            addCriterion("end_city_code <", value, "endCityCode");
            return (Criteria) this;
        }

        public Criteria andEndCityCodeLessThanOrEqualTo(String value) {
            addCriterion("end_city_code <=", value, "endCityCode");
            return (Criteria) this;
        }

        public Criteria andEndCityCodeLike(String value) {
            addCriterion("end_city_code like", value, "endCityCode");
            return (Criteria) this;
        }

        public Criteria andEndCityCodeNotLike(String value) {
            addCriterion("end_city_code not like", value, "endCityCode");
            return (Criteria) this;
        }

        public Criteria andEndCityCodeIn(List<String> values) {
            addCriterion("end_city_code in", values, "endCityCode");
            return (Criteria) this;
        }

        public Criteria andEndCityCodeNotIn(List<String> values) {
            addCriterion("end_city_code not in", values, "endCityCode");
            return (Criteria) this;
        }

        public Criteria andEndCityCodeBetween(String value1, String value2) {
            addCriterion("end_city_code between", value1, value2, "endCityCode");
            return (Criteria) this;
        }

        public Criteria andEndCityCodeNotBetween(String value1, String value2) {
            addCriterion("end_city_code not between", value1, value2, "endCityCode");
            return (Criteria) this;
        }

        public Criteria andEndAreaCodeIsNull() {
            addCriterion("end_area_code is null");
            return (Criteria) this;
        }

        public Criteria andEndAreaCodeIsNotNull() {
            addCriterion("end_area_code is not null");
            return (Criteria) this;
        }

        public Criteria andEndAreaCodeEqualTo(String value) {
            addCriterion("end_area_code =", value, "endAreaCode");
            return (Criteria) this;
        }

        public Criteria andEndAreaCodeNotEqualTo(String value) {
            addCriterion("end_area_code <>", value, "endAreaCode");
            return (Criteria) this;
        }

        public Criteria andEndAreaCodeGreaterThan(String value) {
            addCriterion("end_area_code >", value, "endAreaCode");
            return (Criteria) this;
        }

        public Criteria andEndAreaCodeGreaterThanOrEqualTo(String value) {
            addCriterion("end_area_code >=", value, "endAreaCode");
            return (Criteria) this;
        }

        public Criteria andEndAreaCodeLessThan(String value) {
            addCriterion("end_area_code <", value, "endAreaCode");
            return (Criteria) this;
        }

        public Criteria andEndAreaCodeLessThanOrEqualTo(String value) {
            addCriterion("end_area_code <=", value, "endAreaCode");
            return (Criteria) this;
        }

        public Criteria andEndAreaCodeLike(String value) {
            addCriterion("end_area_code like", value, "endAreaCode");
            return (Criteria) this;
        }

        public Criteria andEndAreaCodeNotLike(String value) {
            addCriterion("end_area_code not like", value, "endAreaCode");
            return (Criteria) this;
        }

        public Criteria andEndAreaCodeIn(List<String> values) {
            addCriterion("end_area_code in", values, "endAreaCode");
            return (Criteria) this;
        }

        public Criteria andEndAreaCodeNotIn(List<String> values) {
            addCriterion("end_area_code not in", values, "endAreaCode");
            return (Criteria) this;
        }

        public Criteria andEndAreaCodeBetween(String value1, String value2) {
            addCriterion("end_area_code between", value1, value2, "endAreaCode");
            return (Criteria) this;
        }

        public Criteria andEndAreaCodeNotBetween(String value1, String value2) {
            addCriterion("end_area_code not between", value1, value2, "endAreaCode");
            return (Criteria) this;
        }

        public Criteria andCreatorCodeIsNull() {
            addCriterion("creator_code is null");
            return (Criteria) this;
        }

        public Criteria andCreatorCodeIsNotNull() {
            addCriterion("creator_code is not null");
            return (Criteria) this;
        }

        public Criteria andCreatorCodeEqualTo(String value) {
            addCriterion("creator_code =", value, "creatorCode");
            return (Criteria) this;
        }

        public Criteria andCreatorCodeNotEqualTo(String value) {
            addCriterion("creator_code <>", value, "creatorCode");
            return (Criteria) this;
        }

        public Criteria andCreatorCodeGreaterThan(String value) {
            addCriterion("creator_code >", value, "creatorCode");
            return (Criteria) this;
        }

        public Criteria andCreatorCodeGreaterThanOrEqualTo(String value) {
            addCriterion("creator_code >=", value, "creatorCode");
            return (Criteria) this;
        }

        public Criteria andCreatorCodeLessThan(String value) {
            addCriterion("creator_code <", value, "creatorCode");
            return (Criteria) this;
        }

        public Criteria andCreatorCodeLessThanOrEqualTo(String value) {
            addCriterion("creator_code <=", value, "creatorCode");
            return (Criteria) this;
        }

        public Criteria andCreatorCodeLike(String value) {
            addCriterion("creator_code like", value, "creatorCode");
            return (Criteria) this;
        }

        public Criteria andCreatorCodeNotLike(String value) {
            addCriterion("creator_code not like", value, "creatorCode");
            return (Criteria) this;
        }

        public Criteria andCreatorCodeIn(List<String> values) {
            addCriterion("creator_code in", values, "creatorCode");
            return (Criteria) this;
        }

        public Criteria andCreatorCodeNotIn(List<String> values) {
            addCriterion("creator_code not in", values, "creatorCode");
            return (Criteria) this;
        }

        public Criteria andCreatorCodeBetween(String value1, String value2) {
            addCriterion("creator_code between", value1, value2, "creatorCode");
            return (Criteria) this;
        }

        public Criteria andCreatorCodeNotBetween(String value1, String value2) {
            addCriterion("creator_code not between", value1, value2, "creatorCode");
            return (Criteria) this;
        }

        public Criteria andFeatureIsNull() {
            addCriterion("feature is null");
            return (Criteria) this;
        }

        public Criteria andFeatureIsNotNull() {
            addCriterion("feature is not null");
            return (Criteria) this;
        }

        public Criteria andFeatureEqualTo(String value) {
            addCriterion("feature =", value, "feature");
            return (Criteria) this;
        }

        public Criteria andFeatureNotEqualTo(String value) {
            addCriterion("feature <>", value, "feature");
            return (Criteria) this;
        }

        public Criteria andFeatureGreaterThan(String value) {
            addCriterion("feature >", value, "feature");
            return (Criteria) this;
        }

        public Criteria andFeatureGreaterThanOrEqualTo(String value) {
            addCriterion("feature >=", value, "feature");
            return (Criteria) this;
        }

        public Criteria andFeatureLessThan(String value) {
            addCriterion("feature <", value, "feature");
            return (Criteria) this;
        }

        public Criteria andFeatureLessThanOrEqualTo(String value) {
            addCriterion("feature <=", value, "feature");
            return (Criteria) this;
        }

        public Criteria andFeatureLike(String value) {
            addCriterion("feature like", value, "feature");
            return (Criteria) this;
        }

        public Criteria andFeatureNotLike(String value) {
            addCriterion("feature not like", value, "feature");
            return (Criteria) this;
        }

        public Criteria andFeatureIn(List<String> values) {
            addCriterion("feature in", values, "feature");
            return (Criteria) this;
        }

        public Criteria andFeatureNotIn(List<String> values) {
            addCriterion("feature not in", values, "feature");
            return (Criteria) this;
        }

        public Criteria andFeatureBetween(String value1, String value2) {
            addCriterion("feature between", value1, value2, "feature");
            return (Criteria) this;
        }

        public Criteria andFeatureNotBetween(String value1, String value2) {
            addCriterion("feature not between", value1, value2, "feature");
            return (Criteria) this;
        }

        public Criteria andHlUrlsIsNull() {
            addCriterion("hl_urls is null");
            return (Criteria) this;
        }

        public Criteria andHlUrlsIsNotNull() {
            addCriterion("hl_urls is not null");
            return (Criteria) this;
        }

        public Criteria andHlUrlsEqualTo(String value) {
            addCriterion("hl_urls =", value, "hlUrls");
            return (Criteria) this;
        }

        public Criteria andHlUrlsNotEqualTo(String value) {
            addCriterion("hl_urls <>", value, "hlUrls");
            return (Criteria) this;
        }

        public Criteria andHlUrlsGreaterThan(String value) {
            addCriterion("hl_urls >", value, "hlUrls");
            return (Criteria) this;
        }

        public Criteria andHlUrlsGreaterThanOrEqualTo(String value) {
            addCriterion("hl_urls >=", value, "hlUrls");
            return (Criteria) this;
        }

        public Criteria andHlUrlsLessThan(String value) {
            addCriterion("hl_urls <", value, "hlUrls");
            return (Criteria) this;
        }

        public Criteria andHlUrlsLessThanOrEqualTo(String value) {
            addCriterion("hl_urls <=", value, "hlUrls");
            return (Criteria) this;
        }

        public Criteria andHlUrlsLike(String value) {
            addCriterion("hl_urls like", value, "hlUrls");
            return (Criteria) this;
        }

        public Criteria andHlUrlsNotLike(String value) {
            addCriterion("hl_urls not like", value, "hlUrls");
            return (Criteria) this;
        }

        public Criteria andHlUrlsIn(List<String> values) {
            addCriterion("hl_urls in", values, "hlUrls");
            return (Criteria) this;
        }

        public Criteria andHlUrlsNotIn(List<String> values) {
            addCriterion("hl_urls not in", values, "hlUrls");
            return (Criteria) this;
        }

        public Criteria andHlUrlsBetween(String value1, String value2) {
            addCriterion("hl_urls between", value1, value2, "hlUrls");
            return (Criteria) this;
        }

        public Criteria andHlUrlsNotBetween(String value1, String value2) {
            addCriterion("hl_urls not between", value1, value2, "hlUrls");
            return (Criteria) this;
        }

        public Criteria andPublishAddressIsNull() {
            addCriterion("publish_address is null");
            return (Criteria) this;
        }

        public Criteria andPublishAddressIsNotNull() {
            addCriterion("publish_address is not null");
            return (Criteria) this;
        }

        public Criteria andPublishAddressEqualTo(String value) {
            addCriterion("publish_address =", value, "publishAddress");
            return (Criteria) this;
        }

        public Criteria andPublishAddressNotEqualTo(String value) {
            addCriterion("publish_address <>", value, "publishAddress");
            return (Criteria) this;
        }

        public Criteria andPublishAddressGreaterThan(String value) {
            addCriterion("publish_address >", value, "publishAddress");
            return (Criteria) this;
        }

        public Criteria andPublishAddressGreaterThanOrEqualTo(String value) {
            addCriterion("publish_address >=", value, "publishAddress");
            return (Criteria) this;
        }

        public Criteria andPublishAddressLessThan(String value) {
            addCriterion("publish_address <", value, "publishAddress");
            return (Criteria) this;
        }

        public Criteria andPublishAddressLessThanOrEqualTo(String value) {
            addCriterion("publish_address <=", value, "publishAddress");
            return (Criteria) this;
        }

        public Criteria andPublishAddressLike(String value) {
            addCriterion("publish_address like", value, "publishAddress");
            return (Criteria) this;
        }

        public Criteria andPublishAddressNotLike(String value) {
            addCriterion("publish_address not like", value, "publishAddress");
            return (Criteria) this;
        }

        public Criteria andPublishAddressIn(List<String> values) {
            addCriterion("publish_address in", values, "publishAddress");
            return (Criteria) this;
        }

        public Criteria andPublishAddressNotIn(List<String> values) {
            addCriterion("publish_address not in", values, "publishAddress");
            return (Criteria) this;
        }

        public Criteria andPublishAddressBetween(String value1, String value2) {
            addCriterion("publish_address between", value1, value2, "publishAddress");
            return (Criteria) this;
        }

        public Criteria andPublishAddressNotBetween(String value1, String value2) {
            addCriterion("publish_address not between", value1, value2, "publishAddress");
            return (Criteria) this;
        }
    }

    /**
     */
    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}