package com.notname.callcar.entity;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * cc_collection
 * @author 
 */
@Data
public class CcCollectionEntity implements Serializable {
    private Long id;

    /**
     * 创建时间
     */
    private Date gmtCreate;

    /**
     * 修改时间
     */
    private Date gmtModified;

    /**
     * 逻辑删除标
     */
    private Integer deleted;

    /**
     * 收藏code
     */
    private String collectionCode;

    /**
     * 业务类型code,人找车:1,车找人:2,头条:3
     */
    private Integer bizTypeCode;

    /**
     * 业务code
     */
    private String bizCode;

    /**
     * 扩展字段 json方式存储
     */
    private String feature;

    /**
     * 收藏人code
     */
    private String createCode;

    private static final long serialVersionUID = 1L;
}