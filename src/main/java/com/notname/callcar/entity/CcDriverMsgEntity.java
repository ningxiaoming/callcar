package com.notname.callcar.entity;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * cc_driver_msg
 * @author 
 */
@Data
public class CcDriverMsgEntity implements Serializable {
    private Long id;

    /**
     * 创建时间
     */
    private Date gmtCreate;

    /**
     * 修改时间
     */
    private Date gmtModified;

    /**
     * 逻辑删除标
     */
    private Integer deleted;

    /**
     * 司机发送的信息code
     */
    private String dmCode;

    /**
     * 联系电话
     */
    private String phone;

    /**
     * 空闲座位数量
     */
    private Integer freeSeatNum;

    /**
     * 预期价格（分）
     */
    private Integer expectPriceFen;

    /**
     * 出发日期
     */
    private Long startDate;

    /**
     * 起始省code
     */
    private String startProvinceCode;

    /**
     * 起始市code
     */
    private String startCityCode;

    /**
     * 起始区code
     */
    private String startAreaCode;

    /**
     * 目的省code
     */
    private String endProvinceCode;

    /**
     * 目的市code
     */
    private String endCityCode;

    /**
     * 目睹区code
     */
    private String endAreaCode;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建者code
     */
    private String creatorCode;

    /**
     * 扩展字段 json方式存储
     */
    private String feature;

    private static final long serialVersionUID = 1L;
}