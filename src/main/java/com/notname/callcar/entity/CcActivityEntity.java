package com.notname.callcar.entity;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * cc_activity
 * @author 
 */
@Data
public class CcActivityEntity implements Serializable {
    private Long id;

    /**
     * 创建时间
     */
    private Date gmtCreate;

    /**
     * 修改时间
     */
    private Date gmtModified;

    /**
     * 逻辑删除标
     */
    private Integer deleted;

    /**
     * 活动code
     */
    private String activityCode;

    /**
     * 业务类型
     */
    private Integer bizType;

    /**
     * 活动状态,1:上架，0:下架
     */
    private Integer activityStatus;

    /**
     * 活动的图片url
     */
    private String imgUrl;

    /**
     * 展示的文案
     */
    private String showText;

    /**
     * 点击跳转的h5页面url
     */
    private String h5Url;

    /**
     * 创建者code
     */
    private String creatorCode;

    /**
     * 扩展字段 json方式存储
     */
    private String feature;

    private static final long serialVersionUID = 1L;
}