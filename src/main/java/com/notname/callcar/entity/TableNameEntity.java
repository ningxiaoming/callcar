package com.notname.callcar.entity;

import java.io.Serializable;
import lombok.Data;

/**
 * table_name
 * @author 
 */
@Data
public class TableNameEntity implements Serializable {
    private Integer id;

    private String name;

    private static final long serialVersionUID = 1L;
}