package com.notname.callcar.entity;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * cc_user_msg
 * @author 
 */
@Data
public class CcUserMsgEntity implements Serializable {
    private Long id;

    /**
     * 创建时间
     */
    private Date gmtCreate;

    /**
     * 修改时间
     */
    private Date gmtModified;

    /**
     * 逻辑删除标
     */
    private Integer deleted;

    /**
     * 用户发送的信息code
     */
    private String umCode;

    /**
     * 联系电话
     */
    private String phone;

    /**
     * 坐车人数
     */
    private Integer headCount;

    /**
     * 预期价格（分）
     */
    private Integer expectPriceFen;

    /**
     * 出发日期
     */
    private Long startDate;

    /**
     * 起始省code
     */
    private String startProvinceCode;

    /**
     * 起始市code
     */
    private String startCityCode;

    /**
     * 起始区code
     */
    private String startAreaCode;

    /**
     * 目的省code
     */
    private String endProvinceCode;

    /**
     * 目的市code
     */
    private String endCityCode;

    /**
     * 目的区code
     */
    private String endAreaCode;

    /**
     * 备注信息
     */
    private String remark;

    /**
     * 创建者code
     */
    private String creatorCode;

    /**
     * 扩展字段 json方式存储
     */
    private String feature;

    private static final long serialVersionUID = 1L;
}