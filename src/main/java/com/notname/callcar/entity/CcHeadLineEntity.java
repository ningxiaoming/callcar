package com.notname.callcar.entity;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * cc_head_line
 * @author 
 */
@Data
public class CcHeadLineEntity implements Serializable {
    private Long id;

    /**
     * 创建时间
     */
    private Date gmtCreate;

    /**
     * 修改时间
     */
    private Date gmtModified;

    /**
     * 逻辑删除标
     */
    private Integer deleted;

    /**
     * 头条信息code
     */
    private String hlCode;

    /**
     * 业务类型code:疫情信息:1，头条信息:2 ....
     */
    private Integer bizTypeCode;

    /**
     * 头条时间
     */
    private Long headLineTime;

    /**
     * 头条内容
     */
    private String content;

    /**
     * 点赞数量
     */
    private Integer likeCount;

    /**
     * 评论数量
     */
    private Integer commentCount;

    /**
     * 起始省code
     */
    private String startProvinceCode;

    /**
     * 起始市code
     */
    private String startCityCode;

    /**
     * 起始区code
     */
    private String startAreaCode;

    /**
     * 目的省code
     */
    private String endProvinceCode;

    /**
     * 目的市code
     */
    private String endCityCode;

    /**
     * 目睹区code
     */
    private String endAreaCode;

    /**
     * 创建者code
     */
    private String creatorCode;

    /**
     * 扩展字段 json方式存储
     */
    private String feature;

    /**
     * 图片urls
     */
    private String hlUrls;

    /**
     * 发布当前位置
     */
    private String publishAddress;

    private static final long serialVersionUID = 1L;
}