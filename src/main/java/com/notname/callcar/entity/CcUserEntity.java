package com.notname.callcar.entity;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * cc_user
 * @author 
 */
@Data
public class CcUserEntity implements Serializable {
    private Long id;

    /**
     * 创建时间
     */
    private Date gmtCreate;

    /**
     * 修改时间
     */
    private Date gmtModified;

    /**
     * 逻辑删除标
     */
    private Integer deleted;

    /**
     * 用户code
     */
    private String userCode;

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 微信openId
     */
    private String openId;

    /**
     * 微信提供
     */
    private String unionId;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 头像URL
     */
    private String avatarUrl;

    /**
     * 性别1 男 0 女
     */
    private Integer gender;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 使用语言
     */
    private String useLanguage;

    /**
     * 是否已补全,0没有，1补全了
     */
    private Integer completed;

    /**
     * 是否锁定
     */
    private Integer isLocked;

    /**
     * 扩展字段 json方式存储
     */
    private String feature;

    private static final long serialVersionUID = 1L;
}