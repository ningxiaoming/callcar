package com.notname.callcar.entity;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * cc_comment
 * @author 
 */
@Data
public class CcCommentEntity implements Serializable {
    private Long id;

    /**
     * 创建时间
     */
    private Date gmtCreate;

    /**
     * 修改时间
     */
    private Date gmtModified;

    /**
     * 逻辑删除标
     */
    private Integer deleted;

    /**
     * 点赞code
     */
    private String commentCode;

    /**
     * 业务类型code,头条信息:2,评论是3
     */
    private Integer bizTypeCode;

    /**
     * 业务code
     */
    private String bizCode;

    /**
     * 评论内容
     */
    private String content;

    /**
     * 点赞数量
     */
    private Integer likeCount;

    /**
     * 创建者code
     */
    private String creatorCode;

    /**
     * 扩展字段 json方式存储
     */
    private String feature;

    private static final long serialVersionUID = 1L;
}