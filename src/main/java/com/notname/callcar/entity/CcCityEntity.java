package com.notname.callcar.entity;

import java.io.Serializable;
import lombok.Data;

/**
 * cc_city
 * @author 
 */
@Data
public class CcCityEntity implements Serializable {
    private Integer id;

    private String cityCode;

    private String cityName;

    private String parentCode;

    private String type;

    private static final long serialVersionUID = 1L;
}